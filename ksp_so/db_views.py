from django.db import models

from observation.models.db_views import BaseHolzProduktion

from .models import Gemeinde, Plot, PlotObs, TreeGroup, TreeObs, TreeSpecies, Vita


class HomepageView(models.Model):
    gemeinde = models.CharField(max_length=50, db_column="gemeindename")
    jahr = models.SmallIntegerField("Aufnahmejahr", db_column="year")
    probepunkte = models.IntegerField("Anzahl Probepunkte", db_column="Anzahl Probepunkte")
    waldflaeche = models.DecimalField("theoretische Waldfläche pro ha", max_digits=5, decimal_places=1,
        db_column="theoretische Waldfläche ha")
    probebaum_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    stammzahl_ha = models.IntegerField("Stammzahl pro ha", db_column="Stammzahl pro ha")
    stammzahl_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler")
    volumen_ha = models.DecimalField("Volumen [m3 pro ha]", max_digits=5, decimal_places=1,
        db_column="Volumen pro ha")
    volumen_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler2")
    grundflaeche_ha = models.DecimalField("Grundflaeche [m2 pro ha]", max_digits=5, decimal_places=1,
        db_column="Grundflaeche pro ha")
    grundflaeche_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler3")
    inv_period = models.SmallIntegerField()

    class Meta:
        db_table = 'bv_web_homepage_holzproduktion'
        managed = False


class HolzProduktion(BaseHolzProduktion):
    waldflaeche = models.FloatField("theoretische Waldfläche", db_column="waldflaeche")

    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs"
        managed = False


class HolzBiomasse(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    plotobs = models.ForeignKey(PlotObs, db_column='id', primary_key=True, on_delete=models.DO_NOTHING, related_name='+')
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING, related_name='+')
    year = models.SmallIntegerField(verbose_name="Jahr")
    gemeinde = models.ForeignKey(
        Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING, related_name='+'
    )
    biomasse_abs = models.FloatField("Biomasse [kg]", db_column="Biomasse")
    biomasse_rel = models.FloatField("Biomasse/ha", db_column="Biomasse pro ha")

    class Meta:
        db_table = "bv_biomasse_pro_plotobs"
        managed = False


class HolzBiomasseProSpec(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    id = models.CharField(max_length=20, primary_key=True)
    plotobs = models.ForeignKey(PlotObs, db_column='plot_obs_id', on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING)
    spec = models.ForeignKey(TreeSpecies, verbose_name="Baumart", db_column='tree_spec_id', on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")

    gemeinde = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING)
    biomasse_abs = models.FloatField("Biomasse [kg]", db_column="Biomasse")
    biomasse_rel = models.FloatField("Biomasse/ha", db_column="Biomasse pro ha")

    class Meta:
        db_table = "bv_biomasse_pro_plotobs_und_allspec"
        managed = False


class HolzBiomasseProSpecGroup(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    id = models.CharField(max_length=20, primary_key=True)
    plotobs = models.ForeignKey(PlotObs, db_column='plot_obs_id', on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING)
    group = models.ForeignKey(TreeGroup, verbose_name="Baumartgruppe", db_column='tree_group_id', on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")

    gemeinde = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING)
    biomasse_abs = models.FloatField("Biomasse [kg]", db_column="Biomasse")
    biomasse_rel = models.FloatField("Biomasse/ha", db_column="Biomasse pro ha")

    class Meta:
        db_table = "bv_biomasse_pro_plotobs_und_specgroup"
        managed = False
