from django.urls import include, path

from . import views

urlpatterns = [
    path('', views.SOGemeindenView.as_view(), name='home'),
    path('inventories/', views.InventoryHomeView.as_view(), name='inventories'),
    path('owners/', views.OwnersHomeView.as_view(), name='owners'),
    path('standard_analyse/inventory-<int:inv1_id>-<int:inv2_id>/', views.StandardAnalyseView.as_view(),
        name='standard-analyse-by-inventory'),
    path('standard_analyse/owner-<int:owner_id>/<int:year1>/<int:year2>/', views.StandardAnalyseView.as_view(),
        name='standard-analyse-by-owner'),

    path('mobile/', include('mobile.urls')),
]
