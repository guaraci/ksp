from django.db.models import Avg, Sum
from django.urls import reverse

from observation.models.db_views import (
    Einwuchs, EinwuchsProSpec, EinwuchsProSpecGroup,
    HolzProduktionProSpec, HolzProduktionProSpecGroup,
    Nutzung, NutzungProSpec, NutzungProSpecGroup,
    Totholz, TotholzProSpec, TotholzProSpecGroup,
)
from observation.queries import (
    GigantenQuery, Groupment, GroupmentRegion, GroupmentSpec, GroupmentSpecGroup,
    GemeindePerimGroup, HeutOwnerPerimGroup, HistOwnerPerimGroup, PerimeterSetBase,
    PerimGroup, QueryData, ZuwachsQuery
)

from .db_views import (
    HolzBiomasse, HolzBiomasseProSpec, HolzBiomasseProSpecGroup, HolzProduktion
)
from .models import AdminRegion, Gemeinde, Inventory, Owner, PlotObs


VIEW_MAP = {
    'Stammzahl': QueryData(
        model=HolzProduktion, descr='Stammzahl, Volumen, Grundfläche',
        model_by_spec=HolzProduktionProSpec, model_by_spec_group=HolzProduktionProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
    ),
    'Totholz': QueryData(
        model=Totholz, descr='Totholz',
        model_by_spec=TotholzProSpec, model_by_spec_group=TotholzProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Naturschutz',
    ),
    'Einwuchs': QueryData(
        model=Einwuchs, descr='Einwuchs',
        model_by_spec=EinwuchsProSpec, model_by_spec_group=EinwuchsProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
    ),
    'Nutzung': QueryData(
        model=Nutzung, descr='Nutzung',
        model_by_spec=NutzungProSpec, model_by_spec_group=NutzungProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
    ),
    'Zuwachs': ZuwachsQuery(),
    'Biomasse': QueryData(
        model=HolzBiomasse,
        descr='Biomasse',
        model_by_spec=HolzBiomasseProSpec, model_by_spec_group=HolzBiomasseProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        annot_map={'biomasse_abs': Sum, 'biomasse_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
    ),
    'Giganten': GigantenQuery(),
    # Essentially to query verbose field names for some groupable keys not on views
    'plotobs': QueryData(model=PlotObs, category=''),
}


class PerimeterSet(PerimeterSetBase):
    @classmethod
    def get_perimeters(cls):
        return [
            GemeindePerimGroup(
                "Gemeinden",
                Gemeinde.objects.all().order_by('name'),
                _infos="Politische Gemeinden des Kantons Solothurn.",
                infos_url=reverse('doc-specific', args=['gemeinden']),
            ),
            PerimGroup(
                "Forstkreise",
                AdminRegion.objects.filter(region_type__name='Forstkreis').order_by('name'),
                _infos="Alle Forstkreise des Kantons Solothurn können ausgewählt werden.",
                infos_url=reverse('doc-specific', args=['forstkreise']),
                short_name="fkreis",
            ),
            PerimGroup(
                "Forstreviere",
                AdminRegion.objects.filter(region_type__name='Forstrevier').order_by('name'),
                _infos="Alle Forstreviere des Kantons Solothurn können ausgewählt werden.",
                infos_url=reverse('doc-specific', args=['forstreviere']),
                short_name="frevier",
            ),
            HistOwnerPerimGroup(
                _infos="Wichtigste Waldeigentümer des Kantons Solothurn können ausgewählt werden.",
            ),
            HeutOwnerPerimGroup(
                _infos="Wichtigste Waldeigentümer des Kantons Solothurn können ausgewählt werden.",
            ),
            PerimGroup(
                "Freiperimeter",
                AdminRegion.objects.filter(region_type__name='Freiperimeter').order_by('name'),
                _infos="Website-Administrator definierte Regionen.",
                infos_url=reverse('doc-specific', args=['freiperimeter']),
                short_name="freiperimeter",
            ),
        ]

    @classmethod
    def query_model_by_qdict(cls, base_qs, query_dict, plot_obs_prefix=None, **kwargs):
        inventories = query_dict.getlist('inventories')
        if inventories:
            query = base_qs.filter(**{f'{plot_obs_prefix}inv_team__pk__in': inventories})
            inv_qs = Inventory.objects.filter(pk__in=inventories).order_by('name')
            perim_feedback = ("Inventory", ", ".join([inv.name for inv in inv_qs]))
            # For now, not combinable with other type of perimeters.
            return query, perim_feedback
        return super().query_model_by_qdict(base_qs, query_dict, plot_obs_prefix=plot_obs_prefix, **kwargs)


def get_groupments():
    return {
        'perims': [
            Groupment('municipality', 'Gemeinde'),
            GroupmentRegion('forstkreis', 'Forstkreis'),
            GroupmentRegion('forstrevier', 'Forstrevier'),
            Groupment('waldfunk', 'Waldfunktion', accessor="waldplan__wpnr"),
            Groupment(
                'eigentumer-name', 'Hist. Eigentümer', accessor='owner__name',
                docs_title="Gruppierung nach wichtigsten Waldeigentümern des Kantons Solothurn. Klicken Sie für weitere Informationen.",
                docs_url='/docs/eigentuemer/',
            ),
            GroupmentRegion(
                'eigentumer-geom', 'Heut. Eigentümer',
                docs_title="Gruppierung nach wichtigsten Waldeigentümern des Kantons Solothurn. Klicken Sie für weitere Informationen.",
                docs_url='/docs/eigentuemer/',
            ),
            GroupmentRegion('freiperimeter', 'Freiperimeter'),
        ],
        'periods': [
            Groupment('year', 'Jahr', checked=True),
            Groupment(
                'inv_period', 'Aufnahmeperiode', accessor='inv_team__period',
                docs_title="Erste Phase (1987-1999), Zweite Phase (2000-2014), Dritte Phase (2017-)"),
            Groupment(
                'latestinv', 'Nur letztes Inventar',
                docs_title="nur Punkte vom letzten Inventar"),
        ],
        'bestand': [
            Groupment(
                'stand_devel_stage', "Entwicklungsstufe",
                docs_title="Bestimmte Etappe der Entwicklung eines Waldbestandes charakterisiert durch dessen Grösse.",
                docs_url=reverse('doc-specific', args=['entwicklungsstufe'])),
            Groupment(
                'stand_forest_mixture', "Mischungsgrad",
                docs_title="In einem Bestand oder in einer Bestandesschicht vorhandene Baumarten.",
                docs_url=reverse('doc-specific', args=['mischungsgrad'])),
            Groupment(
                'stand_crown_closure', "Schlussgrad",
                docs_title="Ist ein Mass für die Intensität der Kronenberührungen innerhalb eines Bestandes.",
                docs_url=reverse('doc-specific', args=['schlussgrad'])),
        ],
        'phytosoc': [],
        'various': [
            GroupmentSpec('spec', "Baumart"),
            GroupmentSpecGroup('group', "Baumartgruppe"),
            Groupment(
                'otype', "Eigentümertyp", accessor='owner__otype', fkey_target='__name',
                docs_title="Die Waldeigentümer werden in verschiedene Kategorien (Tabelle owner_type) gruppiert. Damit kann die Auswertung pro Eigentümertyp umgesetzt werden (Waldentwicklungsplanung). Die Erläuterungen zum Eigentümer sind unter Eigentümer aufgeführt."
            ),
            Groupment(
                'ostatus', "Eigentümer Status", accessor='owner__otype__status',
                docs_title="Die Waldeigentümer werden in die beiden Kategorien i) öffentlich (Einwohnergemeinden, "
                           "Bürgergemeinden, Burgerkoorporationen, Bund, Kanton) und ii) privat (Privatwaldbesitzer, Stiftungen)."),
        ],
    }
