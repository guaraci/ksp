from django.apps import AppConfig


class KantonSOConfig(AppConfig):
    name = 'ksp_so'
    read_only_psql_group = 'kanton_so'
