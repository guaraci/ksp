CREATE OR REPLACE FUNCTION ksp_grundflaeche_bl(diameter smallint) RETURNS double precision AS $$
  SELECT pi() * $1 * $1 / 4 / 10000;
$$ LANGUAGE SQL IMMUTABLE;

COMMENT ON FUNCTION public.ksp_grundflaeche_bl(smallint) IS 'Stammquerschnittsfläche eines Baumes in 1.3 m Höhe (BHD-Messstelle) bzw. Summe der Stammquerschnittsflächen aller Bäume eines Bestandes.';
