CREATE OR REPLACE FUNCTION public.ksp_biomass_tarif_wsl_so_2022(
	dbh integer,
	z25 integer,
	spec_tarif character,
	phyto_tarif character)
    RETURNS numeric
    LANGUAGE 'sql'
    COST 100
    IMMUTABLE PARALLEL SAFE
AS $BODY$
    SELECT CASE WHEN dbh <= 0 THEN 0
        ELSE exp(-2.98
	+2.842*ln(dbh)
	+-0.002177*(ln(dbh))^4
	+-0.0002912*z25 
	+ COALESCE(
	 ((spec_tarif='tanne')::integer * 0.03829) 
	+ ((spec_tarif='ubrige_nad')::integer * -0.07363) 
	+ ((spec_tarif='buche')::integer * 0.4173)
	+ ((spec_tarif='ubrige_laub')::integer * 0.1097),0)
	+ COALESCE(
	 ((phyto_tarif='vege1')::integer * -0.01768) 
	+ ((phyto_tarif='vege4')::integer * 0.005135) 
	+ ((phyto_tarif='vege6')::integer * -0.0713) 
	+ ((phyto_tarif='vege8')::integer * -0.05704) 
	+ ((phyto_tarif='vege10')::integer * -0.04961)
	+ ((phyto_tarif='vege15')::integer * -0.1846)
	+ ((phyto_tarif='vege16')::integer * 0.02689)
	+ ((phyto_tarif='vege24')::integer * 0.01183) 
	+ ((phyto_tarif='vege32')::integer * -0.05444)
	+ ((phyto_tarif='vege33')::integer * -0.03498)
	+ ((phyto_tarif='vege36')::integer * 0.009557)
	+ ((phyto_tarif='vege37')::integer * -0.008618) 
	+ ((phyto_tarif='vege38')::integer * -0.1139),0))
        END
$BODY$;
