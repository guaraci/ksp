CREATE OR REPLACE FUNCTION ksp_volume_ha(diameter smallint, dichte numeric) RETURNS numeric AS $$
  SELECT CASE WHEN $1 = 0 THEN 0 ELSE (($1 * $1 * 0.0011) - (@$1 * 0.0011) - 0.079) * $2 END AS volume_ha;
$$ LANGUAGE SQL IMMUTABLE PARALLEL SAFE;
