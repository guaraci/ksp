CREATE OR REPLACE FUNCTION ksp_grundflaeche_ha(diameter smallint, dichte numeric) RETURNS double precision AS $$
  SELECT pi() * $1 * $1 / 4 / 10000 * $2;
$$ LANGUAGE SQL IMMUTABLE;
