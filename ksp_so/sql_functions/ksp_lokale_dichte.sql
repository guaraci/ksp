CREATE OR REPLACE FUNCTION ksp_lokale_dichte(edgef numeric) RETURNS numeric AS $$
    SELECT 100.0 / 3.0;
$$ LANGUAGE SQL IMMUTABLE;

COMMENT ON FUNCTION public.ksp_lokale_dichte(numeric)
    IS 'Repräsentationsfläche eines Aufnahmeplots in Abhängigkeit des Waldrandfaktors. Die lokale Dichte bezeichnet die am Stichprobenpunkt festgestellte (gemessene) räumlich Dichte der Zielvariable pro Flächeneinheit (normalerweise pro Hektar).';
