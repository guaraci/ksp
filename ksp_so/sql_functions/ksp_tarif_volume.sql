CREATE OR REPLACE FUNCTION public.ksp_tarif_volume(
	dbh integer,
	z25 integer,
	spec_tarif character,
	phyto_tarif character)
    RETURNS double precision
    LANGUAGE 'sql'
    COST 100
    IMMUTABLE PARALLEL SAFE
AS $BODY$
    SELECT CASE WHEN dbh <= 0 THEN 0
        ELSE exp(-12.31
	+3.851*ln(dbh)
	+-0.006672*(ln(dbh))^4
	+-0.0002366*z25 
	+ COALESCE(
	 ((spec_tarif='tanne')::integer * 0.05255) 
	+ ((spec_tarif='ubrige_nad')::integer * -0.05637) 
	+ ((spec_tarif='buche')::integer * -0.0111)
	+ ((spec_tarif='ubrige_laub')::integer * -0.1077),0)
	+ COALESCE(
	 ((phyto_tarif='vege1')::integer * -0.08316) 
	+ ((phyto_tarif='vege4')::integer * 0.01559) 
	+ ((phyto_tarif='vege6')::integer * 0.0494) 
	+ ((phyto_tarif='vege8')::integer * -0.07645) 
	+ ((phyto_tarif='vege10')::integer * 0.03635)
	+ ((phyto_tarif='vege15')::integer * -0.2071)
	+ ((phyto_tarif='vege16')::integer * -0.01414)
	+ ((phyto_tarif='vege24')::integer * -0.04382) 
	+ ((phyto_tarif='vege32')::integer * 0.005721)
	+ ((phyto_tarif='vege33')::integer * -0.1409)
	+ ((phyto_tarif='vege36')::integer * -0.01218)
	+ ((phyto_tarif='vege37')::integer * 0.07607) 
	+ ((phyto_tarif='vege38')::integer * -0.03547),0))
        END
$BODY$;

COMMENT ON FUNCTION public.ksp_tarif_volume(integer, integer, character, character)
    IS 'Modell aus Baumdaten des LFI (erstellt durch Meinrad Abegg)
Parameter:
dbh ([cm] Brusthöhendurchmesser (mit Kluppe bzw. Messband)
z25 ([m ü.M.] Höhe ü.M. aus DHM25 abgeleitet)
spec_tarif ([code] Baumartengruppen (HABART))
phyto_tarif ([code] Vegetationseinheiten gruppiert (vege))

Modellentwicklung Volumen m3 (WV=Wahres Einzelbaumvolumen) ohne HAHBART1  UND VEGE7  (Schaftholzvolumen in Rinde)
Bemerkung:  VEGE7 (Vegetationseinheit = 7) und HAHBART1 (Baumart = Fichte) werden als Standard angesehen. Der Graph wird dann für alle anderen Ausprägungen angehoben oder gesenkt.
WV= [m3] Wahres Einzelbaumvolumen (Schaftholzvolumen, geschätzt mit der Schaftholzfunktion: v=f (d13, d7, hoehe).)

Die Publikation der Ergebnisse, zu deren Erarbeitung die gelieferten Daten herangezogen wurden, ist mit folgendem Hinweis auf die Datenherkunft zu versehen:
WSL, 2022: Schweizerisches Landesforstinventar LFl. Daten der Erhebungen 2004-06 und 2009-17. Meinrad Abegg. Eidg. Forschungsanstalt WSL, Birmensdorf.
';
