import time
from datetime import date
from decimal import Decimal

from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Point

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait

from gemeinde.models import Gemeinde
from mobile.tests.base import MobileSeleniumTestsBase
from ksp_so.models import (
    Inventory, Plot, PlotObs, Relief, SurveyType, Tree, TreeObs, TreeSpecies,
    Vita
)

this_year = date.today().year


class ProjectDataMixin:
    @classmethod
    def setUpTestData(cls):
        cls.user = get_user_model().objects.create_user(username='user', password='secret')
        plot_point = Point(2626487, 1241534, srid=2056)
        gemeinde = Gemeinde.objects.get(name='Egerkingen')
        cls.inventory = Inventory.objects.create(
            name="Test inventory",
            team="Super Team",
            municipality=gemeinde,
            period=2,
            default_density=20000,
            inv_from=date(year=date.today().year, month=1, day=1),
            inv_to=date(year=date.today().year, month=12, day=31),
        )
        plot = Plot.objects.create(
            nr=1, sealevel=263, slope=70, exposition='N',
            phytosoc=None,  #models.Phytosoc.objects.get(pk=74),
            the_geom=plot_point,
        )
        plotobs = PlotObs.objects.create(
            plot=plot, year=2013, inv_team=cls.inventory, area=1, evaluation_unit='33',
            forest_clearing=False, subsector='VA', forest_edgef='1.0', gwl=5147.27,
            municipality=gemeinde,
            relief=Relief.objects.get(pk=4),
        )
        '''
            owner_type=models.OwnerType.objects.get(pk=4),
            sector=models.Sector.objects.get(pk=2),
            forest_form=models.ForestForm.objects.get(pk=4),
            acidity=models.Acidity.objects.get(pk=2),
            region=models.Region.objects.get(pk=1),
            regen_type=models.RegenType.objects.get(pk=1),
            soil_compaction=models.SoilCompaction.objects.get(pk=1),
            stand_forest_mixture=models.ForestMixture.objects.get(code=4), # 90-100% LbH (Laubholz)
            stand_devel_stage=models.DevelStage.objects.get(code="1"), # Jungwuchs/Dickung (BHD<12cm)
            stand_structure=models.StandStructure.objects.get(pk=2),
            stand_crown_closure=models.CrownClosure.objects.get(code=5), # räumig bis aufgelöst
            gap=models.Gap.objects.get(pk=1),
        '''
        tree1 = Tree.objects.create(
            plot=plot, nr=1, azimuth=29, distance=41, spec=TreeSpecies.objects.get(species="Buche"))
        tree2 = Tree.objects.create(
            plot=plot, nr=2, azimuth=229, distance=92, spec=TreeSpecies.objects.get(species="Bergahorn und Spitzahorn"))
        tree3 = Tree.objects.create(
            plot=plot, nr=3, azimuth=0, distance=0, spec=TreeSpecies.objects.get(species="Tanne"))
        TreeObs.objects.create(
            tree=tree1, obs=plotobs, dbh=25, vita=Vita.objects.get(code='e'),
        )
        TreeObs.objects.create(
            tree=tree2, obs=plotobs, dbh=32, vita=Vita.objects.get(code='l'),
        )
        TreeObs.objects.create(
            tree=tree3, obs=plotobs, dbh=15, vita=Vita.objects.get(code='c'),
        )
        SurveyType.objects.create(code='K', description='Kontrollstichprobenaufnahme')


class MobileSeleniumTestsSO(ProjectDataMixin, MobileSeleniumTestsBase):
    fixtures = ['gemeinde_so_tests', 'tree_species', 'vita', 'relief']

    def new_observation(self):
        # Click on current year button
        self.selenium.find_element(By.CLASS_NAME, 'new').click()
        self.selenium.find_element(By.ID, "input-start").click()
        ####### STEP 1 ########
        self.assertEqual(
            Select(self.selenium.find_element(By.ID, "id_municipality")).first_selected_option.text,
            "4622 Egerkingen"
        )
        self.selenium.find_element(By.ID, "forest_edgef_h").send_keys("5")
        self.assertEqual(self.selenium.find_element(By.ID, "id_forest_edgef").get_attribute('value'), '0.8')
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step1"]//button[@type="submit"]'))
        ).click()
        ####### STEP 2 ########
        # Freeze current coordinates from GPS
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, "currentLong"), '.')
        )
        self.selenium.find_element(By.ID, "keepthis").click()
        # Check radio values
        self.assertEqual(
            self.selenium.find_element(By.ID, "id_exposition_0").get_attribute('value'),
            ''
        )
        self.assertFalse(self.selenium.find_element(By.ID, "id_exposition_0").is_selected())
        self.assertEqual(
            self.selenium.find_element(By.ID, "id_exposition_2").get_attribute('value'), 'N')
        self.assertTrue(self.selenium.find_element(By.ID, "id_exposition_2").is_selected())
        self.assertFalse(self.selenium.find_element(By.ID, "id_exposition_1").is_selected())

        self.assertTrue(self.selenium.find_element(By.ID, "id_relief_4").is_selected())
        self.selenium.find_element(By.ID, "id_relief_3").click()

        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step2"]//button[@type="submit"]'))
        ).click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step3"]//button[@type="submit"]'))
        ).click()
        self.selenium.find_element(By.XPATH, '//*[@id="trees_summary"]/tbody/tr[1]/td[1]').click()
        ####### Tree forms (2) #######
        self.assertFalse(self.selenium.find_element(By.ID, "id_species").is_enabled())
        self.assertFalse(self.selenium.find_element(By.ID, "id_nr").is_enabled())
        self.assertFalse(self.selenium.find_element(By.ID, "id_distance").is_enabled())
        self.assertFalse(self.selenium.find_element(By.ID, "id_azimuth").is_enabled())
        self.assertEqual(self.selenium.find_element(By.ID, "id_dbh").get_attribute('value'), "")
        self.assertFalse(self.selenium.find_element(By.ID, "tr_stem_height").is_displayed())
        # A warning is displayed when the value is <= than previous obs.
        dbh_el = self.selenium.find_element(By.ID, "id_dbh")
        dbh_warning_el = self.selenium.find_element(By.ID, "dbh_warning")
        self.assertFalse(dbh_warning_el.is_displayed())
        dbh_el.send_keys("24")
        self.assertTrue(dbh_warning_el.is_displayed())
        dbh_el.clear()
        dbh_el.send_keys("25")
        self.assertTrue(dbh_warning_el.is_displayed())
        dbh_el.clear()
        dbh_el.send_keys("26")
        self.assertFalse(dbh_warning_el.is_displayed())
        # When choosing the "(c)" vita, dbh is set to 0 and disabled, and selects are set empty and disabled.
        Select(self.selenium.find_element(By.ID, "id_vita")
            ).select_by_visible_text("Probebaum wurde genutzt (c)")
        self.assertFalse(self.selenium.find_element(By.ID, "id_dbh").is_enabled())
        self.assertEqual(self.selenium.find_element(By.ID, "id_dbh").get_attribute('value'), "0")
        sel = self.selenium.find_element(By.ID, 'id_rank')
        self.assertFalse(sel.is_enabled(), "id_rank should not be enabled")
        try:
            self.assertEqual(Select(sel).all_selected_options[0].get_attribute('value'), "")
        except IndexError:
            pass

        # When set to "(m)", stem height is visible.
        Select(self.selenium.find_element(By.ID, "id_vita")
            ).select_by_visible_text("Probebaum ist ein Dürrständer (m)")
        self.assertTrue(self.selenium.find_element(By.ID, "tr_stem_height").is_displayed())

        # When back to "(l)", dbh is editable again.
        Select(self.selenium.find_element(By.ID, "id_vita")
            ).select_by_visible_text("Probebaum lebt (l)")
        self.assertTrue(self.selenium.find_element(By.ID, "id_dbh").is_enabled())
        Select(self.selenium.find_element(By.ID, "id_vita")
            ).select_by_visible_text("Probebaum wurde genutzt (c)")
        self.assertFalse(self.selenium.find_element(By.ID, "tr_stem_height").is_displayed())
        self.selenium.find_element(By.XPATH, '//*[@id="steptree"]//button[@type="submit"]').click()

        # Second tree form
        # Go back to previous obs (e.g. to see some value) and come back
        self.selenium.find_element(By.ID, "button-2013").click()
        self.selenium.find_element(By.ID, "button-%d" % date.today().year).click()

        self.selenium.find_element(By.XPATH, '//*[@id="trees_summary"]/tbody/tr[2]/td[1]').click()
        self.assertEqual(self.selenium.find_element(By.ID, "id_dbh").get_attribute('value'), "")
        self.selenium.find_element(By.ID, "id_dbh").send_keys("38")
        Select(self.selenium.find_element(By.ID, "id_vita")).select_by_visible_text("Probebaum lebt (l)")
        self.selenium.find_element(By.XPATH, '//*[@id="steptree"]//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//button[@class="newtree"]'))
        ).click()

        ####### New tree form ########
        tree_form = self.selenium.find_element(By.ID, "steptree")
        self.assertTrue(tree_form.find_element(By.ID, "id_species").is_enabled())
        # Next number is automatically attributed
        self.assertEqual(tree_form.find_element(By.ID, "id_nr").get_attribute('value'), "3")
        self.send_caliper_values(species='Ta', diameter=141, distance=240)
        tree_form.find_element(By.ID, "id_azimuth").send_keys("320")
        Select(tree_form.find_element(By.ID, "id_vita")).select_by_visible_text(
            "Probebaum ist eingewachsen (e)")
        tree_form.find_element(By.ID, "id_remarks").send_keys("Bemerkung.")
        self.selenium.find_element(By.XPATH, '//*[@id="steptree"]//button[@type="submit"]').click()

        # It's possible to go back to an already handled tree and see the entered values
        self.selenium.find_element(By.XPATH, '//*[@id="trees_summary"]/tbody/tr[2]/td[1]').click()
        self.assertEqual(
            self.selenium.find_element(By.ID, "id_dbh").get_attribute('value'), "38")
        self.assertNotIn(
            "Probebaum ist eingewachsen (e)", self.active_select_options("id_vita")
        )
        self.selenium.find_element(By.ID, 'canceltree').click()

        # Going back to a new tree also possible (delete button still visible)
        self.selenium.find_element(By.XPATH, '//*[@id="trees_summary"]/tbody/tr[3]/td[1]').click()
        self.assertTrue(self.selenium.find_element(By.ID, "canceltree").is_displayed())
        vita_opts = self.active_select_options("id_vita")
        self.assertIn("Probebaum ist eingewachsen (e)", vita_opts)
        self.assertNotIn("Probebaum ist nicht mehr auffindbar (x)", vita_opts)
        self.selenium.find_element(By.XPATH, '//*[@id="steptree"]//button[@type="submit"]').click()

        self.selenium.find_element(By.ID, "input-end").click()
        self.selenium.find_element(By.ID, "confirm-ok").click()

    def test_new_observation_online(self):
        self.login()
        self.go_to_inventory("Test inventory")
        self.go_to_plotobs()
        self.assertEqual(
            self.selenium.find_element(By.XPATH, '//*[@id="info-municipality"]/td[2]').text,
            "4622 Egerkingen"
        )
        self.new_observation()
        self.assertEqual(
            self.selenium.execute_script("return app.currentPlot.obs[%s].year;" % this_year),
            this_year
        )
        # Test new PlotObs/Tree/TreeObs presence
        time.sleep(1)  # FIXME: use some other mean to detect saving is done
        plot = Plot.objects.get(nr=1)
        self.assertIsNotNone(plot.point_exact)
        self.assertEqual(plot.plotobs_set.count(), 2)
        po = plot.plotobs_set.get(year=this_year)
        self.assertEqual(po.municipality, Gemeinde.objects.get(name='Egerkingen'))
        self.assertEqual(po.forest_edgef, Decimal('0.8'))
        self.assertEqual(po.relief, Relief.objects.get(description='Mittelhang'))
        self.assertEqual(po.treeobs_set.count(), 3)
        self.assertEqual(po.inv_team.name, "Test inventory")
        self.assertEqual(
            sorted(po.treeobs_set.values_list('tree__nr', flat=True)),
            [1, 2, 3]
        )
        tree1, tree2, tree3 = po.treeobs_set.order_by('tree__nr')
        self.assertEqual(tree1.survey_type, SurveyType.objects.get(code='K'))
        self.assertEqual(tree1.vita.code, "c")
        self.assertEqual(tree2.vita.code, "l")
        self.assertEqual(tree3.remarks, "Bemerkung.")
        self.assertEqual(tree3.vita.code, "e")

    def test_new_observation_overwrite_species(self):
        self.login()
        self.go_to_inventory("Test inventory")
        self.go_to_plotobs()
        self.selenium.find_element(By.CLASS_NAME, 'new').click()
        self.selenium.find_element(By.ID, "input-start").click()
        for step in ['step1', 'step2', 'step3']:
            WebDriverWait(self.selenium, 4).until(
                EC.visibility_of_element_located((By.XPATH, f'//*[@id="{step}"]//button[@type="submit"]'))
            ).click()
        self.selenium.find_element(By.XPATH, '//*[@id="trees_summary"]/tbody/tr[1]/td[1]').click()
        self.assertEqual(
            Select(self.selenium.find_element(By.ID, "id_species")).first_selected_option.text,
            "Buche"
        )
        self.send_caliper_values(species='Ta', diameter=141, distance=240)
        time.sleep(0.5)
        # Warning species
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.ID, 'confirm-ok'))
        ).click()
        # Warning distance
        time.sleep(0.5)
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.ID, 'confirm-ok'))
        ).click()
