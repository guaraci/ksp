from django.test import TestCase

from ksp_so.models import Plot


class GeneralTests(TestCase):
    def test_plot_radius(self):
        self.assertEqual(Plot(slope=15).radius, 9.77)
        self.assertEqual(Plot(slope=20).radius, 9.87)
        self.assertEqual(Plot(slope=25).radius, 9.92)
