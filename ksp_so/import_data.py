"""
Example:
PYTHONPATH=. python ksp_so/import_data.py --project=leberberg
"""
import chardet
import logging
import math
import os
import sys
from argparse import ArgumentParser
from dataclasses import dataclass, field
from datetime import date
from decimal import Decimal
from functools import cached_property
from pathlib import Path

import django
from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from django.db import connection, transaction
import tablib

growth_query = '''
SELECT
    avg(tobsa.dbh - tobsb.dbh)
   FROM tree_obs tobsa
     JOIN tree_obs tobsb ON tobsa.tree_id = tobsb.tree_id AND tobsa.id != tobsb.id
     LEFT JOIN plot_obs poa ON tobsa.obs_id = poa.id
     LEFT JOIN inventory inv1 ON poa.inv_team_id = inv1.id
     LEFT JOIN lt_vita vitaa ON tobsa.vita_id = vitaa.id
     LEFT JOIN lt_vita vitab ON tobsb.vita_id = vitab.id
  WHERE vitaa.code='l' AND (vitab.code = ANY (ARRAY['e','l'])) AND inv1.id=%s;
'''

@dataclass
class DataDef:
    key: str
    name: str
    prefix: int  # prefix to add to plot as plot nr is not unique
    year: int
    year_old: int
    density: int
    headers: dict
    file_plots: str
    files_trees: list
    file_coords: str = ''
    file_durre: str = ''
    density_old: int = None
    owner_map: dict = field(default_factory=dict)
    dbh_1_tot_stehend: bool = False
    dbh_2_tot_liegend: bool = False
    existing_trees:bool = False

    def __post_init__(self):
        # Only run for definitions created by DataDef(...), not for subclasses.
        # Transform file path strings to pathlib.Path instances
        self.file_coords = Path(self.file_coords) if self.file_coords else None
        if isinstance(self.file_plots, str):
            self.file_plots = Path(self.file_plots)
        self.files_trees = [
            Path(path) if isinstance(path, str) else path
            for path in self.files_trees
        ]
        self.file_durre = Path(self.file_durre) if self.file_durre else None

    def get_inventory(self, old=False):
        from ksp_so.models import Inventory

        year = self.year_old if old else self.year
        if year is None:
            return None
        year = int(year)
        existing = list(Inventory.objects.filter(name=self.name).order_by('inv_from'))
        density = self.density_old if old and self.density_old else self.density
        try:
            inv = [inv for inv in existing if inv.inv_from.year==year][0]
        except IndexError:
            if existing and year < existing[0].inv_from.year:
                ordering = existing[0].ordering - 1
            elif existing and year > existing[-1].inv_from.year:
                ordering = existing[-1].ordering + 1
            else:
                ordering = 2
            inv = Inventory.objects.create(
                name=self.name,
                team=self.name,
                period=2 if year < 2015 else 3,
                ordering=ordering,
                inv_from=date(year, 1, 1),
                inv_to=date(year, 12, 31),
                municipality=None,
                default_density=density,
            )
        return inv

    def before_plot_import(self):
        # DELETE ALL PLOTS FOR THIS INVENTORY AT EACH RUN
        if self.year_old is not None:
            Plot.objects.filter(nr__startswith=f'{self.prefix}-').delete()
        else:
            # Only delete PlotObs for this import file
            PlotObs.objects.filter(plot__nr__startswith=f'{self.prefix}-', year=self.year).delete()

    def has_prev_obs_from_line(self, line):
        if str(line.get('Zneu')) == '1':  # At least for Kleinlützel
            return False
        return True

    def get_val(self, line, header, default=None):
        integer_headers = [
            'dbh_new', 'dbh_old', 'baumart', 'bestand', 'bestand_old', 'dis_rand', 'azimuth',
            'distance', 'radius', 'bestand', 'soziologie', 'zuwachs_code',
        ]
        boolean_headers = ['markiert']
        for _head in self.headers.get(header, []):
            if isinstance(_head, tuple):
                # Get col[0], and fallback to col[1] if not found
                value = line.get(_head[0])
                if value is None or value in ('', '.', '-'):
                    value = line.get(_head[1])
            elif _head in line:
                value = line[_head]
            else:
                continue
            if header in integer_headers:
                if value in ('.', '-', '/', '?'):
                    return default
                try:
                    return int(value)
                except (ValueError, TypeError):
                    if header == 'bestand':
                        continue
            elif header in boolean_headers:
                return value in ['x', 'M', 'm']
            return value

        if default == '__raise__':
            raise KeyError(f"No value found for '{header}'")
        return default

    @cached_property
    def coords_index(self):
        coords_data = get_tablib_data(self.file_coords)
        return {str(self.get_val(line, 'probe_num')): line for line in coords_data.dict}

    def get_point(self, line) -> tuple[Point, Point]:
        coords_line = None
        if self.file_coords:
            probe_num = self.get_val(line, 'probe_num', default='__raise__')
            coords_index = self.coords_index
            if str(probe_num) in coords_index:
                coords_line = coords_index[str(probe_num)]
            elif '..PNR' in line and str(line['..PNR']) in coords_index:
                coords_line = coords_index[str(line['..PNR'])]
            else:
                # Carefully chek this hack
                nr = str(int(str(probe_num)[-3:]))
                if nr in coords_index:
                    coords_line = coords_index[nr]
        else:
            coords_line = line
        x, y = to_num(self.get_val(coords_line, 'plot_x')), to_num(self.get_val(coords_line, 'plot_y'))
        x_exact, y_exact = to_num(self.get_val(coords_line, 'plot_x_exact')), to_num(self.get_val(coords_line, 'plot_y_exact'))

        def xy_to_point(x, y):
            if x is None or x in (0, '0', '', '.', '-', '#NV', '#N/A'):
                return None
            if isinstance(x, str):
                x = float(x) if '.' in x else int(x)
            if isinstance(y, str):
                y = float(y) if '.' in y else int(y)
            srid = 2056 if int(x) > 1000000 else (21781 if int(x) > 10000 else 4326)
            pt = Point(x=x, y=y, srid=srid)
            if srid != 2056:
                pt.transform(2056)  # query_elevation expects 2056
            return pt

        pt_exact = xy_to_point(x_exact, y_exact)
        if pt_exact:
            precision = self.get_val(line, 'precision')
            pt_exact.precision = Decimal(precision) if precision not in ('', None, '.') else None
        return xy_to_point(x, y) or pt_exact, pt_exact

    def cleanup(self):
        """Any data-specific cleanup to do after imports."""
        pass


@dataclass
class MittleresGauDef(DataDef):
    def __init__(self):
        self.key = 'mittleresgau'
        self.name = 'Mittleres Gäu'
        self.prefix = 13
        self.year = 2019
        self.year_old = 2006
        self.density = 12000
        self.file_plots=Path('ksp_so/data-mittleresgau/PD2019.xlsx')
        self.files_trees=[Path('ksp_so/data-mittleresgau/BD2019.xlsx')]
        self.headers = {
            'probe_num': ['PBFNR.'],
            'bestand_old': ['BT'],
            'bestand': ['BTneu'],
            'radius': [('RAD\'', 'RAD')],
            'plot_x': ['GPS-X'],
            'plot_y': ['GPS-Y'],
            'dis_rand': ['Dis>R'],
            'plot_remarks': ['Lage_PF', 'Randart', 'Bemerkung'],
            'dbh_new': ['BHDneu'],
            'dbh_old': [('BHD\'', 'BHD')],
            'baumart': [('BA\'', 'BA')],
            'baum_num': [],
            'azimuth': [('AZI\'', 'AZI')],
            'distance': [('DIS\'', 'DIS')],
            'soziologie': ['SOZ'],
            'tree_remarks': ['UeBA', 'ZUSATZTEXT', 'Kommentar_Erfa']
        }
        self.owner_map = {
            '210': 'NEUE.BG',
            '220': 'HARK.BG',
        }

    def cleanup(self):
        # Probe fehlt in Formular
        PlotObs.objects.filter(plot__nr__in=[f'{self.prefix}-220008', f'{self.prefix}-210009'], year=2019).delete()


@dataclass
class UnteresGauDef(DataDef):
    def __init__(self):
        self.key = 'unteresgau'
        self.name = 'Unteres Gäu'
        self.prefix = 12
        self.year = 2017
        self.year_old = 2005
        self.density = 19200
        self.file_plots = Path('ksp_so/data-unteresgau/Probedaten.xlsx')
        self.files_trees = [Path('ksp_so/data-unteresgau/Baumdaten.xlsx')]
        self.headers={
            'probe_num': ['PBFNR.'],
            'plot_x': ['xkoord'],
            'plot_y': ['ykoord'],
            'plot_x_exact': ['GPS-X'],
            'plot_y_exact': ['GPS-Y'],
            'precision': ['dGPS'],
            'bestand_old': ['BT'],
            'bestand': ['BTneu'],
            #'eigentumer': ['Eigentümer'],
            'radius': ['RAD', "RAD'"],
            'dis_rand': ['Dis>R'],
            'plot_remarks': ['LagePF', 'Bemerk_BTneu', 'Randart', 'Bemerkung', 'Kommentar_Erfa'],

            'dbh_new': ['BHDneu'],
            'dbh_old': [('BHD\'', 'BHD')],
            'baumart': [('BA\'', 'BA')],
            'azimuth': [('AZI\'', 'AZI')],
            'distance': [('DIS\'', 'DIS')],
            'soziologie': ['SOZ'],
            'markiert': ['M'],
            'tree_remarks': ['UeBA', 'ZUSATZTEXT', 'Kommentar_Erfa'],
        }
        self.owner_map={
            '110': 'SW-Uwald',
            '120': 'HAEGE.BG',
            '140': 'RICK.GE',
            '150': 'WANG.BG',
            '230': 'KAPP.BG',
            '240': 'RICK.GE',
            '250': 'WANG.BG',
        }

    def cleanup(self):
        # Not inventories in 2017
        PlotObs.objects.filter(plot__nr__startswith=f'{self.prefix}-110', year=2017).delete()


@dataclass
class OberesGauDef(DataDef):
    def __init__(self):
        self.key = 'oberesgau'
        self.name = 'Oberes Gäu'
        self.prefix = 19
        self.year = 2017
        self.year_old = 2007
        self.existing_trees = True
        self.density = 12000
        self.file_plots = Path('ksp_so/data-oberesgau/OberesGau-PD.xlsx')
        self.files_trees = [Path('ksp_so/data-oberesgau/OberesGau-BD.xlsx')]
        self.headers = {
            # PD
            'probe_num': ['PBFNR'],
            'bestand_old': ['BT'],
            'bestand': ['BTneu'],
            'eigentumer': ['owner'],
            'radius': ['RAD', "RAD'"],
            'dis_rand': ['Dis>R'],
            'plot_remarks': ['LagePF', 'RandArt', 'Bemerkung', 'Cmt_Erfa'],
            'plot_x': ['Soll-X'],
            'plot_y': ['Soll-Y'],
            'plot_x_exact': ['GPS-X'],
            'plot_y_exact': ['GPS-Y'],
            'precision': ['dGPS'],
            # BD
            'baumart': [('BA\'', 'BA')],
            'azimuth': [('AZI\'', 'AZI')],
            'distance': [('DIS\'', 'DIS')],
            'dbh_old': [('BHD\'', 'BHD')],
            'dbh_new': ['BHDneu'],
            'soziologie': ['SOZ'],
            'markiert': ['M'],
            'tree_remarks': ['UeBA', 'ZUSATZTEXT', 'Kommentar_Erfa'],
        }

    def has_prev_obs_from_line(self, line):
        return str(line.get(2007)) == '1'

    def before_plot_import(self):
        # Only delete PlotObs > 2000
        PlotObs.objects.filter(plot__nr__startswith=f'{OberesGauDef().prefix}-', year__gt=2000).delete()


@dataclass
class OberesGau97Def(DataDef):
    # To be imported AFTER 'oberesgau'
    def __init__(self):
        self.key = 'oberesgau97'
        self.name = 'Oberes Gäu'
        self.prefix = 19  # Same as OberesGauDef
        self.year = 1997
        self.year_old = None
        self.density = 12000
        self.file_coords = Path('ksp_so/data-oberesgau/OberesGau-Coords.xlsx')
        self.file_plots = (Path('ksp_so/data-oberesgau/1997_Daten_import.xlsx'), 1)
        self.files_trees = [(Path('ksp_so/data-oberesgau/1997_Daten_import.xlsx'), 2)]
        self.headers = {
            # Coords file
            'plot_x': ['Soll-X'],
            'plot_y': ['Soll-Y'],
            'plot_x_exact': ['GPS-X'],
            'plot_y_exact': ['GPS-Y'],
            'precision': ['dGPS'],
            # PD
            'probe_num': ['PbfNr'],
            'radius': ['Radius'],
            'bestand': ['BT 1997'],
            'eigentumer': ['waeig'],
            #'radius': ['Radius'],
            # BD
            'baumart': ['BA'],
            'azimuth': ['Az'],
            'distance': ['Dis'],
            'dbh_new': ['BHD1997'],
            'markiert': ['M'],
        }


REGION_DATA = {
    'leberberg': DataDef(
        key='leberberg',
        name='Leberberg',
        prefix=10,
        year=2018,
        year_old=2004,
        density=9600,
        file_plots='ksp_so/data-leberberg/PD2018.xlsx',
        files_trees=['ksp_so/data-leberberg/BD2018.xlsx'],
        file_durre='ksp_so/data-leberberg/durre.csv',
        headers={
            'rec': ['rec'],
            'plot_x': ['xkoord'],
            'plot_y': ['ykoord'],
            'probe_num': ['pnr', '...PBF', 'sort1', 'PBFNR.', 'tree.plot_pfnr'],
            'radius': ['.RAD'],
            'eigentumer': ['we_neu'],
            'bestand_old': ['...BTold'],
            'bestand': ['...BT', 'BTneu'],
            'einheit': ['AE'],
            'dbh_new': ['BHN', 'tree_obs.dbh'],
            'dbh_old': ['BHA'],
            'baumart': ['BA', 'tree.spec_id'],
            'baum_num': ['..BNR'],
            'azimuth': ['AZI', 'tree.azimuth'],
            'distance': ['DIS', 'tree.distance'],
        },
    ),
    'hinteresthal': DataDef(
        key='hinteresthal',
        name='Hinteres Thal',
        prefix=11,
        year=2018,
        year_old=2008,
        density=19200,
        file_coords='ksp_so/data-hinteresthal/04-herb_ktSo_2018_PF_import.csv',
        file_plots='ksp_so/data-hinteresthal/Probe2018.xlsx',
        files_trees=['ksp_so/data-hinteresthal/Baum2018.xlsx'],
        headers={
            'rec': ['ErfaNr'],
            'probe_num': ['...PBF', 'PBFNR.', 'tree.plot_pfnr'],
            'plot_x': ['GPS-X'],
            'plot_y': ['GPS-Y'],
            'radius': ['RAD'],
            'eigentumer': ['WCODE'],
            'bestand_old': ['BT'],
            'bestand': ['...BT', 'BTneu'],
            'dis_rand': ['Dis>R'],
            'plot_remarks': ['Bemerk_BTneu', 'Lage_PF', 'Randart', 'Bemerkung', 'Kommentar_Erfa'],
            'einheit': ['AE'],
            'dbh_new': ['BHDneu'],
            'dbh_old': [('BHD\'', 'BHD')],
            'baumart': [('BA\'', 'BA')],
            'baum_num': [],
            'azimuth': [('AZI\'', 'AZI')],
            'distance': [('DIS\'', 'DIS')],
            'soziologie': ['SOZ'],
            'tree_remarks': ['UeBA', 'Kom', 'Kommentar_Erfa'],
        },
    ),
    'hinteresthal2': DataDef(
        key='hinteresthal2',
        name='Hinteres Thal',  # same name as 'hinteresthal' def so that data goes in same inventory
        prefix=20,
        year=2018,
        year_old=2007,
        density=19200,
        file_plots='ksp_so/data-hinteresthal/Probe2018-AEDE.xlsx',
        files_trees=['ksp_so/data-hinteresthal/Baum2018-AEDE.xlsx'],
        headers={
            'rec': ['ErfaNr'],
            'probe_num': ['PBFNR.'],
            'plot_x': ['GPS-X'],
            'plot_y': ['GPS-Y'],
            'radius': ['RAD'],
            'eigentumer': ['WCODE'],
            'bestand_old': ['BT'],
            'bestand': ['BTneu'],
            'dis_rand': ['Dis>R'],
            'plot_remarks': ['Bemerk_BTneu', 'Lage_PF', 'Randart', 'Bemerkung'],

            'azimuth': [('AZI\'', 'AZI')],
            'distance': [('DIS\'', 'DIS')],
            'baumart': [('BA\'', 'BA')],
            'dbh_old': [('BHD\'', 'BHD')],
            'dbh_new': ['BHDneu'],
            'soziologie': ['SOZ'],
            'markiert': ['M'],
            'tree_remarks': ['UeBA', 'ZUSATZTEXT', 'Kommentar_Erfa'],
        },
    ),
    'unteresgau': UnteresGauDef(),
    'mittleresgau': MittleresGauDef(),
    'mittleresgau2': DataDef(
        key='mittleresgau2',
        name='MittleresGäu-2',
        prefix=22,
        year=2019,
        year_old=2008,
        density=12000,
        file_plots = (Path('ksp_so/data-mittleresgau/20220914_invdaten_LD_2019_BonFulGun_import.xlsx'), 1),
        files_trees = [(Path('ksp_so/data-mittleresgau/20220914_invdaten_LD_2019_BonFulGun_import.xlsx'), 2)],
        headers={
            # PD
            'probe_num': ['PbfNr'],
            'zuwachs_code': ['Zc'],
            'eigentumer': ['waeig'],
            'bestand_old': ['BTalt'],
            'bestand': ['Btneu'],
            'plot_x': ['GPS_X'],
            'plot_y': ['GPS_Y'],
            'radius': ['Radius'],
            'dis_rand': ['Dist>R'],
            'plot_remarks': ['Bemerkung'],
            # BD
            'baumart': ['BA'],
            'azimuth': ['Az'],
            'distance': ['Dis'],
            'dbh_old': ['BHD'],
            'dbh_new': ['BHDneu'],
            'soziologie': ['Soz'],
            'markiert': ['M'],
            'tree_remarks': ['Bemerkung'],
        },

    ),
    'oberesgau': OberesGauDef(),
    'oberesgau97': OberesGau97Def(),
    'mumliswil': DataDef(
        key='mumliswil',
        name='Mümliswil',
        prefix=14,
        year=2020,
        year_old=2009,
        density=19200,
        file_plots='ksp_so/data-mumliswil/mümliswil 2020 lubor_RH_import_pd.csv',
        files_trees=['ksp_so/data-mumliswil/mümliswil 2020 lubor_RH_import_bd.csv'],
        headers={
            'probe_num': ['...PBF', 'PBFNR'],
            'bestand': ['BT-Lubor', 'BT-Bestandeskarte', '...BT'],
            'eigentumer': ['...WC'],
            'plot_x': ['GPS-X'],
            'plot_y': ['GPS-Y'],
            'radius': [('RAD\'', 'RAD')],
            'einheit': ['AE'],
            'dis_rand': ['Dis>R'],
            'plot_remarks':['Randart', 'Bemerk_PF', 'Kommentar_Erfa'],
            'dbh_new': ['BHDneu'],
            'dbh_old': [('BHD\'', 'BHD')],
            'baumart': [('BA\'', 'BA')],
            'baum_num': ['..BNR'],
            'azimuth': [('AZI\'', 'AZI')],
            'distance': [('DIS\'', 'DIS')],
            'soziologie': ['SOZ'],
            'tree_remarks': ['UeBA', 'Kommentar_Erfa'],
        },
        owner_map={
            '110': 'SO411',
        },
    ),
    'solothurn1': DataDef(
        key='solothurn1',
        name='Solothurn Rev 1-5',
        prefix=15,
        year=2013,
        year_old=2001,
        density=9579,
        file_plots='ksp_so/data-solothurn1/Coord_combi.csv',
        files_trees=[
            'ksp_so/data-solothurn1/04-INV_SOLO.BG-2012-14_pdbd(2)_lubor#20130826-1743_rev5-korr_rh_revier5_baumdaten.csv',
            'ksp_so/data-solothurn1/05-INV_SOLO.BG-2012-14_pdbd(3)_lubor#20130916-1524_rev4_rh_revier4_baumdaten.csv',
            'ksp_so/data-solothurn1/06-INV_SOLO.BG-2012-14_pdbd_lubor#20130820-1811_rev1-3_Rh_revire1-3_baumdaten.csv',
        ],
        headers={
            'probe_num': ['PBFNR'],
            'bestand_old': ['BTalt'],
            'bestand': ['BTneu'],
            #'eigentumer': ['WCODE'],
            'plot_x': ['coord1'],
            'plot_y': ['coord2'],
            'radius': ['RAD'],
            'dis_rand': ['Dis>Rand'],
            'plot_remarks': ['Lage_Probe', 'BEMERKUNGneu'],
            'baum_num': ['BNR'],
            'azimuth': ['AZI'],
            'distance': ['DIS'],
            'baumart': ['BA'],
            'dbh_new': ['BHDneu'],
            'dbh_old': ['BHD'],
            'tree_remarks': ['BEMERKUNGneu', 'kommentar_erfa'],
        },
        owner_map={
            '225': 'SOLO.BG',
            '214': 'SOLO.BG',
            '211': 'SOLO.BG',
            '233': 'SOLO.BG',
        },
    ),
    'solothurn2': DataDef(
        key='solothurn2',
        name='Solothurn Rev 6-7',
        prefix=16,
        year=2015,
        year_old=1989,
        density=9743,
        file_plots='ksp_so/data-solothurn2/03-_bgs_rev6-7_daten91_190902-1213_RH_revier7+6_probedaten.csv',
        files_trees=[
            'ksp_so/data-solothurn2/03-_bgs_rev6-7_daten91_190902-1213_RH_revier6_baumdaten.csv',
            'ksp_so/data-solothurn2/03-_bgs_rev6-7_daten91_190902-1213_RH_revier7_baumdaten.csv',
        ],
        headers={
            'probe_num': ['PBFNR'],
            'bestand_old': ['BTalt'],
            'bestand': ['BTneu'],
            'eigentumer': ['WCODE'],
            'plot_x': ['gps-x'],
            'plot_y': ['gps-y'],
            'radius': [('RAD\'', 'RAD')],
            'plot_remarks': ['Lage_Probe', 'BEMERKneu', 'cmt_erfa', 'cmt_dvb'],
            'dis_rand': ['Dis>Rand'],
            'baum_num': ['BNR'],
            'baumart': [('BA\'', 'BA')],
            'azimuth': [('AZI\'', 'AZI')],
            'distance': [('DIS\'', 'DIS')],
            'dbh_new': [('TOTBHDN', 'BHDN')],
            'dbh_old': ['BHDA'],
            'tree_remarks': ['BEMERKneu'],
        },
    ),
    'kleinlutzel': DataDef(
        key='kleinlutzel',
        name='Kleinlützel',
        prefix=17,
        year=2014,
        year_old=1995,
        density=19200,
        file_coords='ksp_so/data-kleinlutzel/Kleinluetzel_GPS_V2.csv',
        file_plots='ksp_so/data-kleinlutzel/00-INV@kluet-14_datenerfaluborok.xlsx',
        files_trees=['ksp_so/data-kleinlutzel/Kleinlutzel-BD.xlsx'],
        headers={
            'probe_num': ['PBFNR'],
            'plot_x': ['X'],
            'plot_y': ['Y'],
            'rec': ['recnr'],
            'bestand_old': ['BT'],
            'bestand': ['BTneu'],
            'radius': ['RAD'],
            'dis_rand': ['Dist>R'],
            'plot_remarks': ['Lage_Probe', 'Bemerkungen'],
            'baumart': [('BA\'', 'BA')],
            'azimuth': [('AZI\'', 'AZI')],
            'distance': [('DIS\'', 'DIS')],
            'dbh_old': ['BHD'],
            'dbh_new': ['BHDneu'],
            'soziologie': ['SOZ'],
            'tree_remarks': ['üBa', 'BEMERKUNGneu', 'Kommentar_Erfassung'],
        },
        owner_map={
            '110': 'KLUET.BG',
        },
    ),
    'egerkingen': DataDef(
        key='egerkingen',
        name='Egerkingen',
        prefix=18,
        year=2011,
        year_old=2001,
        density=19485,
        file_plots='ksp_so/data-egerkingen/Inv_PD_BD_Eger_2011_import-PD.xlsx',
        files_trees=['ksp_so/data-egerkingen/Inv_PD_BD_Eger_2011_import-BD.xlsx'],
        headers={
            # PD
            'probe_num': ['PbfNr'],
            'eigentumer': ['waeig'],
            'bestand_old': ['BTalt'],
            'bestand': ['Btneu'],
            'plot_x': ['GPS_X'],
            'plot_y': ['GPS_Y'],
            'radius': ['Radius'],
            'plot_remarks': ['Bemerkung'],
            # BD
            'baumart': ['BA'],
            'azimuth': ['Az'],
            'distance': ['Dis'],
            'dbh_old': ['BHD'],
            'dbh_new': ['BHDneu'],
            'markiert': ['M'],
            'tree_remarks': ['Bemerkung'],
        },
    ),
    'egerkingen22': DataDef(
        key='egerkingen22',
        name='Egerkingen',
        prefix=18,
        year=2022,
        year_old=None,
        density=19485,
        file_plots = (Path('ksp_so/data-egerkingen/Erfa_Eger_2022_LD.xlsx'), 1),
        files_trees = [(Path('ksp_so/data-egerkingen/Erfa_Eger_2022_LD.xlsx'), 2)],
        headers={
            # PD
            'probe_num': ['PbfNr'],
            'eigentumer': ['waeig'],
            #'bestand_old': ['BTalt'],
            'bestand': ['Btneu'],
            'plot_x': ['GPS_X'],
            'plot_y': ['GPS_Y'],
            'radius': ['Radius'],
            'plot_remarks': ['Bemerkung'],
            # BD
            'baumart': ['BA'],
            'azimuth': ['Az'],
            'distance': ['Dis'],
            'dbh_old': ['BHD'],
            'dbh_new': ['BHDneu'],
            'markiert': ['M'],
            'tree_remarks': ['Bemerkung'],
        },
    ),
    'wasseramt-west': DataDef(
        key='wasseramt-west',
        name='Wasseramt-West',
        prefix=19,
        year=2019,
        year_old=2008,
        density=9600,
        file_plots = (Path('ksp_so/data-wamtwest/Vorlage_KSP_Daterf_inv_WAAMT_west.xlsx'), 1),
        files_trees = [(Path('ksp_so/data-wamtwest/Vorlage_KSP_Daterf_inv_WAAMT_west.xlsx'), 2)],
        headers={
            # PD
            'probe_num': ['PbfNr'],
            'eigentumer': ['waeig'],
            'bestand_old': ['BTalt'],
            'bestand': ['Btneu'],
            'plot_x': ['GPS_X'],
            'plot_y': ['GPS_Y'],
            'radius': ['Radius'],
            'dis_rand': ['Dist>R'],
            'plot_remarks': ['Bemerkung'],
            # BD
            'baumart': ['BA'],
            'azimuth': ['Az'],
            'distance': ['Dis'],
            'dbh_old': ['BHDalt'],
            'dbh_new': ['BHDneu'],
            'soziologie': ['SOZ'],
            'markiert': ['M'],
            'tree_remarks': ['Bemerkung'],
        },
    ),
    #prefix=20 used by hinteresthal2,
    'bucheggberg': DataDef(
        key='bucheggberg',
        name='Bucheggberg',
        prefix=21,
        year=2011,
        year_old=2002,
        density=19200,
        density_old=9600,
        file_plots = (Path('ksp_so/data-bucheggberg/Inv_KSP_Daterf_BuchBrg_2011.xlsx'), 1),
        files_trees = [(Path('ksp_so/data-bucheggberg/Inv_KSP_Daterf_BuchBrg_2011.xlsx'), 2)],
        headers={
            # PD
            'probe_num': ['PbfNr'],
            'eigentumer': ['waeig'],
            'bestand_old': ['BTalt'],
            'bestand': ['Btneu'],
            'plot_x': ['GPS_X'],
            'plot_y': ['GPS_Y'],
            'radius': ['Radius'],
            'dis_rand': ['Dist>R'],
            'plot_remarks': ['Bemerkung'],
            # BD
            'baumart': ['BA'],
            'azimuth': ['Az'],
            'distance': ['Dis'],
            'dbh_old': ['BHDalt'],
            'dbh_new': ['BHDneu'],
            'markiert': ['M'],
            'tree_remarks': ['Bemerkung'],
        },
    ),
}

OWNER_MAPPING = {
    # Leberberg
    'Lo': 'Lomm',
    'Be': 'Bell',
    'Klu': 'KLUS',
    'Ld': 'Ldor',
    'Od': 'Odor',
    'SW': 'KTSO',
    # Solothurn 2
    'BGSO6': 'SOLO.BG',
    'BGSO7': 'SOLO.BG',
    # Oberes Gäu
    'BG Kestenholz': 'KEST.BG',
    'Kanton Solothurn (SW Buchbann)': 'SW-Buchban',
    'BG Wolfwil': 'WOLF.BG',
    'BG Niederbuchsiten': 'NIED.BG',
    'BG Egerkingen': 'EGER.BG',
}


def get_tablib_data(file_path):
    sheet_num = None
    if isinstance(file_path, tuple):
        file_path, sheet_num = file_path
    file_ext = file_path.suffix.replace('.', '')
    open_mode = 'r' if file_ext == 'csv' else 'rb'
    open_kwargs = {'mode': open_mode}
    if file_ext == 'csv':
        with file_path.open(mode='rb') as rawdata:
            result = chardet.detect(rawdata.read(40000))
            open_kwargs['encoding'] = result['encoding'] if result['encoding'] != 'ascii' else 'latin-1'

    with file_path.open(**open_kwargs) as fh:
        load_kwargs = {'format': file_ext}
        if file_ext == 'csv':
            load_kwargs['delimiter'] = ';'
        if file_ext == 'csv' or sheet_num is None:
            return tablib.Dataset().load(fh, **load_kwargs)
        else:
            book = tablib.Databook().load(fh, **load_kwargs)
            sheet = book.sheets()[sheet_num - 1]
            print(f"{sheet.height} lines read from file {file_path}")
            return sheet


class ImporterMixin:
    def get_val(self, line, header, default=None):
        return self.defs.get_val(line, header, default=default)

    def aggregate_remarks_from_line(self, line, obj='plot'):
        remarks = ''
        for header in self.defs.headers.get(f'{obj}_remarks', []):
            rem = line.get(header, '')
            if rem and rem != '.':
                remarks += f'\n{rem}' if remarks else str(rem)
        return remarks


class Importer(ImporterMixin):
    def __init__(self, proj_def):
        self.defs = proj_def
        self.year = proj_def.year
        self.yearold = proj_def.year_old
        self.plotobs_dict = {}

    def get_plotobs(self, plot_num):
        if plot_num in self.plotobs_dict:
            return self.plotobs_dict[plot_num]

        obss = PlotObs.objects.filter(plot__nr=f'{self.defs.prefix}-{plot_num}')
        if not obss:
            return None
        self.plotobs_dict[plot_num] = {po.year: po for po in obss}
        return self.plotobs_dict[plot_num]

    def import_data(self, plots=True, trees='all'):
        if plots:
            with transaction.atomic():
                result = PlotImporter(self.defs).import_plots()
            self.plotobs_dict.update(result)
        if trees:
            if not plots and trees == 'all':
                # First clear any existing tree
                if self.yearold is not None:
                    Tree.objects.filter(treeobs__obs__inv_team__team=self.defs.name).delete()
                else:
                    TreeObs.objects.filter(obs__inv_team__team=self.defs.name, obs__year=self.year).delete()
            with transaction.atomic():
                for tree_file in self.defs.files_trees:
                    num_imported = self.import_trees(tree_file, only_new=trees=='new')
                    print(f"Baumdaten importiert ({num_imported} trees imported)")
                self.import_durres()
        self.defs.cleanup()
        print("Import finished")

    def get_vita_from_line(self, line, dbh_new, baumart_code=None, vita_new='l'):
        need_dbh_recalc = False
        if line.get('ausw.bd'):
            if line['ausw.bd'] == 'n.lieg':
                vita = self.vita_dict['z']
            elif line.get('ausw.bd') == 'nutz':
                vita = self.vita_dict['c']
            elif line.get('ausw.bd', '').startswith('zuw'):
                vita = self.vita_dict['l']
            elif line.get('ausw.bd') == 'n.abg' or '#tot' in line.get('ausw.bd', ''):
                vita = self.vita_dict['m']
            elif line.get('ausw.bd') in ['einw', '#u12', 'e.u12']:
                vita = self.vita_dict['e']
            else:
                return self.vita_dict[vita_new], need_dbh_recalc
                #raise ValueError(f"Unknown vita value {line['ausw.bd']}")
        elif line.get('vita'):
            return self.vita_dict[line.get('vita')], need_dbh_recalc
        elif (line.get('TOT') and line['TOT'] in ('l', 'L', '6', '60', '7', '70') or
              line.get('TOTneu') and line['TOTneu'] in ('l', 'L' '6', '60', '7', '70')):
            vita = self.vita_dict['z']
        elif (line.get('TOT') and line['TOT'] in ('s', 'S', '5', '50') or
              line.get('TOTneu') and line['TOTneu'] in ('s', 'S', '5', '50')):
            vita = self.vita_dict['m']
        elif baumart_code in [5, 50]:
            vita = self.vita_dict['m']
        elif baumart_code in [6, 60]:
            vita = self.vita_dict['z']
        elif dbh_new == 0:
            vita = self.vita_dict['c']
        elif dbh_new == 1 and self.defs.dbh_1_tot_stehend:
            vita = self.vita_dict['m']
            need_dbh_recalc = True
        elif dbh_new == 2 and self.defs.dbh_2_tot_liegend:
            vita = self.vita_dict['z']
            need_dbh_recalc = True
        else:
            vita = self.vita_dict[vita_new]
        return vita, need_dbh_recalc

    def import_trees(self, tree_file, only_new=False, vita_old='l', vita_new='l', force_old=False):
        # Add trees to plots from tree_file
        tree_data = get_tablib_data(tree_file)
        spec_dict = {ts.code: ts for ts in TreeSpecies.objects.all()}
        self.vita_dict = {v.code: v for v in Vita.objects.all()}
        num_trees = 0
        inventory = self.defs.get_inventory(old=False)
        need_dbh_recalc_list = []
        plot_blanked = None
        for line in tree_data.dict:
            rec_no = self.get_val(line, 'rec')
            if rec_no:
                # Skip non data lines
                try:
                    int(rec_no)
                except (TypeError, ValueError):
                    continue
            if line.get('BTEXT', '') == '0er-Baum':
                continue

            probe_num = self.get_val(line, 'probe_num')
            dbh_new = self.get_val(line, 'dbh_new')
            dbh_old = self.get_val(line, 'dbh_old', default=-1)
            azimuth = self.get_val(line, 'azimuth')
            distance = self.get_val(line, 'distance')

            if plot_blanked and probe_num != plot_blanked and plot_obs_blank:
                plot_obs_blank = self.get_plotobs(plot_blanked)
                if plot_obs_blank:
                    try:
                        older_plotobs_key = sorted([k for k in plot_obs_blank.keys() if k < self.yearold])[-1]
                        newer_plotobs_key = sorted([k for k in plot_obs_blank.keys() if k >= self.yearold])[0]
                    except IndexError:
                        pass
                    else:
                        print(f"Blanking trees for '{plot_blanked}' in {newer_plotobs_key}")
                        older_plotobs = plot_obs_blank[older_plotobs_key]
                        for treeobs in older_plotobs.treeobs_set.all():
                            TreeObs.objects.create(
                                tree=treeobs.tree,
                                obs=plot_obs_blank[newer_plotobs_key],
                                dbh=0,
                                vita=self.vita_dict['c'],
                            )

            if dbh_new == 0 and dbh_old == 0 and distance == 0:
                # FIXME: a mark that all trees have been cut
                plot_blanked = probe_num
                continue
            plot_blanked = None

            if dbh_new in ('', '.', '#NV') or dbh_new is None:
                dbh_new = 0
            create_new = not (isinstance(dbh_new, int) and dbh_new > 800)
            create_old = self.yearold and dbh_old is not None and dbh_old not in ('',) and (dbh_old > 0 or force_old)

            if isinstance(probe_num, str) and probe_num in ['@', '§']:
                continue
            if azimuth in ('', None) or distance in ('', None):
                continue
            plot_obss = self.get_plotobs(probe_num)
            if plot_obss is None:
                logging.error(f"PBF nr {probe_num} not found, ignoring...")
                continue
            has_old_plotobs = self.year in plot_obss and sorted(plot_obss.keys()).index(self.year) > 0
            if (not create_new and not create_old) or (not has_old_plotobs and dbh_new == 0):
                # Ignore such trees, probably means they were not found any more.
                #print(f"Tree line {rec_no} ignored")
                continue

            baumart_code = self.get_val(line, 'baumart')
            if baumart_code is None:
                print(f"Warning: Tree species not defined in line: {line}")
                baumart = None
            elif baumart_code in spec_dict:
                baumart = spec_dict[self.get_val(line, 'baumart')]
            elif baumart_code < 10:
                baumart = spec_dict[9]  # übrige Laubhölzer
            elif baumart_code < 100:
                baumart = spec_dict[90]  # übrige Nadelhölzer
            else:
                print(f"Unknown tree species code {baumart_code}, ignoring...")
                continue

            plot = plot_obss[self.year].plot
            tree = None
            new_tree = False
            if self.defs.existing_trees or only_new:
                try:
                    tree = Tree.objects.get(
                        plot=plot, spec=baumart, azimuth=azimuth, distance=distance
                    )
                    if only_new:
                        logging.debug(f"Skipping existing tree at {azimuth}ᵍ {distance}dm ({probe_num})")
                        continue
                except Tree.DoesNotExist:
                    pass
            if tree is None:
                nr = self.get_val(line, 'baum_num', default=1) or 1
                if nr in (-1, '-1'):
                    nr = 1
                # Ensure tree number uniqueness for a same plot
                if not hasattr(plot, '_tree_nums'):
                    plot._tree_nums = set()
                while nr in plot._tree_nums:
                    nr = nr + 1
                plot._tree_nums.add(nr)
                tree = Tree(
                    plot=plot,
                    spec=baumart,
                    nr=nr,
                    azimuth=azimuth,
                    distance=distance,
                )
                try:
                    tree.full_clean()
                except Exception as exc:
                    import pdb; pdb.set_trace()
                    print('tree line does not validate')
                    raise
                tree.save()
                logging.debug(f"Create tree at {azimuth}ᵍ {distance}dm ({probe_num})")
                new_tree = True

            vita, need_dbh_recalc = self.get_vita_from_line(line, dbh_new, baumart_code, vita_new)
            if vita == self.vita_dict['x']:
                if new_tree:
                    continue
                dbh_new = None
            if has_old_plotobs and new_tree and not create_old:
                vita = self.vita_dict['e']
            if create_new:
                soz_code = self.get_val(line, 'soziologie')
                if soz_code:
                    try:
                        rank = Rank.objects.get(code=soz_code)
                    except Rank.DoesNotExist:
                        print(f"Warning: unkown Rank with code {soz_code}")
                        rank = None
                else:
                    rank = None
                markiert = self.get_val(line, 'markiert', default=False)
                remarks = self.aggregate_remarks_from_line(line, obj='tree')
                tobs = TreeObs(
                    tree=tree,
                    obs=plot_obss[self.year],
                    dbh=dbh_new,
                    vita=vita,
                    rank=rank,
                    marked=markiert,
                    remarks=remarks,
                )
                try:
                    tobs.full_clean()
                except ValidationError as err:
                    import pdb; pdb.set_trace()
                    self.get_val(line, 'dbh_new')
                    raise
                tobs.save()
                logging.debug(f"Create treeobs (year={self.year}, vita={vita.code}, dbh={dbh_new})")

            old_obs = None
            if create_old:
                if self.yearold not in plot_obss:
                    print(f"Unable to find obs for year {self.yearold} for line {line}")
                else:
                    has_older_plotobs = sorted(plot_obss.keys()).index(self.yearold) > 0
                    if has_older_plotobs and new_tree:
                        vita = self.vita_dict['e']
                    else:
                        vita=self.vita_dict[vita_old]

                    old_obs = TreeObs.objects.create(
                        tree=tree,
                        obs=plot_obss[self.yearold],
                        dbh=self.get_val(line, 'dbh_old', default=dbh_new),
                        vita=vita,
                    )
                    logging.debug(f"Create treeobs (year={self.yearold}, vita={vita.code}, dbh={old_obs.dbh})")
            if need_dbh_recalc:
                need_dbh_recalc_list.append((tobs, old_obs.dbh if old_obs else None))
            num_trees += 1

        # Calculate new values for dbh_new when dbh is 1 or 2 (#13)
        if need_dbh_recalc_list:
            # Calculate growth mean over the period
            with connection.cursor() as cursor:
                cursor.execute(growth_query % inventory.pk)
                mean_growth = int(cursor.fetchone()[0] // 2)
        for tobs, old_dbh in need_dbh_recalc_list:
            if old_dbh is None:
                old_dbh = self.get_val(line, 'dbh_old')
            tobs.dbh = old_dbh + mean_growth
            tobs.save(update_fields=['dbh'])
        return num_trees

    def import_durres(self):
        if self.defs.file_durre:
            self.import_trees(self.defs.file_durre, vita_new='m', vita_old='l', force_old=True)


class PlotImporter(ImporterMixin):
    def __init__(self, defs):
        self.defs = defs
        self.inventory = defs.get_inventory(old=False)
        self.inventory_old = defs.get_inventory(old=True)
        self.slope_mapping_reverse = {round(v * 100): k for k, v in Plot.slope_mapping.items()}
        self.slope_mapping_reverse.update({
            980: 5, 982: 15, 983: 16, 984: 17, 990: 24, 997: 30, 1000: 31, 1010: 37, 1020: 43, 1030: 50,
            1040: 53, 1050: 58, 1060: 62, 1070: 66, 1083: 71, 1090: 74, 1100: 78, 1110: 82,
            1119: 85, 1133: 90, 1140: 93, 1160: 100, 1190: 110,
        })

    def get_plot_from_line(self, line):
        from observation.swisstopo import query_elevation
        # Try to get existing plot, or create it
        probe_num = self.get_val(line, 'probe_num', default='__raise__')
        if probe_num is None or probe_num in ['@', '§']:
            return None
        try:
            plot = Plot.objects.get(nr=f'{self.defs.prefix}-{probe_num}')
        except Plot.DoesNotExist:
            # Create plot and get center point
            pt, pt_exact = self.defs.get_point(line)
            if pt is None:
                print(f"No coords found for {probe_num}, ignoring")
                return None
            radius = self.get_val(line, 'radius')
            if radius is None:
                return None
            slope = self.slope_mapping_reverse[radius]
            plot = Plot.objects.create(
                nr=f'{self.defs.prefix}-{probe_num}',
                slope=slope,
                the_geom=pt,
                point_exact=pt_exact,
                gps_precision=pt_exact.precision if pt_exact else None,
                sealevel=query_elevation(pt) or 0,
            )
        return plot

    def import_plots(self):
        from gemeinde.models import Gemeinde

        plot_data = get_tablib_data(self.defs.file_plots)
        self.defs.before_plot_import()  # Mostly deleting existing data

        plotobs_dict = {}
        for line in plot_data.dict:
            rec_no = self.get_val(line, 'rec')
            if rec_no:
                try:
                    int(rec_no)
                except (TypeError, ValueError):
                    # Not a data line
                    continue
            plot = self.get_plot_from_line(line)
            if plot is None:
                continue
            # Import plotobs data
            try:
                gemeinde = Gemeinde.objects.get(the_geom__contains=plot.the_geom)
            except Gemeinde.DoesNotExist:
                if int(plot.the_geom.x) == 2600156 and int(plot.the_geom.y) == 1221846:
                    gemeinde = Gemeinde.objects.get(name='Buchegg')
                else:
                    print(f"Unable to find Gemeinde for coords {plot.the_geom.coords} (Probenum {plot.nr}). Ignoring...")
                    continue
            devel, mixt = self.get_bestand_from_line(line)
            owner = self.get_owner_from_line(line)
            edgef = self.get_edgef_from_line(line)
            remarks = self.aggregate_remarks_from_line(line)
            zuwachs_code = self.get_val(line, 'zuwachs_code') if 'zuwachs_code' in self.defs.headers else None
            plotobs_dict[plot.nr] = {}
            if zuwachs_code != 3:
                obs = PlotObs.objects.create(
                    plot=plot,
                    year=self.defs.year,
                    inv_team=self.inventory,
                    municipality=gemeinde,
                    owner=owner,
                    density=self.inventory.default_density,
                    evaluation_unit=self.get_val(line, 'einheit', default=0),
                    stand_devel_stage=devel,
                    stand_forest_mixture=mixt,
                    forest_edgef=edgef,
                    gwl=0,
                    remarks=remarks,
                )
                plotobs_dict[plot.nr][self.defs.year] = obs

            if zuwachs_code not in (1, 2) and self.defs.year_old and self.defs.has_prev_obs_from_line(line):
                owner = Owner.objects.get(nr=OWNER_MAPPING[line['we_alt']]) if 'we_alt' in line else owner
                devel_alt, mixt_alt = self.get_bestand_from_line(line, head='bestand_old')
                obs_old = PlotObs.objects.create(
                    plot=plot,
                    year=self.defs.year_old,
                    inv_team=self.inventory_old,
                    municipality=gemeinde,
                    owner=owner,
                    density=self.inventory_old.default_density,
                    evaluation_unit=self.get_val(line, 'einheit', default=0),
                    stand_devel_stage=devel_alt or devel,
                    stand_forest_mixture=mixt_alt or mixt,
                    forest_edgef=1,  # Currently mandatory
                    gwl=0,
                )
                plotobs_dict[plot.nr][self.defs.year_old] = obs_old

        print("Probedaten importiert")
        return plotobs_dict

    def get_bestand_from_line(self, line, head='bestand'):
        bestand_val = self.get_val(line, head)
        if not bestand_val:
            return None, None
        try:
            devel = DevelStage.objects.get(code=str(bestand_val // 10 * 10))
        except DevelStage.DoesNotExist:
            devel = None
        mixt_code = str(bestand_val % 10)
        if mixt_code and mixt_code != '0':
            try:
                mixt = ForestMixture.objects.get(code=mixt_code)
            except ForestMixture.DoesNotExist:
                mixt = None
        else:
            mixt = None
        return devel, mixt

    def get_owner_from_line(self, line):
        if 'eigentumer' in self.defs.headers and self.get_val(line, 'eigentumer'):
            return Owner.objects.get(
                nr=OWNER_MAPPING.get(self.get_val(line, 'eigentumer'), self.get_val(line, 'eigentumer'))
            )
        if self.defs.owner_map:
            pnum = self.get_val(line, 'probe_num')
            if str(pnum)[:3] in self.defs.owner_map:
                return Owner.objects.get(nr=self.defs.owner_map[str(pnum)[:3]])

    def get_edgef_from_line(self, line):
        dis_rand = self.get_val(line, 'dis_rand')
        if not dis_rand or dis_rand in ("-", "?"):
            return 1
        rad = self.get_val(line, 'radius')
        h = rad - dis_rand
        rand_area = rad * rad * math.acos(1 - h / rad) - (rad - h) * math.sqrt(2 * h * rad - h * h)
        rand_fact = 1 - (rand_area / (math.pi * rad * rad))
        return round(rand_fact, 1)


def to_num(text):
    if not isinstance(text, str):
        return text
    if text in ['.', '-', '']:
        return None
    if '.' in text:
        return float(text)
    else:
        return int(text)


if __name__ == "__main__":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
    logging.basicConfig(level=logging.DEBUG)
    django.setup()
    from ksp_so.models import (
        DevelStage, ForestMixture, Owner, Plot, PlotObs, Rank, Tree, TreeObs, TreeSpecies, Vita
    )

    parser = ArgumentParser()
    parser.add_argument('--project', required=True)
    parser.add_argument('--only-trees', dest='only_trees', action='store_true')
    parser.add_argument('--only-new-trees', dest='only_new_trees', action='store_true')
    args = parser.parse_args()
    if not args.project in REGION_DATA:
        print(f"{args.project} is not known project. Projects are: {','.join(REGION_DATA.keys())}")
        sys.exit(1)
    importer = Importer(REGION_DATA[args.project])
    importer.import_data(
        plots=not args.only_trees and not args.only_new_trees,
        trees='new' if args.only_new_trees else 'all'
    )
