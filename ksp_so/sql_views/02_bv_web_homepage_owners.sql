CREATE OR REPLACE VIEW public.bv_web_homepage_owners
 AS
 SELECT o.name,
    inventory.inv_from,
    date_part('year', inventory.inv_from) AS year,
    sum(b."Anzahl Probebaeume")::integer AS "Anzahl Probebaeume",
    avg(b."Stammzahl pro ha")::integer AS "Stammzahl pro ha",
    stddev(b."Stammzahl pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Stammzahl pro ha")::double precision * 100::double precision AS "% Standardfehler",
    avg(b."Volumen pro ha")::numeric(5,1) AS "Volumen pro ha",
    stddev(b."Volumen pro ha") / sqrt(count(*)::double precision) / avg(b."Volumen pro ha") * 100::double precision AS "% Standardfehler2",
    avg(b."Grundflaeche pro ha")::numeric(5,1) AS "Grundflaeche pro ha",
    stddev(b."Grundflaeche pro ha") / sqrt(count(*)::double precision) / avg(b."Grundflaeche pro ha") * 100::double precision AS "% Standardfehler3",
    sum(b.waldflaeche)::numeric(5,1) AS "theoretische Waldfläche",
    count(b.id) AS "Anzahl Probepunkte",
    b.owner_id
   FROM bv_grundf_vol_yx_pro_plotobs b
     FULL JOIN owner o ON b.owner_id = o.id
     LEFT JOIN inventory ON b.inv_team_id = inventory.id
  GROUP BY b.owner_id, o.name, inventory.inv_from
  ORDER BY o.name, year;
