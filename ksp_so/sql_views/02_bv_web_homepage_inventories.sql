CREATE OR REPLACE VIEW public.bv_web_homepage_inventories
 AS
 SELECT inv.name,
    date_part('year', inv.inv_from) AS year_from,
    date_part('year', inv.inv_to) AS year_to,
    sum(b."Anzahl Probebaeume")::integer AS "Anzahl Probebaeume",
    avg(b."Stammzahl pro ha")::integer AS "Stammzahl pro ha",
    stddev(b."Stammzahl pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Stammzahl pro ha")::double precision * 100::double precision AS "% Standardfehler",
    avg(b."Volumen pro ha")::numeric(5,1) AS "Volumen pro ha",
    stddev(b."Volumen pro ha") / sqrt(count(*)::double precision) / avg(b."Volumen pro ha") * 100::double precision AS "% Standardfehler2",
    avg(b."Grundflaeche pro ha")::numeric(5,1) AS "Grundflaeche pro ha",
    stddev(b."Grundflaeche pro ha") / sqrt(count(*)::double precision) / avg(b."Grundflaeche pro ha") * 100::double precision AS "% Standardfehler3",
    sum(b.waldflaeche)::numeric(5,1) AS "theoretische Waldfläche ha",
    count(b.inv_period) AS "Anzahl Probepunkte",
    inv.id
   FROM bv_grundf_vol_yx_pro_plotobs b
     LEFT JOIN inventory inv ON b.inv_team_id = inv.id
  GROUP BY inv.id, inv.name, inv.inv_from
  ORDER BY inv.name, inv.inv_from;
