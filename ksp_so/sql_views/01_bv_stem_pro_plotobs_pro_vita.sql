CREATE OR REPLACE VIEW bv_stem_pro_plotobs_pro_vita AS
 SELECT tree_obs.obs_id AS id,
    count(*) AS total_stem,
    sum(
        CASE
            WHEN lt_vita.code::text = 'l'::text THEN 1
            ELSE 0
        END) AS lebend,
    sum(
        CASE
            WHEN lt_vita.code::text = 'e'::text THEN 1
            ELSE 0
        END) AS einwuchs,
    sum(
        CASE
            WHEN lt_vita.code::text = 'c'::text THEN 1
            ELSE 0
        END) AS nutzung,
    sum(
        CASE
            WHEN lt_vita.code::text = 'm'::text THEN 1
            ELSE 0
        END) AS tot
   FROM tree_obs
     LEFT JOIN plot_obs ON tree_obs.obs_id = plot_obs.id
     LEFT JOIN lt_vita ON tree_obs.vita_id = lt_vita.id
  GROUP BY tree_obs.obs_id;
