CREATE OR REPLACE VIEW public.bv_biomasse_pro_treeobs
 AS
 SELECT tree_obs.id,
    plot_obs.id AS obs_id,
    plot.id AS plot_id,
    tree_obs.dbh,
    tree_obs.vita_id,
    tree_obs.tree_id,
    tree.spec_id,
    ksp_biomass_tarif_wsl_so_2022(tree_obs.dbh, plot.sealevel, tree_spec.tarif_klass20, phytosoc.tarif_klass20) AS biomasse
   FROM tree_obs
     JOIN tree ON tree_obs.tree_id = tree.id
     JOIN tree_spec ON tree.spec_id = tree_spec.id
     JOIN plot_obs ON tree_obs.obs_id = plot_obs.id
     JOIN plot ON plot_obs.plot_id = plot.id
     LEFT JOIN phytosoc ON plot.phytosoc_id = phytosoc.id;
