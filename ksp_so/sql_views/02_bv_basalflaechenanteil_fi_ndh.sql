CREATE OR REPLACE VIEW bv_basalflaechenanteil_fi_ndh
 AS
 SELECT a.plot_id,
    a.plot_obs_id,
    100.0::double precision / NULLIF(sum(a."Grundflaeche cm2"), 0) * sum(
        CASE
            WHEN a.tree_spec_id = 15 THEN a."Grundflaeche cm2"
            ELSE 0::double precision
        END) AS "% Basalflächenanteil der Fichte",
    100.0::double precision / NULLIF(sum(a."Grundflaeche cm2"), 0) * sum(
        CASE
            WHEN a.tree_spec_id = ANY (ARRAY[15, 16, 17, 18, 19, 20, 21]) THEN a."Grundflaeche cm2"
            ELSE 0::double precision
        END) AS "% Basalflächenanteil Nadelholz",
    100.0::double precision / NULLIF(sum(a."Grundflaeche cm2"), 0) * sum(
        CASE
            WHEN a.tree_spec_id = ANY (ARRAY[15, 17, 18, 19, 20, 21]) THEN a."Grundflaeche cm2"
            ELSE 0::double precision
        END) AS "% Basalflächenanteil Nadelholz ohne Tanne"
   FROM bv_grundf_vol_yx_pro_plotobs_und_allspec a
  GROUP BY a.plot_id, a.plot_obs_id;
