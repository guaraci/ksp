CREATE OR REPLACE VIEW public.bv_biomasse_pro_plotobs
 AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0::bigint) AS "Anzahl Probebaeume",
    COALESCE(subq.biomasse, 0::numeric) AS "Biomasse",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(plot_obs.forest_edgef), 0::numeric) AS "Stammzahl pro ha",
    COALESCE(subq.biomasse * ksp_lokale_dichte(plot_obs.forest_edgef), 0::numeric) AS "Biomasse pro ha",
    plot_obs.forest_edgef,
    inventory.period AS inv_period,
    inventory.municipality_id AS inv_municipality_id,
    plot_obs.density,
    plot_obs.inv_team_id,
    plot_obs.owner_id
   FROM ( SELECT bv.obs_id,
            count(*) AS "Anzahl Probebaeume",
            sum(bv.biomasse) AS biomasse
           FROM public.bv_biomasse_pro_treeobs bv
          WHERE (bv.vita_id = 2 OR bv.vita_id = 3) AND bv.dbh > 11
          GROUP BY bv.obs_id
        ) subq
     RIGHT JOIN plot_obs ON subq.obs_id = plot_obs.id
     LEFT JOIN inventory ON plot_obs.inv_team_id = inventory.id;
