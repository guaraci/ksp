CREATE OR REPLACE VIEW public.bv_biomasse_pro_plotobs_und_specgroup
 AS
 SELECT (sub2.plot_obs_id || '-'::text) || sub2.tree_group_id AS id,
    sub2.plot_id,
    sub2.plot_obs_id,
    sub2.year,
    sub2.tree_group_id,
    subq.name,
    sub2.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0::bigint) AS "Anzahl Probebaeume",
    COALESCE(subq.biomasse, 0::numeric) AS "Biomasse",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(sub2.forest_edgef), 0::numeric) AS "Stammzahl pro ha",
    COALESCE(subq.biomasse * ksp_lokale_dichte(sub2.forest_edgef), 0::numeric) AS "Biomasse pro ha"
   FROM ( SELECT bv.obs_id,
            tree_group.id,
            tree_group.name,
            count(*) AS "Anzahl Probebaeume",
            sum(bv.biomasse) AS biomasse
           FROM public.bv_biomasse_pro_treeobs bv
             JOIN tree_spec ON bv.spec_id = tree_spec.id
             JOIN tree_group ON tree_spec.group_id = tree_group.id
          WHERE (bv.vita_id = 2 OR bv.vita_id = 3) AND bv.dbh > 11
          GROUP BY bv.obs_id, tree_group.id, tree_group.name
        ) subq
     RIGHT JOIN ( SELECT plot_obs.id AS plot_obs_id,
            plot_obs.plot_id,
            plot_obs.year,
            plot_obs.forest_edgef,
            plot_obs.municipality_id,
            tree_group.id AS tree_group_id
           FROM plot_obs,
            tree_group) sub2 ON sub2.plot_obs_id = subq.obs_id AND sub2.tree_group_id = subq.id;
