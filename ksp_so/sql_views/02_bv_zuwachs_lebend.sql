CREATE OR REPLACE VIEW public.bv_zuwachs_lebend
 AS
 SELECT tobsa.id,
    tobsa.tree_id,
    tobsa.obs_id,
    inv0.ordering AS invent0,
    inv1.ordering AS invent1,
    tobsb.dbh AS dbh0,
    tobsa.dbh AS dbh1,
    ksp_tarif_volume(tobsb.dbh, plot.sealevel, tree_spec.tarif_klass20, phytosoc.tarif_klass20) AS vol0,
    ksp_tarif_volume(tobsa.dbh, plot.sealevel, tree_spec.tarif_klass20, phytosoc.tarif_klass20) AS vol1,
    ksp_grundflaeche_bl(tobsb.dbh) AS grundf0,
    ksp_grundflaeche_bl(tobsa.dbh) AS grundf1,
    tobsa.dbh - tobsb.dbh AS dbh_diff,
    poa.year - pob.year AS year_diff,
    ksp_grundflaeche_bl(tobsa.dbh) - ksp_grundflaeche_bl(tobsb.dbh) AS grundf_diff,
    GREATEST(0::double precision, (ksp_tarif_volume(tobsa.dbh, plot.sealevel, tree_spec.tarif_klass20, phytosoc.tarif_klass20) - ksp_tarif_volume(tobsb.dbh, plot.sealevel, tree_spec.tarif_klass20, phytosoc.tarif_klass20))::double precision / NULLIF(poa.year - pob.year, 0)::double precision) AS growth_year
   FROM tree_obs tobsa
     JOIN tree_obs tobsb ON tobsa.tree_id = tobsb.tree_id
     JOIN tree ON tobsa.tree_id = tree.id
     JOIN tree_spec ON tree.spec_id = tree_spec.id
     LEFT JOIN plot_obs poa ON tobsa.obs_id = poa.id
     LEFT JOIN inventory inv1 ON poa.inv_team_id = inv1.id
     LEFT JOIN plot_obs pob ON tobsb.obs_id = pob.id
     LEFT JOIN inventory inv0 ON pob.inv_team_id = inv0.id
     LEFT JOIN plot ON poa.plot_id = plot.id
     LEFT JOIN phytosoc ON plot.phytosoc_id = phytosoc.id
     LEFT JOIN lt_vita vitaa ON tobsa.vita_id = vitaa.id
     LEFT JOIN lt_vita vitab ON tobsb.vita_id = vitab.id
  WHERE vitaa.code::text = 'l'::text AND (vitab.code::text = ANY (ARRAY['e'::character varying::text, 'l'::character varying::text])) AND inv1.ordering = (inv0.ordering + 1);
