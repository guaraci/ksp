CREATE OR REPLACE VIEW public.bv_zuwachs_lebend_pro_plotobs
 AS 
 SELECT po.id,
    max(po.plot_id) AS plot_id,
    COALESCE(sum(zuw.growth_year), 0::double precision) AS growth,
    COALESCE(sum(zuw.growth_year), 0::double precision) * ksp_lokale_dichte(max(po.forest_edgef))::double precision AS growth_ha,
    count(zuw.id) AS nb_stem,
    COALESCE(sum(zuw.growth_year) / NULLIF(count(zuw.id), 0)::double precision * ksp_lokale_dichte(max(po.forest_edgef))::double precision, 0::double precision) AS growth_stem_ha
   FROM bv_secondobs po
     LEFT JOIN bv_zuwachs_lebend zuw ON zuw.obs_id = po.id
  GROUP BY po.id;
