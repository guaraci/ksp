import copy
from datetime import date
from pathlib import Path
from tempfile import NamedTemporaryFile

from django.db import connection
from django.db.models import Sum
from django.http import HttpResponse, QueryDict
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import TemplateView, View

from openpyxl import load_workbook
from openpyxl.utils import column_index_from_string

from observation.views import GemeindenView
from .db_views import HomepageView
from .models import DevelStage, Inventory, Owner, TreeGroup
from .queries import VIEW_MAP

openxml_contenttype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'


class TabsMixin:
    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context['tabs'] = [
            ('Gemeinden', reverse('home')),
            ('Inventare', reverse('inventories')),
            ('Eigentümer', reverse('owners')),
        ]
        return context


class SOGemeindenView(TabsMixin, GemeindenView):
    has_waldflaeche_col = False
    db_view = HomepageView


class InventoryHomeView(TabsMixin, TemplateView):
    template_name = 'ksp_so/inventories.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        with connection.cursor() as cursor:
            # Note: it may be possible to create a Django model in db_views to replace this.
            cursor.execute("SELECT * FROM public.bv_web_homepage_inventories")
            result = dictfetchall(cursor)
        previous_line = None
        for idx, line in enumerate(result):
            line['years'] = (
                int(line['year_from']) if line['year_from'] == line['year_to']
                else f"{int(line['year_from'])} - {int(line['year_to'])}"
            )
            # Attach analysis URL to every two lines
            if previous_line and line['name'] == previous_line['name']:
                result[idx]['analysis_url'] = reverse(
                    'standard-analyse-by-inventory', args=[previous_line['id'], line['id']]
                )
            previous_line = line
        context.update({
            'inventories': result,
            'current_inventories': Inventory.objects.filter(inv_to__gt=date.today()),
            'object_name': 'Inventar',
            'can_add': self.request.user.has_perm('ksp_so.add_inventory'),
        })
        return context


class OwnersHomeView(TabsMixin, TemplateView):
    template_name = 'ksp_so/inventories.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM public.bv_web_homepage_owners")
            result = dictfetchall(cursor)
        previous_line = None
        for idx, line in enumerate(result):
            if previous_line and line['owner_id'] and line['name'] == previous_line['name']:
                # Attach analysis URL to every two lines
                result[idx]['analysis_url'] = reverse(
                    'standard-analyse-by-owner', args=[line['owner_id'], int(previous_line['year']), int(result[idx]['year'])]
                )
            previous_line = line
        context.update({
            'inventories': result,
            'object_name': 'Eigentümer',
        })
        return context


def dictfetchall(cursor): 
    "Returns all rows from a cursor as a dict" 
    desc = cursor.description 
    return [
        dict(zip([col[0].replace(' ', '_').replace('%', 'prc') for col in desc], row)) 
        for row in cursor.fetchall() 
    ]


class StandardAnalyseView(View):
    # Cell coordinates for the first cell of each analysis
    stammzahl_per_devel_year = {'old': ('BE', 15), 'recent': ('BE', 24)}
    stammzahl_per_species_year = {'old': ('BE', 58), 'recent': ('BE', 66)}
    stammzahl_total_year = ('BD', 47)
    zuwachs_per_devel_year = ('BQ', 15)
    zuwachs_total_year = ('BP', 48)
    nutzung_per_devel_year = {'old': ('CE', 15), 'recent': ('CE', 25)}
    nutzung_total_year = ('CD', 48)
    perimeter_cells = ['B13', 'B16', 'BD38', 'BD51', 'BD76', 'BP26', 'BP51', 'CD39', 'CD52']
    diff_year_cell = 'I16'
    zustand_start_cell = 'G22'
    zustand_end_cell = 'S22'

    def get(self, request, *args, **kwargs):
        if 'owner_id' in kwargs:
            # By owner...
            owner = get_object_or_404(Owner, pk=kwargs['owner_id'])
            perim_data = QueryDict(f'owners={owner.pk}')
            perimeter_name = owner.name
            year_old = kwargs['year1']
            year_recent = kwargs['year2']
        elif 'inv1_id' in kwargs:
            # ... or by inventory
            invent1 = get_object_or_404(Inventory, pk=kwargs['inv1_id'])
            invent2 = get_object_or_404(Inventory, pk=kwargs['inv2_id'])
            perim_data = QueryDict(f'inventories={invent1.pk}&inventories={invent2.pk}')
            perimeter_name = invent1.name
            year_old = invent1.inv_from.year
            year_recent = invent2.inv_from.year

        devel_stages = list(DevelStage.objects.all().order_by('description').values_list('description', flat=True))
        species_groups = list(TreeGroup.objects.all().order_by('name').values_list('name', flat=True))

        wb = load_workbook(filename=str(Path(__file__).parent / 'Vorlage_Standardanalyse.xlsx'))
        ws = wb.active

        # Query 1: Stammzahl, Volumen, Grundfläche, grouped by year/devel_stage
        stamm_query = copy.deepcopy(VIEW_MAP['Stammzahl'])
        stamm_query.annot_map['waldflaeche'] = Sum

        context = stamm_query.build_query(
            perim_data, aggrs=['year', 'stand_devel_stage'], stddev='rel',
        )
        anzahl_idx = context['field_names'].index('Anzahl KSP')
        waldf_idx = context['field_names'].index('theoretische Waldfläche')
        waldf_rows = {
            'Jungwuchs/Dickung': 25, 'Stangenholz': 26,
            'Baumholz 1': 27, 'Baumholz 2': 28, 'Baumholz 3g': 29, 'Baumholz 3a': 30,
            'Stufig / DW / PL': 31, 'unbest.': 32
        }

        for line in context['query']:
            year = line[0]
            if year not in [year_old, year_recent]:
                continue
            if line[1] is None:  # unbestockt
                devel_index = len(devel_stages)
            else:
                devel_index = devel_stages.index(line[1])
            cell_start = list(self.stammzahl_per_devel_year['old' if year == year_old else 'recent'])
            cell_start = [column_index_from_string(cell_start[0]), cell_start[1] + devel_index]
            ws.cell(cell_start[1], cell_start[0] - 2).value = year
            
            col, row = cell_start
            for col_idx in range(2, 8):
                cell = ws.cell(row, col + col_idx - 2).value = line[col_idx]
            ws.cell(row, col + 6).value = line[anzahl_idx]
            # fill theoretische Waldfläche
            ws.cell(waldf_rows[line[1] or 'unbest.'], 4 if year == year_old else 20).value = line[waldf_idx]

        # Query 2: Stammzahl, Volumen, Grundfläche, grouped by year
        context = stamm_query.build_query(
            perim_data, aggrs=['year'], stddev='rel',
        )
        anzahl_idx = context['field_names'].index('Anzahl KSP')
        for line_idx, line in enumerate(context['query']):
            year = line[0]
            if year not in [year_old, year_recent]:
                continue
            cell_start = list(self.stammzahl_total_year)
            cell_start = [column_index_from_string(cell_start[0]), cell_start[1] + line_idx]
            ws.cell(cell_start[1], cell_start[0] - 1).value = line[0]  # year
            col, row = cell_start
            for col_idx in range(1, 7):
                ws.cell(row, col + col_idx - 1).value = line[col_idx]
            ws.cell(row, col + 6).value = line[anzahl_idx]

        del stamm_query.annot_map['waldflaeche']
        # Query 3: Stammzahl, Volumen, Grundfläche, grouped by species group
        context = stamm_query.build_query(
            perim_data, aggrs=['year', 'group'], stddev='rel',
        )
        anzahl_idx = context['field_names'].index('Anzahl KSP')
        for line in context['query']:
            year = line[0]
            if year not in [year_old, year_recent]:
                continue
            species_index = species_groups.index(line[1])
            cell_start = list(self.stammzahl_per_species_year['old' if year == year_old else 'recent'])
            cell_start = [column_index_from_string(cell_start[0]), cell_start[1] + species_index]
            ws.cell(cell_start[1], cell_start[0] - 2).value = year
            col, row = cell_start
            for col_idx in range(2, 8):
                cell = ws.cell(row, col + col_idx - 2)
                cell.value = line[col_idx]
            ws.cell(row, col + 6).value = line[anzahl_idx]

        # Query 4: Zuwachs grouped by year/devel_stage
        zuwachs_query = VIEW_MAP['Zuwachs']
        context = zuwachs_query.build_query(
            perim_data, aggrs=['year', 'stand_devel_stage'], stddev='rel',
        )
        for line in context['query']:
            year = line[0]
            if year != year_recent:
                continue
            if line[1] is None:  # unbestockt
                devel_index = len(devel_stages)
            else:
                devel_index = devel_stages.index(line[1])
            cell_start = list(self.zuwachs_per_devel_year)
            cell_start = [column_index_from_string(cell_start[0]), cell_start[1] + devel_index]
            ws.cell(cell_start[1], cell_start[0] - 2).value = year
            
            for col_idx in range(2, 11):
                col, row = cell_start
                cell = ws.cell(row, col + col_idx - 2)
                cell.value = line[col_idx]

        # Query 5: Zuwachs total
        context = zuwachs_query.build_query(
            perim_data, aggrs=['year'], stddev='rel',
        )
        for line in context['query']:
            year = line[0]
            if year != year_recent:
                continue
            cell_start = list(self.zuwachs_total_year)
            cell_start = [column_index_from_string(cell_start[0]), cell_start[1] + line_idx]
            ws.cell(cell_start[1], cell_start[0] - 1).value = line[0]  # year
            for col_idx in range(1, 10):
                col, row = cell_start
                ws.cell(row, col + col_idx - 1).value = line[col_idx]

        # Query 6: Nutzung grouped by year/devel_stage
        nutzung_query = VIEW_MAP['Nutzung']
        context = nutzung_query.build_query(
            perim_data, aggrs=['year', 'stand_devel_stage'], stddev='rel',
        )
        for line in context['query']:
            year = line[0]
            if year not in [year_old, year_recent]:
                continue
            if line[1] is None:  # unbestockt
                devel_index = len(devel_stages)
            else:
                devel_index = devel_stages.index(line[1])
            cell_start = list(self.nutzung_per_devel_year['old' if year == year_old else 'recent'])
            cell_start = [column_index_from_string(cell_start[0]), cell_start[1] + devel_index]
            ws.cell(cell_start[1], cell_start[0] - 2).value = year
            
            for col_idx in range(2, 9):
                col, row = cell_start
                cell = ws.cell(row, col + col_idx - 2)
                cell.value = line[col_idx]

        # Query 7: Nutzung total
        context = nutzung_query.build_query(
            perim_data, aggrs=['year'], stddev='rel',
        )
        for line_idx, line in enumerate(context['query']):
            year = line[0]
            if year not in [year_old, year_recent]:
                continue
            cell_start = list(self.nutzung_total_year)
            cell_start = [column_index_from_string(cell_start[0]), cell_start[1] + line_idx]
            ws.cell(cell_start[1], cell_start[0] - 1).value = line[0]  # year
            for col_idx in range(1, 8):
                col, row = cell_start
                ws.cell(row, col + col_idx - 1).value = line[col_idx]

        # Fill variable values for various cells
        for cell in self.perimeter_cells:
            ws[cell].value = perimeter_name
        ws[self.diff_year_cell] = year_recent - year_old
        ws[self.zustand_start_cell] = f'Zustand am Periodenanfang [{year_old}]'
        ws[self.zustand_end_cell] = f'Zustand am Periodenende [{year_recent}]'

        filename = f'Vorlage_Standardanalyse_{perimeter_name}_{date.today()}.xlsx'

        with NamedTemporaryFile() as tmp:
            wb.save(tmp.name)
            tmp.seek(0)
            response = HttpResponse(tmp, content_type=openxml_contenttype)
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response

