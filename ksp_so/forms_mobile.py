from django import forms
from django.contrib.gis.geos import Point

from mobile.forms import RadioSelectLabelAfter, SpeciesSelect
from .models import Plot, PlotObs, Relief, TreeObs, TreeSpecies


class DevelStageSelect(forms.Select):
    def create_option(self, *args, **kwargs):
        opt = super().create_option(*args, **kwargs)
        if 'historisch' in opt['label']:
            opt['attrs']['disabled'] = True
        return opt


class POFormStep1(forms.ModelForm):
    forest_edgef = forms.DecimalField(
        label="Waldrandfaktor", min_value=0.5, max_value=1.0, max_digits=2, decimal_places=1
    )
    template_name = 'mobile/form_step1.html'

    help_content = {
        'forest_edgef': {
            'title': "Abstandsberechnung für Waldrandfaktor",
            'template': 'mobile/forest_edgef.html',
        }
    }

    class Meta:
        model = PlotObs
        fields = (
            'municipality', 'inv_team', 'forest_edgef', 'stand_devel_stage',
            'stand_forest_mixture', 'soil_compaction', 'regen_type',
        )
        widgets = {
            'stand_devel_stage': DevelStageSelect,
            'inv_team': forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['municipality'].can_be_locked = True
        self.fields['municipality'].queryset = self.fields['municipality'].queryset.order_by('name')


class PlotForm(forms.ModelForm):
    exact_long = forms.FloatField(label="Genaue Länge", required=False)
    exact_lat = forms.FloatField(label="Genaue Breite", required=False)
    # A field borrowed from PlotObs
    relief = forms.ModelChoiceField(
        queryset=Relief.objects.all(), widget=RadioSelectLabelAfter, required=False, blank=True
    )
    template_name = 'mobile/form_step_plot.html'

    class Meta:
        model = Plot
        fields = ('exact_long', 'exact_lat', 'exposition', 'slope')
        widgets = {
            'exposition': RadioSelectLabelAfter,
        }

    def __init__(self, instance=None, **kwargs):
        self.plotobs = instance
        if isinstance(instance, PlotObs):
            instance = instance.plot
        super().__init__(instance=instance, **kwargs)

    def save(self, *args, **kwargs):
        if self.plotobs and 'relief' in self.cleaned_data:
            self.plotobs.relief = self.cleaned_data['relief']
            self.plotobs.save()
        plot = super().save(*args, **kwargs)
        lon, lat = self.cleaned_data.get('exact_long'), self.cleaned_data.get('exact_lat')
        if lon and lat:
            plot.point_exact = Point(lon, lat, srid=2056)
            plot.set_elevation_from_swisstopo(save=False)
            plot.save()
        return plot


class RemarkForm(forms.ModelForm):
    template_name = 'mobile/form_step_remarks.html'

    class Meta:
        model = PlotObs
        fields = ['remarks']


TREE_FIELDS = ('species', 'nr', 'distance', 'azimuth')


class TreeForm(forms.ModelForm):
    species = forms.ModelChoiceField(
        queryset=TreeSpecies.objects.filter(is_tree=True).order_by('species'),
        widget=SpeciesSelect
    )
    nr = forms.IntegerField()
    distance = forms.DecimalField(label="Distanz", max_digits=6, decimal_places=2)
    azimuth = forms.IntegerField(
        min_value=0, max_value=400, widget=forms.NumberInput(attrs={'class': 'speechEnabled'})
    )
    dbh = forms.IntegerField(
        label="BHD", min_value=12, max_value=200, widget=forms.NumberInput(attrs={'class': 'speechEnabled'})
    )
    perim = forms.IntegerField(label="Umfang", max_value=900, required=False)

    class Meta:
        model = TreeObs
        fields = TREE_FIELDS + (
            'tree', 'vita', 'dbh', 'rank', 'marked', 'stem_height', 'remarks',
        )
        widgets = {
            'tree': forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # azimuth unit depends on user choice
        self.fields['distance'].unit = 'dm'
        self.fields['dbh'].unit = 'cm'
        self.fields['stem_height'].unit = 'm'

    def other_fields(self):
        for field in self.visible_fields():
            if field.name not in TREE_FIELDS + ('perim', 'marked'):
                yield field

    def add_error(self, field, error):
        try:
            vita_code = self.fields['vita'].clean(self.data['vita']).code
        except Exception:
            pass
        else:
            if field in ('dbh', 'azimuth') and vita_code in ('c', 'x', 'y'):
                # Ignore dbh/azimuth errors for cut/not considered trees
                self.cleaned_data[field] = self.data[field]
                return
        super().add_error(field, error)

    def save(self, **kwargs):
        # Save tree fields if changed
        tree_changed = False
        for fname in TREE_FIELDS:
            tree_field = 'spec' if fname == 'species' else fname
            if getattr(self.instance.tree, tree_field) != self.cleaned_data[fname]:
                setattr(self.instance.tree, tree_field, self.cleaned_data[fname])
                tree_changed = True
        if tree_changed:
            self.instance.tree.save()
        return super().save(**kwargs)


def get_step_forms(data=None, instance=None):
    return [
        {'label': 'Schritt 1', 'form': POFormStep1(data=data, instance=instance)},
        {'label': 'Schritt 2', 'form': PlotForm(data=data, instance=instance)},
        {'label': 'Schritt 3 - Bemerkungen', 'form': RemarkForm(data=data, instance=instance)},
    ]


def get_tree_form(data=None):
    return TreeForm(data=data)

