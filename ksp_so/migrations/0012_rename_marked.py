from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0011_treeobs_markiert'),
    ]

    operations = [
        migrations.RenameField(
            model_name='treeobs',
            old_name='markiert',
            new_name='marked',
        ),
    ]
