from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0014_treespecies_order_and_abbrev_length'),
    ]

    operations = [
        migrations.AddField(
            model_name='treeobs',
            name='stem_height',
            field=models.IntegerField(blank=True, null=True, verbose_name='Stammhöhe'),
        ),
    ]
