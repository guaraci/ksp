from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0016_treespecies_abbrev_checkconstraint'),
    ]

    operations = [
        migrations.AddField(
            model_name='waldplan',
            name='owner',
            field=models.CharField(blank=True, max_length=30, verbose_name='Eigentum'),
        ),
    ]
