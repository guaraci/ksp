from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0015_treeobs_stem_height'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='treespecies',
            constraint=models.CheckConstraint(check=models.Q(('abbrev__regex', '^[a-zA-Z_]*$')), name='abbrev_unaccented'),
        ),
    ]
