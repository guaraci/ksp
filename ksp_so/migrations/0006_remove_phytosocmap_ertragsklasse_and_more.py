from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0005_phytosocmap'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='phytosocmap',
            name='ertragsklasse',
        ),
        migrations.RemoveField(
            model_name='phytosocmap',
            name='legende',
        ),
        migrations.AddField(
            model_name='phytosoc',
            name='farbcode',
            field=models.CharField(blank=True, max_length=20),
        ),
        migrations.AddField(
            model_name='phytosoc',
            name='verband',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='phytosoc',
            name='zuwachs',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
