from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0009_plot_sealevel_not_null'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='treeobs',
            name='ash_dieback',
        ),
        migrations.RemoveField(
            model_name='treeobs',
            name='crown_form',
        ),
        migrations.RemoveField(
            model_name='treeobs',
            name='crown_length',
        ),
        migrations.RemoveField(
            model_name='treeobs',
            name='damage',
        ),
        migrations.RemoveField(
            model_name='treeobs',
            name='damage_cause',
        ),
        migrations.RemoveField(
            model_name='treeobs',
            name='layer',
        ),
        migrations.RemoveField(
            model_name='treeobs',
            name='stem',
        ),
        migrations.RemoveField(
            model_name='treeobs',
            name='stem_forked',
        ),
        migrations.RemoveField(
            model_name='treeobs',
            name='stem_height',
        ),
        migrations.RemoveField(
            model_name='treeobs',
            name='woodpecker',
        ),
        migrations.DeleteModel(
            name='CrownForm',
        ),
        migrations.DeleteModel(
            name='CrownLength',
        ),
        migrations.DeleteModel(
            name='Damage',
        ),
        migrations.DeleteModel(
            name='DamageCause',
        ),
        migrations.DeleteModel(
            name='FoliageDensity',
        ),
        migrations.DeleteModel(
            name='Layer',
        ),
        migrations.DeleteModel(
            name='Stem',
        ),
    ]
