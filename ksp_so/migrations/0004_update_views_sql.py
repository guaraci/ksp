from pathlib import Path
from django.db import models, migrations

sql_dir = Path(__file__).parent.parent / 'sql_views'


def read_sql(dir_):
    for path in sorted(dir_.glob('*.sql')):
        with path.open('r') as fh:
            sql_content = fh.read()
            if 'ksp_tarif_volume' in sql_content:
                yield sql_content


class Migration(migrations.Migration):
    """
    Update views containing ksp_tarif_volume().
    """

    dependencies = [
        ('ksp_so', '0003_waldbestand'),
    ]

    operations = [migrations.RunSQL(sql_code) for sql_code in read_sql(sql_dir)]
