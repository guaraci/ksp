from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0002_create_views_sql'),
    ]

    operations = [
        migrations.CreateModel(
            name='WaldBestand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('typ', models.CharField(choices=[('90-100% Ndh', 'Nadel90'), ('50-90% Ndh', 'Nadel50'), ('90-100% Lbh', 'Laub90'), ('50-90% Lbh', 'Laub50')], max_length=20, verbose_name='Bestandestyp')),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'waldbestand',
            },
        ),
        migrations.AddField(
            model_name='waldplan',
            name='bestand',
            field=models.ForeignKey(null=True, on_delete=models.deletion.PROTECT, to='ksp_so.waldbestand'),
        ),
    ]
