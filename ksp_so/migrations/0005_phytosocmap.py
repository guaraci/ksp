import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0004_update_views_sql'),
    ]

    operations = [
        migrations.CreateModel(
            name='PhytosocMap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('massstab', models.CharField(blank=True, max_length=20)),
                ('standorteinheit', models.CharField(blank=True, max_length=20)),
                ('grundeinheit', models.CharField(blank=True, max_length=10)),
                ('legende', models.CharField(blank=True, max_length=200)),
                ('farbcode', models.CharField(blank=True, max_length=20)),
                ('verband', models.CharField(blank=True, max_length=100)),
                ('ertragsklasse', models.CharField(blank=True, max_length=100)),
                ('zuwachs', models.IntegerField()),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(srid=2056)),
            ],
            options={
                'verbose_name': 'Standortskarte',
                'verbose_name_plural': 'Standortskarten',
                'db_table': 'phytosoc_map',
            },
        ),
    ]
