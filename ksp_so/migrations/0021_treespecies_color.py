import colorful.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0020_inventory_geometry'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='color',
            field=colorful.fields.RGBColorField(blank=True, verbose_name='Farbe'),
        ),
    ]
