from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0013_plot_gps_precision'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='order',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='treespecies',
            name='abbrev',
            field=models.CharField(max_length=7, unique=True),
        ),
    ]
