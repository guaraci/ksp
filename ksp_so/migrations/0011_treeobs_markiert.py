from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0010_simplified_treeobs'),
    ]

    operations = [
        migrations.AddField(
            model_name='treeobs',
            name='markiert',
            field=models.BooleanField(default=False, verbose_name='Markiert'),
        ),
    ]
