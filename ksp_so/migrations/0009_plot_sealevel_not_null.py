from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0008_remove_phytosocmap_grundeinheit_old'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plot',
            name='sealevel',
            field=models.SmallIntegerField(verbose_name='Höhe ü. Meer'),
        ),
    ]
