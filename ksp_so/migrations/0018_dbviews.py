from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0017_waldplan_owner'),
    ]

    operations = [
        migrations.CreateModel(
            name='HolzBiomasse',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, related_name='+', serialize=False, to='ksp_so.plotobs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('biomasse_abs', models.FloatField(db_column='Biomasse', verbose_name='Biomasse [kg]')),
                ('biomasse_rel', models.FloatField(db_column='Biomasse pro ha', verbose_name='Biomasse/ha')),
            ],
            options={
                'db_table': 'bv_biomasse_pro_plotobs',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HolzBiomasseProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('biomasse_abs', models.FloatField(db_column='Biomasse', verbose_name='Biomasse [kg]')),
                ('biomasse_rel', models.FloatField(db_column='Biomasse pro ha', verbose_name='Biomasse/ha')),
            ],
            options={
                'db_table': 'bv_biomasse_pro_plotobs_und_allspec',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HolzBiomasseProSpecGroup',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('biomasse_abs', models.FloatField(db_column='Biomasse', verbose_name='Biomasse [kg]')),
                ('biomasse_rel', models.FloatField(db_column='Biomasse pro ha', verbose_name='Biomasse/ha')),
            ],
            options={
                'db_table': 'bv_biomasse_pro_plotobs_und_specgroup',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HolzProduktion',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, related_name='+', serialize=False, to='ksp_so.plotobs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('waldflaeche', models.FloatField(db_column='waldflaeche', verbose_name='theoretische Waldfläche')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HomepageView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gemeinde', models.CharField(db_column='gemeindename', max_length=50)),
                ('jahr', models.SmallIntegerField(db_column='year', verbose_name='Aufnahmejahr')),
                ('probepunkte', models.IntegerField(db_column='Anzahl Probepunkte', verbose_name='Anzahl Probepunkte')),
                ('waldflaeche', models.DecimalField(db_column='theoretische Waldfläche ha', decimal_places=1, max_digits=5, verbose_name='theoretische Waldfläche pro ha')),
                ('probebaum_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('stammzahl_ha', models.IntegerField(db_column='Stammzahl pro ha', verbose_name='Stammzahl pro ha')),
                ('stammzahl_stdf', models.FloatField(db_column='%% Standardfehler', verbose_name='Sf [%]')),
                ('volumen_ha', models.DecimalField(db_column='Volumen pro ha', decimal_places=1, max_digits=5, verbose_name='Volumen [m3 pro ha]')),
                ('volumen_stdf', models.FloatField(db_column='%% Standardfehler2', verbose_name='Sf [%]')),
                ('grundflaeche_ha', models.DecimalField(db_column='Grundflaeche pro ha', decimal_places=1, max_digits=5, verbose_name='Grundflaeche [m2 pro ha]')),
                ('grundflaeche_stdf', models.FloatField(db_column='%% Standardfehler3', verbose_name='Sf [%]')),
                ('inv_period', models.SmallIntegerField()),
            ],
            options={
                'db_table': 'bv_web_homepage_holzproduktion',
                'managed': False,
            },
        ),
    ]
