from pathlib import Path
from django.db import models, migrations

sql_dir = Path(__file__).parent.parent / 'sql_functions'


def read_sql(dir_):
    for path in sorted(dir_.glob('*.sql')):
        with path.open('r') as fh:
            yield fh.read()


class Migration(migrations.Migration):

    dependencies = []

    operations = [migrations.RunSQL(sql_code) for sql_code in read_sql(sql_dir)]

