from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0012_rename_marked'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='inventory',
            options={'ordering': ['inv_from'], 'verbose_name': 'Inventar', 'verbose_name_plural': 'Inventare'},
        ),
        migrations.AddField(
            model_name='plot',
            name='gps_precision',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True),
        ),
    ]
