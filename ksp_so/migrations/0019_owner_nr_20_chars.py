from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0018_dbviews'),
    ]

    operations = [
        migrations.AlterField(
            model_name='owner',
            name='nr',
            field=models.CharField(blank=True, max_length=20),
        ),
    ]
