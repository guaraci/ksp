import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_so', '0019_owner_nr_20_chars'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='geometry',
            field=django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056),
        ),
    ]
