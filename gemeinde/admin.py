from django.contrib.gis import admin as geo_admin

from .models import Gemeinde

class GemeindeAdmin(geo_admin.GISModelAdmin):
    list_display = ('name', 'plz', 'kanton', 'bfs_nr')
    ordering = ('name',)

geo_admin.site.register(Gemeinde, GemeindeAdmin)
