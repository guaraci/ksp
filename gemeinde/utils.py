import json
import requests
from django.contrib.gis.geos import GEOSGeometry

from .models import Gemeinde

# BFS Nummern for Kanton SO as of 1.07.2021
SO_NUMMERN = [
    2401, 2402, 2403, 2404, 2405, 2406, 2407, 2408, 2421, 2422,
    2424, 2425, 2426, 2427, 2428, 2430, 2445, 2455, 2456, 2457,
    2461, 2463, 2464, 2465, 2471, 2472, 2473, 2474, 2475, 2476,
    2477, 2478, 2479, 2480, 2481, 2491, 2492, 2493, 2495, 2497,
    2499, 2500, 2501, 2502, 2503, 2511, 2513, 2514, 2516, 2517,
    2518, 2519, 2520, 2523, 2524, 2525, 2526, 2527, 2528, 2529,
    2530, 2532, 2534, 2535, 2541, 2542, 2543, 2544, 2545, 2546,
    2547, 2548, 2549, 2550, 2551, 2553, 2554, 2555, 2556, 2571,
    2572, 2573, 2574, 2575, 2576, 2578, 2579, 2580, 2581, 2582,
    2583, 2584, 2585, 2586, 2601, 2611, 2612, 2613, 2614, 2615,
    2616, 2617, 2618, 2619, 2620, 2621, 2622,
]

GEOADMIN_API_URL = (
    'https://api3.geo.admin.ch/rest/services/api/MapServer/'
    'ch.swisstopo.swissboundaries3d-gemeinde-flaeche.fill/{bfs_num}'
)


def import_so():
    for bfs_num in SO_NUMMERN:
        response = requests.get(
            GEOADMIN_API_URL.format(bfs_num=bfs_num),
            params={'geometryFormat': 'geojson', 'sr': '2056'}
        )
        data = response.json()
        geom = GEOSGeometry(json.dumps(data['feature']['geometry']))#, srid=2056)
        geom.srid = 2056
        Gemeinde.objects.create(
            name=data['feature']['properties']['gemname'],
            bfs_nr=bfs_num, plz='?', kanton='SO',
            the_geom=geom[0],
        )
        
