import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Gemeinde',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.IntegerField()),
                ('name', models.CharField(db_column='gemeindename', max_length=50, unique=True)),
                ('bfs_nr', models.IntegerField(unique=True)),
                ('plz', models.CharField(blank=True, max_length=4)),
                ('vma_nummer', models.IntegerField()),
                ('vma_numm0', models.IntegerField()),
                ('code', models.IntegerField()),
                ('bezirk', models.CharField(max_length=2)),
                ('temp_link', models.IntegerField()),
                ('record_meaning_cd', models.IntegerField()),
                ('bbox_xmin', models.FloatField()),
                ('bbox_xmax', models.FloatField()),
                ('bbox_ymin', models.FloatField()),
                ('bbox_ymax', models.FloatField()),
                ('the_geom', django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=2056)),
                ('kanton', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'gemeinde',
                'verbose_name_plural': 'gemeinden',
                'db_table': 'gemeindegrenzen_bsbl',
            },
        ),
    ]
