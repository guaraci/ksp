from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gemeinde', '0001_squashed_0002_change_srid_to_2056'),
        ('observation', '0002_install_pg_deps_management'),
        (settings.MAIN_APP, '0002_create_views_sql'),
    ]

    operations = [
        migrations.RunSQL("""
        select public.deps_save_and_drop_dependencies(
          'public', 'gemeindegrenzen_bsbl',
          '{
            "dry_run": false,
            "verbose": false,
            "populate_materialized_view": false
          }'
        );"""),

        migrations.RemoveField(
            model_name='gemeinde',
            name='bbox_xmax',
        ),
        migrations.RemoveField(
            model_name='gemeinde',
            name='bbox_xmin',
        ),
        migrations.RemoveField(
            model_name='gemeinde',
            name='bbox_ymax',
        ),
        migrations.RemoveField(
            model_name='gemeinde',
            name='bbox_ymin',
        ),
        migrations.RemoveField(
            model_name='gemeinde',
            name='record_meaning_cd',
        ),
        migrations.RemoveField(
            model_name='gemeinde',
            name='temp_link',
        ),
        migrations.AlterField(
            model_name='gemeinde',
            name='bezirk',
            field=models.CharField(blank=True, max_length=2),
        ),
        migrations.AlterField(
            model_name='gemeinde',
            name='code',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='gemeinde',
            name='id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='gemeinde',
            name='kanton',
            field=models.CharField(max_length=2),
        ),
        migrations.AlterField(
            model_name='gemeinde',
            name='plz',
            field=models.CharField(max_length=4),
        ),
        migrations.AlterField(
            model_name='gemeinde',
            name='vma_numm0',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='gemeinde',
            name='vma_nummer',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.RunSQL("""
        UPDATE deps_saved_ddl SET ddl_statement=REPLACE(ddl_statement, 'gem.temp_link,', '') WHERE ddl_statement LIKE '%gem.temp_link%';
        """),
        migrations.RunSQL("""
        UPDATE deps_saved_ddl SET ddl_statement=REPLACE(ddl_statement, 'gem.record_meaning_cd,', '') WHERE ddl_statement LIKE '%gem.record_meaning_cd%';
        """),
        migrations.RunSQL("""
        UPDATE deps_saved_ddl SET ddl_statement=REPLACE(ddl_statement, 'gem.bbox_xmin,', '') WHERE ddl_statement LIKE '%gem.bbox_xmin%';
        """),
        migrations.RunSQL("""
        UPDATE deps_saved_ddl SET ddl_statement=REPLACE(ddl_statement, 'gem.bbox_xmax,', '') WHERE ddl_statement LIKE '%gem.bbox_xmax%';
        """),
        migrations.RunSQL("""
        UPDATE deps_saved_ddl SET ddl_statement=REPLACE(ddl_statement, 'gem.bbox_ymin,', '') WHERE ddl_statement LIKE '%gem.bbox_ymin%';
        """),
        migrations.RunSQL("""
        UPDATE deps_saved_ddl SET ddl_statement=REPLACE(ddl_statement, 'gem.bbox_ymax,', '') WHERE ddl_statement LIKE '%gem.bbox_ymax%';
        """),

        migrations.RunSQL("""
        SELECT public.deps_restore_dependencies(
          'public', 'gemeindegrenzen_bsbl',
          '{
            "dry_run": false,
            "verbose": false
          }'
        );"""),
    ]
    if settings.MAIN_APP == 'ksp_bl':
        operations.extend([
            migrations.RunSQL("REFRESH MATERIALIZED VIEW public.gemeinden_with_flaeche;"),
        ])
