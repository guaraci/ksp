import json

from django.conf import settings
from django.contrib.gis.db import models


class GemeindeManager(models.Manager):
    def get_by_natural_key(self, plz, name):
        return self.get(name=name)


class Gemeinde(models.Model):
    gid = models.AutoField(primary_key=True)
    id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=50, unique=True, db_column='gemeindename')
    bfs_nr = models.IntegerField(unique=True)
    plz = models.CharField(max_length=4)
    kanton = models.CharField(max_length=2)
    vma_nummer = models.IntegerField(blank=True, null=True)
    vma_numm0 = models.IntegerField(blank=True, null=True)
    code = models.IntegerField(blank=True, null=True)
    bezirk = models.CharField(max_length=2, blank=True)
    the_geom = models.PolygonField(srid=2056, null=True, blank=True)

    geom_field = 'the_geom'
    objects = GemeindeManager()

    class Meta:
        db_table = 'gemeindegrenzen_bsbl'
        verbose_name = 'gemeinde'
        verbose_name_plural = "gemeinden"

    def __str__(self):
        return "%s %s" % (self.plz, self.name)

    @property
    def name_with_k(self):
        """Append kanton to name if it's not a project kanton."""
        return self.name if (not self.kanton or self.kanton in settings.CANTONS) else '%s (%s)' % (self.name, self.kanton)

    def natural_key(self):
        return (self.plz, self.name)

    @classmethod
    def from_code(cls, gem_code):
        """
        Return Gemeinde object from code like A2345.
        """
        letter = gem_code[0]
        plz = gem_code[1:]
        try:
            gem = cls.objects.get(plz=plz, name__startswith=letter)
        except Gemeinde.DoesNotExist:
            return None
        return gem

    def as_geojson(self, dumped=True, srid=None):
        geom = self.the_geom
        if (srid is not None and srid != geom.srid):
            geom.transform(srid)

        geojson = {
            "type": "Feature",
            "id": self.pk,
            "crs": {"type": "name", "properties": {"name": "urn:x-ogc:def:crs:EPSG:%s" % geom.srid}},
            "properties": {
                "id": self.pk,
                "name": self.name,
                "bfs_nr": self.bfs_nr,
                "plz": self.plz,
                "bezirk": self.bezirk,
                "kanton": self.kanton,
            },
            "geometry": json.loads(geom.json),
        }
        if dumped:
            return json.dumps(geojson)
        return geojson
