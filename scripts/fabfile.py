import getpass
from fabric import task
from invoke import Context, Exit

MAIN_HOST = 'guaraci.vserver.softronics.ch:2222'

default_db_owner = 'claude'
venv_base = "/var/www/virtualenvs/"

projects = {
    'ksp': {'venv': venv_base + 'ksp3', 'db': 'ksp2'},
    'ksptest': {'venv': venv_base + 'ksp3', 'db': 'ksptest'},
    'ksp_so': {'venv': venv_base + 'ksp_so', 'db': 'ksp_so'},
    'ksp_zg': {'venv': venv_base + 'ksp_zg', 'db': 'ksp_zg'},
    'valforet': {'venv': venv_base + 'valforet', 'db': 'valforet'},
}

@task(hosts=[MAIN_HOST])
def deploy(conn, project):
    if project not in projects:
        raise ValueError(f"project is not in {', '.join(projects.keys())}")
    python_exec = f"{projects[project]['venv']}/bin/python"

    with conn.cd(f"/var/www/{project}"):
        # activate maintenance mode
        conn.run('sed -i -e "s/UPGRADING = False/UPGRADING = True/" ksp/wsgi.py')
        conn.run('git stash && git pull && git stash pop')
        conn.run('git submodule update --init --recursive')  # for mobile_base
        if project in ('ksp_zg', 'valforet'):
            with conn.cd(f"/var/www/{project}/{project}"):
                conn.run('git pull')
        conn.run('%s manage.py migrate' % python_exec)
        conn.run('%s manage.py compilemessages' % python_exec)
        conn.run('%s manage.py collectstatic --noinput' % python_exec)
        conn.run('sed -i -e "s/UPGRADING = True/UPGRADING = False/" ksp/wsgi.py')


@task(hosts=[MAIN_HOST])
def clone_to_testdb(conn):
    dbname = 'ksp2'
    tmp_path = f'/tmp/{dbname}.dump'
    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.sudo(f'pg_dump -Fc {dbname} > {tmp_path}', user='postgres')
    conn.sudo('dropdb ksptest', user='postgres')
    # recreate ksp_test
    conn.sudo('psql -c "CREATE DATABASE ksptest OWNER=ksp_django;"', user='postgres')
    conn.sudo(f'psql -d ksptest -c "CREATE SCHEMA postgis;"', user='postgres')
    conn.sudo(f'psql -d ksptest -c "ALTER DATABASE ksptest SET search_path = public,postgis;"', user='postgres');
    conn.sudo('psql -c "CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA postgis;"', user='postgres')
    # load dump
    conn.sudo(f'pg_restore -d ksptest {tmp_path}', user='postgres')


@task(hosts=[MAIN_HOST])
def clone_remote_db(conn, project):
    """ Dump a remote database and load it locally """
    if project not in projects:
        raise ValueError(f"project is not in {', '.join(projects.keys())}")

    dbname = projects[project]['db']
    local = Context()
    django_schema = 'django' if dbname in ('ksp_zg',) else 'public'

    def exist_local_db(db):
        res = local.run('psql --list', hide='stdout')
        return db in res.stdout.split()

    def exist_username(user):
        res = local.run('psql -d postgres -c "select usename from pg_user;"', hide='stdout')
        return user in res.stdout.split()

    tmp_path = f'/tmp/{dbname}.dump'
    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.sudo(f'pg_dump --no-owner --no-privileges --exclude-schema=postgis -Fc {dbname} > {tmp_path}', user='postgres')
    conn.get(tmp_path, None)

    if exist_local_db(dbname):
        rep = input(f'A local database named "{dbname}" already exists. Overwrite? (y/n)')
        if rep == 'y':
            local.run(f'psql -d postgres -c "DROP DATABASE {dbname};"')
        else:
            raise Exit("Database not copied")

    if exist_username(dbname):
        owner = dbname
    else:
        owner = default_db_owner
    local.run(f'psql -d postgres -c "CREATE DATABASE {dbname} OWNER={owner};"')
    local.run(f'psql -d {dbname} -c "CREATE SCHEMA postgis;"')
    local.run(f'psql -d {dbname} -c "ALTER DATABASE {dbname} SET search_path = public,postgis"');
    if django_schema != 'public':
        local.run(f'psql -d {dbname} -c "CREATE SCHEMA django;"')
        local.run(f'psql -d {dbname} -c "ALTER DATABASE {dbname} SET search_path = public,postgis,django"');
    local.run(f'psql -d {dbname} -c "CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA postgis;"')
    local.run(f'pg_restore --no-owner --no-privileges -d {dbname} {dbname}.dump')
