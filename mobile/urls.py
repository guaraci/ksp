from django.contrib.auth.views import LogoutView
from django.urls import path

from observation.views import PlotObsDetailView
from . import views

# Service worker is hooked at root level.
urlpatterns = [
    path('', views.main, name="mobile-home"),
    path('login/', views.MobileLoginView.as_view(), name='mobile-login'),
    path('logout/', LogoutView.as_view(), name='mobile-logout'),
    path('manifest.json', views.manifest, name="manifest.json"),
    path('inventory/list/', views.InventoryListView.as_view(), name="inventory-list"),
    path('inventory/<int:pk>/geojson/', views.inventory_view, name="inventory-geojson"),
    path('inventory/<int:pk>/plots/', views.inventory_plots, name="inventory-plots"),
    path('plotobs/sync/', views.sync_view, name="mobile-sync"),
    # Duplicate 'plotobs_detail_json' url for /mobile (cached) namespace
    path('plotobs/<int:pk>/json/', PlotObsDetailView.as_view(output='json'),
        name='mobile_plotobs_detail_json'),
]
