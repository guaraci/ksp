var CACHE_NAME = 'kspmobile-v1',
    debug = false,
    loginRequired = false,
    // No need to cache much at install time considering our "get-from-cache-or-fetch-and-cache" strategy.
    urlsToCache = [
      '/mobile/static/img/locked.svg',
      '/mobile/static/img/unlocked.svg',
      '/mobile/static/img/online.svg',
      '/mobile/static/img/offline.svg',
      '/mobile/static/img/vert-horiz.png',
      '/mobile/static/img/plan.png',
      '/mobile/static/img/satellite.png',
      '/mobile/static/img/back.svg',
      '/mobile/static/img/okcontinue.svg',
      '/mobile/static/img/cancel.svg',
      '/mobile/static/img/newtree.svg'
    ];

self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(CACHE_NAME).then(cache => {
            console.log(`Initially cached ${urlsToCache.length} images`);
            // Try to cache with or without the /mobile prefix
            cache.addAll(urlsToCache);
            cache.addAll(urlsToCache.map(url => url.replace('/mobile/', '/')));
        })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(async function() {
        const cache = await caches.open(CACHE_NAME);
        const cachedResponse = await cache.match(event.request);
        let networkResponse = null;

        //event.waitUntil(async function() {
        try {
            networkResponse = await fetch(event.request);
        } catch(err) {
            return cachedResponse;
        }
        if (!networkResponse ||networkResponse.status !== 200 || networkResponse.type !== 'basic') {
            return networkResponse;
        }
        if (event.request.url.indexOf("/login/") < 0 && event.request.method == 'GET') {
            console.log("Caching " + event.request.url);
            await cache.put(event.request, networkResponse.clone());
        }
        //}());
        return networkResponse || cachedResponse;
    }());
});
