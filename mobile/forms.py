from django import forms
from django.core.exceptions import FieldError
from django.utils.functional import cached_property

from observation.common_models import TreeSpecies


class RadioSelectLabelAfter(forms.RadioSelect):
    option_template_name = 'mobile/widgets/radio_option.html'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.attrs['class'] = 'radioselect'


class RadioNullBoolean(forms.NullBooleanSelect):
    input_type = 'radio'
    template_name = 'django/forms/widgets/radio.html'
    option_template_name = 'mobile/widgets/radio_option.html'
    # Restore defaults for ChoiceWidget
    checked_attribute = {'checked': True}
    option_inherits_attrs = True
    add_id_index = True


class SpeciesSelect(forms.Select):
    # Customized to be able to set species abbrev as data attribute of options
    option_template_name = 'mobile/widgets/species_select_option.html'

    @cached_property
    def historic_ids(self):
        try:
            return list(TreeSpecies.objects.filter(is_historic=True).values_list('pk', flat=True))
        except FieldError:
            return []

    def create_option(self, *args, **kwargs):
        option = super().create_option(*args, **kwargs)
        if option['value'] in self.historic_ids:
            option['attrs']['data-historic'] = 'true'
        return option
