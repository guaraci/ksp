import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse

import geckodriver_autoinstaller

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.webdriver import WebDriver as ChromeDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import FirefoxProfile, WebDriver as FirefoxDriver
from selenium.webdriver.support.ui import Select


class MobileSeleniumTestsBase(StaticLiveServerTestCase):
    _overridden_settings = {'DEBUG': True}  # To obtain any traceback in the terminal!

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        geckodriver_autoinstaller.install()
        #driver = 'chrome'
        driver = 'firefox'
        if driver == 'chrome':
            cls.selenium = ChromeDriver()
        elif driver == 'firefox':
            prf = FirefoxProfile()
            prf.set_preference("geo.prompt.testing", True)
            prf.set_preference("geo.prompt.testing.allow", True)
            cls.selenium = FirefoxDriver(prf)
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def setUp(self):
        super().setUp()
        self.setUpTestData()

    def login(self):
        self.selenium.get('%s%s' % (self.live_server_url, reverse('mobile-home')))
        try:
            username_input = self.selenium.find_element(By.NAME, "username")
        except NoSuchElementException:
            # Assume the user is already logged in.
            return
        username_input.send_keys('user')
        password_input = self.selenium.find_element(By.NAME, "password")
        password_input.send_keys('secret')
        self.selenium.find_element(By.XPATH, '//button[@type="submit"]').click()

    def go_to_inventory(self, name, degree=False):
        inventory_select = Select(self.selenium.find_element(By.ID, "inventory-choice"))
        time.sleep(2)  # setting up current geolocation may take time
        inventory_select.select_by_visible_text("Test inventory")
        if (degree):
            self.selenium.find_element(By.XPATH, '//input[@value="deg"]').click()
        self.selenium.find_element(By.ID, "inventory-ok").click()
        time.sleep(1)  # getting and displaying plots may take time

    def go_to_plotobs(self):
        for marker in self.selenium.find_elements(By.CLASS_NAME, 'leaflet-marker-icon'):
            classes = marker.get_attribute('class').split()
            if 'icon-blue' in classes:
                break
        else:
            self.fail("No marker with icon-blue class")
        marker.click()

    def active_select_options(self, element_id):
        """Return the list of active options of a <select> element"""
        return [
            opt.text
            for opt in Select(self.selenium.find_element(By.ID, element_id)).options
            if opt.is_enabled()
        ]

    def send_caliper_values(self, species='', diameter=None, distance=None, line=None):
        values = []
        if species:
            values.append(f'$PHGF,SPC,2,{species},*2B')
        if diameter is not None and distance is not None:
            # Only 20 chars can be sent at once by Bluetooth
            value = f'$PHGF,DSTDIA,M,{diameter},{distance},*38'
            values.extend([value[:20], value[20:]])
        elif diameter is not None:
            values.append(f'$PHGF,DIA,M,{diameter},*2A')
        elif line is not None:
            values.append(line)
        lines = ['window.app.caliper.fakeCaliper();'] + [f"window.app.caliper.sendFakeValue('{val}')" for val in values if val]
        self.selenium.execute_script('\n'.join(lines))
