import importlib
import json
from datetime import date
from decimal import Decimal
from pprint import pformat

from django.apps import apps
from django.conf import settings
from django.contrib.auth.views import LoginView
from django.contrib.gis.db.models.functions import Transform
from django.core.exceptions import ValidationError
from django.core.mail import mail_admins
from django.db import IntegrityError, transaction
from django.forms import BooleanField
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic import View

from observation.common_models import (
    Gemeinde, Inventory, Plot, PlotObs, Tree, TreeObs, TreeSpecies,
)
try:
    from observation.common_models import WaldBestandKarte
except ImportError:
    WaldBestandKarte = None

forms_module = importlib.import_module(f'{settings.MAIN_APP}.forms_mobile')


class MobileLoginView(LoginView):
    template_name = 'mobile/login.html'
    title = settings.MOBILE_TITLE


def main(request):
    context = {
        'title': settings.MOBILE_TITLE,
        'gemeinden': Gemeinde.objects.all().order_by('name'),
        'step_forms': forms_module.get_step_forms(),
        'tree_form': forms_module.get_tree_form(),
        'has_tree_position': True,
        'verbose_names':  {
            field.name: field.verbose_name for field in PlotObs._meta.fields
            if not field.name.startswith('i')
        },
        'slope_mapping': Plot.slope_mapping,
        'static_prefix': reverse('mobile-home').rstrip('/'),
    }
    context['verbose_names'].update({
        'year': _("Year"),
        'gps_precision': _("GPS precision"),
        'radius': _("Radius"),
    })

    return render(request, 'mobile/index.html', context)


def manifest(request):
    return render(request, 'mobile_base/manifest.json', {})


class InventoryListView(View):
    """This view returns a list of all accessible inventories."""
    def get(self, request, *args, **kwargs):
        current_invents = Inventory.objects.filter(
            inv_from__lte=date.today(), inv_to__gte=date.today()
        )
        return JsonResponse([
            {'pk': inv.pk,  'name': inv.name, 'url': reverse('inventory-geojson', args=[inv.pk])}
            for inv in current_invents
        ], safe=False)


def inventory_view(request, pk):
    """This view returns a GeoJSON with Inventory data."""
    inventory = get_object_or_404(Inventory, pk=pk)
    inventory.geom.transform(4326)
    geojson = {
        "type": "Feature",
        "id": inventory.pk,
        "properties": {
            "name": inventory.name,
            "type": getattr(inventory, 'typ', 'standard'),
            "plotsURL": reverse('inventory-plots', args=[inventory.pk]),
            "layerURLs": inventory.layer_urls(),
        },
        "geometry": json.loads(inventory.geom.json),
    }
    if apps.is_installed('afw'):
        geojson["properties"]["layerURLs"]['Erschliessung'] = reverse('afw-erschliessung', args=[inventory.pk])
    return JsonResponse(geojson)


def inventory_plots(request, pk):
    inventory = get_object_or_404(Inventory, pk=pk)
    gemeinden = Gemeinde.objects.filter(the_geom__intersects=inventory.geom)

    def get_gemeinde(pt):
        for gem in gemeinden:
            if gem.the_geom.contains(pt):
                return gem

    geojson = {"type": "FeatureCollection", "features": []}
    plot_qs = inventory.get_plots(srid=4326)

    for plot in plot_qs:
        geojson["features"].append(plot.as_geojson(
            geom_field='plotgeomexact' if getattr(plot, 'plotgeomexact', False) else 'plotgeom', for_mobile=True
        ))
        for year in geojson["features"][-1]["properties"]["obsURLs"].keys():
            geojson["features"][-1]["properties"]["obsURLs"][year] += '?srid=4326'
        gemeinde = get_gemeinde(plot.the_geom)
        geojson["features"][-1]["properties"]["municipality_id"] = gemeinde.pk if gemeinde else ''
        geojson["features"][-1]["properties"]["municipality_name"] = str(gemeinde) if gemeinde else ''
        geojson["features"][-1]["properties"]["coords_2056"] = [
            int(round(c, 0)) for c in plot.the_geom.coords
        ]
        if getattr(plot, 'point_exact', None):
            geojson["features"][-1]["properties"].update({
                "coords_2056_exact": [
                    round(c, 1) for c in plot.point_exact.coords
                ],
                # This will populate the plot form step
                "exact_long": plot.point_exact.coords[0],
                "exact_lat": plot.point_exact.coords[1],
            })
        else:
            geojson["features"][-1]["properties"]["coords_2056_exact"] = None
        # Add infos from Bestandeskarte
        if WaldBestandKarte:
            try:
                bestand = WaldBestandKarte.objects.select_related(
                    'entwicklungstufe', 'mischungsgrad', 'schlussgrad'
                ).get(geom__contains=plot.the_geom)
            except (WaldBestandKarte.DoesNotExist, WaldBestandKarte.MultipleObjectsReturned):
                pass
            else:
                def to_list(val):
                    return [val.pk, str(val)] if val is not None else []
                geojson["features"][-1]["properties"]["stand_devel_stage"] = to_list(
                    bestand.entwicklungstufe)
                geojson["features"][-1]["properties"]["stand_forest_mixture"] = to_list(
                    bestand.mischungsgrad)
                geojson["features"][-1]["properties"]["stand_crown_closure"] = to_list(
                    bestand.schlussgrad)
    return JsonResponse(geojson)


class TreeDataError(IntegrityError):
    # subclass of IntegrityError to make the transaction rollback
    pass


class HttpResponseWithError(HttpResponseForbidden):
    def __init__(self, data, error, user):
        mail_admins(
            f'Sync error on {settings.ALLOWED_HOSTS[0]}',
            ('%s\n'
             'User: %s\n\n'
             'Sent data:\n%s') % (error, user, pformat(data))
        )
        super().__init__(error)


@csrf_exempt
@require_POST
def sync_view(request):
    """
    This view receives (in the request body) JSON-formatted data to sync PlotObs
    data entered in the mobile app.
    """
    data = json.loads(request.body.decode('utf-8'))
    with transaction.atomic():
        # This will prevent parallel saving of PlotObs for the same plot.
        plot = get_object_or_404(Plot.objects.select_for_update(), pk=int(data['plot']))
        errs = sync_data(plot, data, request.user)
    if errs:
        return HttpResponseWithError(data, "Errors: %s" % ", ".join([str(err) for err in errs]), request.user)
    else:
        return HttpResponse()


def sync_data(plot, data, user):
    plotobs_props = data['features'][0]['properties']

    try:
        year = int(plotobs_props.get('year'))
    except TypeError:
        return ["year expected, received '%s'" % plotobs_props.get('year')]
    if year != date.today().year:
        return ["Unable to save a PlotObs for another year"]

    try:
        plotobs = PlotObs.get_from_mobile_data(plotobs_props, plot)
    except ValueError as err:
        return [f"Unable to get plotobs from data ({err})"]
    plotobs.operator = user

    valid_forms = []
    errs = []
    for key, val in plotobs_props.items():
        if isinstance(val, list) and len(val) == 2:
            # FIXME: this might be a bit too aggressive (if a field does expect a list...)
            if val == ['', '']:
                plotobs_props[key] = ''
            else:
                plotobs_props[key] = val[0]

    for form_data in forms_module.get_step_forms(data=plotobs_props, instance=plotobs):
        form = form_data['form']
        if not form.is_valid():
            if hasattr(form, 'forms'):  # Formset
                for subform in form.forms:
                    if not subform.is_valid():
                        errs.append(form.errors.as_text())
            else:
                errs.append(form.errors.as_text())
            continue
        valid_forms.append(form)

    if errs:
        return errs

    previous_obs = plotobs.previous_obs()
    required_trees = {to.tree for to in previous_obs.not_cut_trees()} if previous_obs else set()
    try:
        with transaction.atomic():
            for form in valid_forms:
                form.save()

            # Save new TreeObs
            seen_trees = set()
            for tree_feature in data['features'][1:]:
                tree_id = tree_feature['properties']['tree']
                if tree_id and tree_id in seen_trees:
                    # Some client-side bug *may* produce duplicates
                    continue
                try:
                    treeobs = save_treeobs(plotobs, tree_feature)
                except ValidationError as err:
                    errs.append(err)
                else:
                    required_trees.discard(treeobs.tree)
                seen_trees.add(tree_id)
            # Checks
            if len(required_trees) > 0 and not errs and plotobs.forest_edgef >= 0.6:
                errs.append("Some previous trees are missing new data")
            if errs:
                # Raising this error will rollback the transaction
                raise TreeDataError("Errors in observation data")
    except TreeDataError:
        # Error already saved, just ignore the error
        pass
    except IntegrityError as err:
        errs.append(err)
    if errs:
        return errs

    return []


def save_treeobs(plotobs, tree_feature):
    if tree_feature['properties']['tree'] == '':
        # New tree
        spec_id = tree_feature['properties']['species'][0]
        if spec_id == '':
            raise ValidationError("The tree species is mandatory")
        spec = TreeSpecies.objects.get(pk=spec_id)
        tree = Tree.objects.create(
            plot=plotobs.plot, spec=spec,
            nr=tree_feature['properties']['nr'],
            azimuth=int(tree_feature['properties']['azimuth']),
            distance=Decimal(tree_feature['properties']['distance']),
        )
    else:
        tree = Tree.objects.get(pk=int(tree_feature['properties']['tree']))
    try:
        from observation.common_models import SurveyType
        treeobs = TreeObs(obs=plotobs, tree=tree, survey_type=SurveyType.objects.get(code='K'))
    except ImportError:
        treeobs = TreeObs(obs=plotobs, tree=tree)
    # Set TreeObs values from tree_feature['properties']
    errs = []

    data = {}
    for key, value in tree_feature['properties'].items():
        # Select choices come in the form [pk, human-readable]
        data[key] = value[0] if isinstance(value, list) else value
    data['tree'] = tree.pk
    form = forms_module.TreeForm(instance=treeobs, data=data)
    if not form.is_valid():
        raise ValidationError(", ".join('%s: %s' % (k, v) for k, v in form.errors.items()))
    treeobs = form.save()
    return treeobs


def map_redirect(request):
    return HttpResponseRedirect('https://geowms.bl.ch/?' + request.environ['QUERY_STRING'])
