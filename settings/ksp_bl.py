from .base import *  # NOQA

INSTALLED_APPS.extend([
    #'imports',
    'gemeinde',
    'ksp_bl',
    'mobile_base',
    'mobile',
    'afw',
    'tariff',
])
MAIN_APP = 'ksp_bl'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'ksp2',
        'USER': '',
        'PASSWORD': '',
    },
    'afw': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'afw',
    }
}

AUTH_USER_MODEL = "ksp_bl.User"

TEMPLATES[0]['OPTIONS']['context_processors'].append('ksp_bl.context_processors.project')

CANTONS = ['BS', 'BL']

DEFAULT_LON = 2614200
DEFAULT_LAT = 1262600
