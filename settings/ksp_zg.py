from .base import *  # NOQA

INSTALLED_APPS.append('ksp_zg')

MAIN_APP = 'ksp_zg'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'ksp_zg',
        'USER': '',
        'PASSWORD': '',
        'OPTIONS': {
            'options': '-c search_path=public,postgis,django'
        },
    },

}
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

AUTH_USER_MODEL = "ksp_zg.User"

TEMPLATES[0]['OPTIONS']['context_processors'].append('ksp_zg.context_processors.project')

CANTONS = ['ZG']

DEFAULT_LON = 2680000
DEFAULT_LAT = 1224000
