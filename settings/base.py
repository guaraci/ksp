# Django settings for ksp project.
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = False

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'ksp2',
        'USER': '',
        'PASSWORD': '',
    },
    'afw': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'afw',
    }
}
DATABASE_ROUTERS = ['ksp.routers.DefaultRouter']
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

TIME_ZONE = 'Europe/Zurich'

LANGUAGE_CODE = 'de'

USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'remote_finder.RemoteFinder',
)

MIDDLEWARE = [
    # ConditionalGetMiddleware is important for the mobile app, where only HTTP 200 responses are re-cached
    'django.middleware.http.ConditionalGetMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'ksp.middleware.LoginRequiredMiddleware',
]

ROOT_URLCONF = 'ksp.urls'

LOGIN_URL = '/login/'
LOGOUT_REDIRECT_URL = '/'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'ksp.wsgi.application'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                # Default list
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
            'builtins': ['django.templatetags.static'],
            'loaders': [
                # Prioritize app_directories loader
                ('django.template.loaders.cached.Loader', [
                    'django.template.loaders.app_directories.Loader',
                    'django.template.loaders.filesystem.Loader',
                ]),
            ],
        },
    },
]

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.gis',
    'leaflet',
    'colorful',
    'document',
    'observation',
]

REMOTE_FINDER_CACHE_DIR = os.path.join(BASE_DIR, 'remote-finder-cache')

REMOTE_FINDER_RESOURCES = [
    ('js/ol3.js', 'https://cdnjs.cloudflare.com/ajax/libs/ol3/3.20.1/ol.js',
     'md5:d389450e4b75a69bb4ed7ce4b62ca49d'),
    ('css/ol.css', 'https://cdnjs.cloudflare.com/ajax/libs/ol3/3.20.1/ol.css',
     'md5:6bf5b77decf2e63357f1e912f5052fc7'),

    ('js/jquery-1.12.0.min.js', 'http://code.jquery.com/jquery-1.12.0.min.js',
     'md5:cbb11b58473b2d672f4ed53abbb67336'),
    ('js/jquery-ui.min.js', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
     'md5:d935d506ae9c8dd9e0f96706fbb91f65'),

    ('css/themes/smoothness/jquery-ui.css',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css',
     'md5:64dfb75ef30cbf691e7858dc1992b4df'),
    ('css/themes/smoothness/images/ui-bg_flat_0_aaaaaa_40x100.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_flat_0_aaaaaa_40x100.png',
     'md5:21f222d245c6229bf1086527335c49e8'),
    ('css/themes/smoothness/images/ui-bg_flat_75_ffffff_40x100.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_flat_75_ffffff_40x100.png',
     'md5:985f46b74b703e605966201743fb6f7e'),
    ('css/themes/smoothness/images/ui-bg_highlight-soft_75_cccccc_1x100.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_highlight-soft_75_cccccc_1x100.png',
     'md5:dd13068fa67603e4ed7302f69af8d905'),
    ('css/themes/smoothness/images/ui-bg_glass_65_ffffff_1x400.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_glass_65_ffffff_1x400.png',
     'md5:177bd763ea7a4ac8fcb1dc94570dd833'),
    ('css/themes/smoothness/images/ui-bg_glass_75_dadada_1x400.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_glass_75_dadada_1x400.png',
     'md5:2d6f73d4cc0d5c0e6bb8b05c2ebc1d11'),
    ('css/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png',
     'md5:5c579dd5f9969ffdc83a00007271684c'),
    ('css/themes/smoothness/images/ui-icons_888888_256x240.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-icons_888888_256x240.png',
     'md5:0979e30214877ac97c47cd1ea71a1058'),
    ('css/themes/smoothness/images/ui-icons_222222_256x240.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-icons_222222_256x240.png',
     'md5:5b5ec59318bb5f73baf58fcbfeca4e46'),
    ('css/themes/smoothness/images/ui-icons_454545_256x240.png',
     'https://code.jquery.com/ui/1.11.4/themes/smoothness/images/ui-icons_454545_256x240.png',
     'md5:aa541bdd3a9d7e85aa84de5342c5712c'),

    ('js/jquery.cookie.js',
     'https://raw.githubusercontent.com/carhartl/jquery-cookie/master/src/jquery.cookie.js',
     'md5:0f1f6cd6e0036897019b376d38593403'),
    ('js/jquery.tablesorter.min.js',
     'https://raw.githubusercontent.com/christianbach/tablesorter/master/jquery.tablesorter.min.js',
     'md5:28f91818bc0e61a9b5445eed72e45ee5'),

    ('js/highlight.min.js',
     'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.3.0/highlight.min.js',
     'md5:e663cb8c64830ade4ae4795e150d29e3'),
    ('css/highlight.default.min.css',
     'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.3.0/styles/default.min.css',
     'md5:5133d11fbaf87d3978cf403eba33c764'),
]

TEST_RUNNER = 'ksp.custom_runner.CustomRunner'

REDIRECT_TILES = False

# Used in mobile login template and as main window title
MOBILE_TITLE = 'KSP Mobile App'
