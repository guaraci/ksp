from .base import *  # NOQA

INSTALLED_APPS.extend([
    'valforet',
    'mobile_base',
    'mobile',
])

MAIN_APP = 'valforet'

LANGUAGE_CODE = 'fr'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'valforet',
        'USER': '',
        'PASSWORD': '',
        'OPTIONS': {
            'options': '-c search_path=public,postgis'
        },
    },
}

DEFAULT_LON = 2575000
DEFAULT_LAT = 1232500

MOBILE_TITLE = "VALFORÊT - Saisie placettes"
