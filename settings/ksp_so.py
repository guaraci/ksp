from .base import *  # NOQA

INSTALLED_APPS.extend([
    'gemeinde',
    'mobile_base',
    'mobile',
    'ksp_so',
])

MAIN_APP = 'ksp_so'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'ksp_so',
        'USER': '',
        'PASSWORD': '',
    },

}

AUTH_USER_MODEL = "ksp_so.User"

TEMPLATES[0]['OPTIONS']['context_processors'].append('ksp_so.context_processors.project')

CANTONS = ['SO']

DEFAULT_LON = 2616800
DEFAULT_LAT = 1240300
