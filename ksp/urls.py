import sys

from django.conf import settings
from django.conf.urls import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.staticfiles.views import serve as static_serve
from django.urls import include, path
from django.views.decorators.cache import cache_page
from django.views.i18n import JavaScriptCatalog

from observation import views

urlpatterns = [
    path('', include(f'{settings.MAIN_APP}.urls')),
]

urlpatterns += [
    path('admin/', admin.site.urls),
    path('jsi18n/', cache_page(86400, key_prefix='jsi18n-1')(JavaScriptCatalog.as_view()),
         name='javascript-catalog'),

    path('gemeinde/<int:pk>/', views.GemeindeDetailView.as_view(), name='gemeinde'),
    path('gemeinde/<int:pk>/plots/', views.GemeindePlotsView.as_view(), name='gemeinde-plots'),
    path('gemeinden_in_box/', views.GemeindenByExtent.as_view(), name='gemeinden-by-extent'),
    path('region/geojson/', views.AdminRegionGeoJson.as_view(), name='region-geojson'),
    path('perimeter/geojson/', views.PerimeterGeoJson.as_view(), name='perimeter-geojson'),

    path('inventory/new/', views.InventoryEditView.as_view(), name='inventory-new'),
    path('inventory/<int:pk>/', views.InventoryView.as_view(), name='inventory'),
    path('inventory/<int:pk>/plotobs/', views.InventoryPlotObsView.as_view(),
        name='inventory-plotobs'),
    path('inventory/<int:pk>/plotsnoobs/', views.InventoryPlotNoObsView.as_view(),
        name='inventory-plots-no-obs'),
    path('inventory/<int:pk>/exclude_plots/', views.InventoryExcludePlotsView.as_view(),
        name='inventory-exclude-plots'),
    path('inventory/<int:pk>/plot/new/', views.InventoryAddPlotView.as_view(), name='inventory-new-plot'),
    path('inventory/<int:pk>/plot/fromshape/', views.InventoryPlotsFromShape.as_view(), name='inventory-new-plots-shape'),
    path('inventory/<int:pk>/plot/dividedensity/', views.InventoryPlotsDivideDensity.as_view(), name='inventory-divide-density'),
    path('inventory/<int:pk>/savepolygon/', views.InventorySavePolygonView.as_view(), name='inventory-savepolygon'),
    # Views returning GeoJSON data
    path('plotobs/', views.PlotObsDataView.as_view(), name='plotobs'),
    path('plotobs/<int:pk>/json/', views.PlotObsDetailView.as_view(output='json'), name='plotobs_detail_json'),
    path('plotobs/<int:pk>/', views.PlotObsDetailView.as_view(output='html'), name='plotobs_detail'),
    path('plot/<int:pk>/', views.PlotDetailView.as_view(), name='plot_detail'),
    path('inventory/<int:pk>/plot/<int:plot_pk>/infos/', views.PlotDetailEmbedView.as_view(), name='plot_detail_embed'),
    path('waldbestand/<int:inventory_pk>/', views.waldbestand, name='obs-bestand'),
    # Temporary view:
    path('plot/tobechecked/', views.plots_to_check),
    path('tree/reconcile/', views.TreeReconcileView.as_view(), name='tree_reconcile'),
    path('tree/<int:pk>/edit/', views.TreeEditView.as_view(), name='tree_edit'),
    path('data/', views.DataPageView.as_view(), name='data_page'),
    path('data/grid/', views.DataGridView.as_view(), name='data_grid'),
    path('data/<str:model>/', views.ModelView.as_view(), name='data_model'),
    path('docs/', views.DocumentationView.as_view(), name='docs'),
    path('docs/glossar/', views.TemplateView.as_view(template_name='glossar.html'), name='glossar'),
    path('docs/<slug:slug>/', views.DocSpecificView.as_view(), name='doc-specific'),
    path('backups/<str:filename>/', views.serve_backup, name='serve-backup'),

    # Introspection views
    path('view_def/', views.ViewList.as_view(), name='view-list'),
    path('view_def/<int:oid>/', views.ViewDefinition.as_view(), name='view_def'),
    path('table_def/', views.TableListView.as_view(), name='table-list'),
    path('table_def/<str:oid_or_name>/', views.TableDefinitionView.as_view(), name='table_def'),
    path('func_def/<str:func_name>/', views.FunctionDefinitionView.as_view(), name='func_def'),

    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    path('chart/<str:view_name>/', views.chart, name="view_chart"),
    path('SPCNAME.TXT', views.SpeciesExportView.as_view(), name='spcname'),
]

if 'mobile' in settings.INSTALLED_APPS:
    urlpatterns += [
        # Hooked at root level so as the scope can be for the whole project
        path('serviceworker.js', static_serve, kwargs={
            'path': 'js/serviceworker.js', 'insecure': True}),
    ]

if 'imports' in settings.INSTALLED_APPS:
    from imports import views as import_views
    urlpatterns += [
        path('import/form/', import_views.ImportView.as_view(), name="import"),
        path('import/', import_views.ImportedListView.as_view(), name="import_list"),
    ]

TESTING = len(sys.argv) > 1 and sys.argv[1] == 'test'

if settings.DEBUG or TESTING:
    # Serve media files in development
    urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # Serve static mobile alias in developement
    urlpatterns += [
        path('mobile/static/<path:path>', static_serve),
    ]
    # Serve favicon in development
    urlpatterns.append(
        path('favicon.ico', static_serve, kwargs={'path': 'icons/favicon.ico'})
    )
