class DefaultRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'afw':
            return 'afw'
        return None

    def db_for_write(self, model, **hints):
        """
        afw app writes to the afw db.
        """
        if model._meta.app_label == 'afw':
            if not model._meta.managed:
                raise Exception("The afw database is read only")
            return 'afw'
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Only sync afw models on afw database (managed only during tests).
        """
        if 'model' in hints and not hints['model']._meta.managed:
            return False
        if (app_label == 'afw' and db != 'afw') or (db == 'afw' and app_label != 'afw'):
            return False
        return None

