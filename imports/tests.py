import os

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Point
from django.core.files import File
from django.test import TestCase
from django.urls import reverse

from observation.models import OBSERVATION_FIXTURES, Plot, PlotObs, RegenObs, Tree, TreeObs
from .models import FileImport
from .imports import Importer, ImportErrors

TEST_FILES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "test_files")


class ImportTests(TestCase):
    fixtures = ('gemeinde.json',) + OBSERVATION_FIXTURES

    def tearDown(self):
        dir_to_clean = os.path.join(settings.MEDIA_ROOT, 'imported')
        for f in os.listdir(dir_to_clean):
            if f.startswith(("sample", "municipality")):
                os.remove(os.path.join(dir_to_clean, f))

    def _import_test_file(self, file_name):
        file_path = os.path.join(TEST_FILES_DIR, file_name)
        with open(file_path, mode='rb') as f:
            fi = FileImport.objects.create(ifile=File(f, name=os.path.basename(f.name)))
        Importer().do_import(fi)
        return fi

    def test_xml_sample_import(self):
        imp = self._import_test_file("sample.xml")
        self.assertEqual(sorted(list(Plot.objects.all().values_list('nr', flat=True))), [78, 78, 80, 81])
        self.assertEqual(Plot.objects.count(), 4)
        self.assertEqual(PlotObs.objects.count(), 6)
        self.assertEqual(PlotObs.objects.filter(forest_clearing=True).count(), 1)
        self.assertEqual(TreeObs.objects.count(), 57)
        self.assertEqual(Tree.objects.count(), 34)
        self.assertEqual(RegenObs.objects.count(), 10)
        self.assertEqual(imp.warnings, '')
        plot1 = Plot.objects.get(nr=78, igis=0)
        plot_obs1 = plot1.plotobs_set.filter(year='1998')[0]
        self.assertEqual(plot_obs1.remarks,
            "spz nur 3m unterhalb rückegasse auf flacher schulter\n\n"
            "viel zu weit im e, etwas zu weit im s")
        # Edge faktor not touched (it is a clearing)
        self.assertEqual(plot_obs1.forest_edgef, 0)
        plot_obs2 = plot1.plotobs_set.filter(year='2013')[0]

    def test_xml_sample_double_import(self):
        # Importing the same file twice should be idempotent
        self.test_xml_sample_import()
        self.test_xml_sample_import()

    def test_xml_sample_errors(self):
        with self.assertRaises(ImportErrors) as exc:
            imp = self._import_test_file("sample_errors.xml")
        self.assertIn(
            "Value 'A' is not suitable for a DecimalField with max_digits=10 and decimal_places=6 (field iflae)",
            exc.exception.errors)
        self.assertIn("Forest edge faktor (ifaktor) should be between 0 and 1 (1.1 instead)",
            exc.exception.errors)

    def test_xml_sample_warnings(self):
        imp = self._import_test_file("sample_warnings.xml")
        self.assertIn('Blösse (iaufn=B), but ianteil = 81.456700 (should be 100)',
            imp.warnings)
        self.assertIn("Duplicate TreeObs ignored (tree Tree (Buche) on plot 78 (az:150 dist:6m) for obs 78 (1998))",
            imp.warnings)
        self.assertIn("Plotobs 78 (1998): Unable to transform value 'z' to integer from key 'ineig' (setting 0 instead)",
            imp.warnings)
        self.assertIn("Plotobs 78 (1998): Unable to retrieve the value 'W' for field 'code' for object class RegenType; setting NULL instead",
            imp.warnings)
        self.assertIn("Plotobs 78 (1998): Value 'X' is not appropriate for field 'code' of object class CrownClosure; setting NULL instead",
            imp.warnings)

    def test_form_import(self):
        file_path = os.path.join(TEST_FILES_DIR, "sample.xml")
        get_user_model().objects.create_user(username='user', password='password')
        self.client.login(username='user', password='password')
        with open(file_path, mode='rb') as f:
            form_data = {'ifile': f}
            response = self.client.post(reverse('import'), form_data, follow=True)

        self.assertContains(response, "Data for file sample.xml have been successfully imported")
        # 6 lines: 4 Ksp (same plot, 2 with same tree), 1 Verj., 1 Blösse
        self.assertEqual(FileImport.objects.count(), 1)
        self.assertEqual(FileImport.objects.first().name, "sample.xml")
        self.assertEqual(Plot.objects.count(), 4)

    def test_warning_when_municipality_mismatch(self):
        imp = self._import_test_file("municipality_mismatch.xml")
        self.assertEqual(imp.warnings, "Municipality code 'X4253' for municipality Liesberg doesn't start with L")
