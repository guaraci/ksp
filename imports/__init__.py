from os.path import dirname, abspath
from django.apps import apps


def load_tests(loader, tests, pattern):
    from django.conf import settings

    if apps.is_installed("imports"):
        return loader.discover(start_dir=dirname(abspath(__file__)), pattern=pattern)
