import codecs
from collections import defaultdict
from decimal import Decimal, InvalidOperation
import sys
import xml.etree.ElementTree as ET

from django.contrib.gis.geos import Point
from django.contrib.gis.gdal import CoordTransform, SpatialReference
from django.contrib.gis.measure import Distance
from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.backends.utils import format_number

from gemeinde.models import Gemeinde
from observation.models import (Plot, PlotObs, Tree, TreeSpecies, RegenObs, TreeObs,
    OwnerType, RegenType, Region, Sector, Gap, DevelStage, SoilCompaction, ForestForm,
    ForestMixture, CrownClosure, StandStructure, Relief, Geology, Acidity,
    # TreeObs lookups:
    SurveyType, Layer, Rank, Vita, Damage, DamageCause, CrownForm, CrownLength, Stem)

class ImportErrors(Exception):
    def __init__(self, errors=None, *args, **kwargs):
        super(ImportErrors, self).__init__(*args, **kwargs)
        self.errors = errors or []

    def __str__(self):
        return "\n".join(self.errors)


PROBE_XML_FIELDS = (
    'NR', 'IRECHTS', 'IHOCH', 'IGIS', 'ISEEH', 'INEIG', 'IEXP', 'IPSI1', 'IPSI2', 'IPSF1', 'IPSF2')
KOPF_XML_FIELDS = (
    'IFLAE', 'ITFLAE', 'ISGR', 'IBGR', 'IBGR2', 'IBGR3', 'IBGR4', 'IBGR5', 'IBGR6', 'IBGR7',
    'IBGR8', 'IBGR9', 'IPROBENR', 'IKALTER', 'IKFLAG', 'IFOA', 'IREVF', 'IDISTR', 'IABT',
    'IUABT', 'IUFL', 'IBEST', 'IDATUM', 'INA', 'IK1', 'IK2', 'IK3', 'IK4', 'IK5', 'IK6',
    'IK7', 'IK8', 'ISTAO', 'IA', 'IB', 'IC', 'ID', 'IE', 'BEFEINH', 'IK9', 'IK10', 'IFF',
    'IZEITPKT', 'IFAKTOR', 'INVDATUM', 'ABT', 'PFLSOZ', 'BESITZART', 'RELIEF', 'GEBIET',
    'GWL', 'GEOLOGIE', 'ACIDITAET', 'REGION', 'WC', 'TG', 'SG', 'AE')
KLUPPL_XML_FIELDS = (
    'IRADIUS', 'IBON', 'IANTEIL', 'IANZAHL', 'IH', 'IAZIMUT', 'IBANR', 'IALTER', 'IBHD',
    'IBHD2', 'IFLAG', 'IAUFN', 'IBA', 'IBS', 'ISOZ', 'ILEBLAUF', 'IKREIS', 'IU', 'IV',
    'IW', 'IX', 'IY', 'IZ', 'IKRON', 'IBAUMNR', 'INADELV', 'IVERGILB', 'ISCHAD1', 'ISCHAD2',
    'IINTENS1', 'IINTENS2', 'ITOTH', 'IZSTAM', 'SCHICHT', 'IVERBISS')


def reencode(text):
    # file encoding is IBM850, but decoded as iso-8859-1 (as stated in the XML header), reverse the mess
    if text:
        return text.encode('iso-8859-1').decode('ibm850')
    return ''


class Importer(object):
    trans_to_2056 = CoordTransform(SpatialReference(21781), SpatialReference(2056))

    def __init__(self):
        self.cache = defaultdict(dict)
        self.errors = []
        self.warnings = []

    @classmethod
    def check_validity(cls, file_):
        line1 = file_.readline().strip()
        if line1.startswith(codecs.BOM_UTF8):
            line1 = line1[3:]
        if not line1.startswith(b'<?xml version="1.0"'):
            raise ValidationError("The file to import does not seem to be an XML file.")

    def get_object(self, ObjClass, value, field='code', force_upper=False, zero_is_none=False):
        """
        Getting objects and filling a cache so as lookup values might be reused.
        """
        if value in ('', None) or (zero_is_none and value in (0, '0')):
            return None
        if force_upper:
            value = value.upper()
        try:
            return self.cache[ObjClass][value]
        except KeyError:
            try:
                self.cache[ObjClass][value] = ObjClass.objects.get(**{field: value})
            except ObjClass.DoesNotExist:
                self.warnings.append(
                    "Plotobs %s: Unable to retrieve the value '%s' for field '%s' for object class %s; "
                    "setting NULL instead" % (
                        self.current_plotobs, value, field, ObjClass.__name__))
                return None
            except ValueError:
                self.warnings.append(
                    "Plotobs %s: Value '%s' is not appropriate for field '%s' of object class %s; "
                    "setting NULL instead" % (
                        self.current_plotobs, value, field, ObjClass.__name__))
                return None
        return self.cache[ObjClass][value]

    def set_data(self, obj, value, field_name):
        """
        Set the value for obj.field_name with value, and do some value checking.
        """
        mfield = obj.__class__._meta.get_field(field_name)
        if mfield.get_internal_type() == 'IntegerField':
            value = self.to_int(value, field_name)
        elif mfield.get_internal_type() == 'DecimalField':
            try:
                value = format_number(Decimal(value), mfield.max_digits, mfield.decimal_places)
            except InvalidOperation:
                self.errors.append(
                    "Value '%s' is not suitable for a DecimalField with max_digits=%d and decimal_places=%d (field %s)" % (
                    value, mfield.max_digits, mfield.decimal_places, field_name))
                raise ImportErrors
        if value is None and not mfield.null:
            self.errors.append("Null value is not admitted for field %s.%s" % (obj.__class__.__name__, field_name))
            raise ImportErrors
        setattr(obj, field_name, value)

    def do_import(self, impfile):
        """impfile is a FileImport model instance."""
        def read_nodes(parent, field_names):
            item = {}
            for node in parent:
                if node.tag in field_names:
                    item[node.tag.lower()] = node.text if node.text is not None else ''
            return item

        self.impfilename = impfile.ifile.file.name
        self.current_plotobs = None
        with transaction.atomic():
            for event, elem in ET.iterparse(self.impfilename):
                if elem.tag != 'probepkt':
                    continue

                # Get probe point attributes
                pitem = read_nodes(elem, PROBE_XML_FIELDS)
                # Plot identification (+ check identical point)
                point = Point(int(pitem['irechts']), int(pitem['ihoch']), srid=21781)
                try:
                    plot, created = Plot.objects.get_or_create(nr=pitem['nr'],
                        defaults={'the_geom': point, 'igis':pitem['igis'], 'sealevel': 0})
                except Plot.MultipleObjectsReturned:
                    # Try to find the one at the good location, accounts for some rounding issues
                    plots = Plot.objects.filter(nr=pitem['nr'], the_geom__distance_lt=(point, Distance(m=10)))
                    if len(plots):
                        plot = plots[0]
                        created = False
                    else:
                        plot = Plot.objects.create(nr=pitem['nr'], the_geom=point, igis=pitem['igis'])
                        created = True
                if not created:
                    point.transform(self.trans_to_2056)
                    if not plot.the_geom.equals_exact(point, tolerance=0.1):
                        # Same plot number, different position, => not the same plot
                        plot = Plot.objects.create(nr=pitem['nr'], the_geom=point, igis=pitem['igis'], sealevel=0)
                        created = True

                for kopf in elem.findall('kopf'):
                    # Get kopf attributes
                    kitem = read_nodes(kopf, KOPF_XML_FIELDS)
                    kitem['remarks'] = "\n".join([reencode(rem_node.find('INVTEXT').text)
                                                  for rem_node in kopf.findall('inv_text')])
                    kitem.update(dict([(k, pitem[k]) for k in ('iseeh', 'iexp', 'ineig', 'ipsi1', 'ipsi2', 'ipsf1', 'ipsf2')]))
                    try:
                        plotobs, po_created = self.import_plotobs(plot, kitem)
                    except ImportErrors:
                        self.errors.append("Unable to import 'kopf' element for probepkt nr %s" % pitem['nr'])
                        kopf.clear()
                        continue

                    for kluppl in kopf.findall('kluppl'):
                        # Get kluppl attributes
                        klitem = read_nodes(kluppl, KLUPPL_XML_FIELDS)
                        if klitem['iaufn'] == 'B':  # Blösse
                            if float(klitem['ianteil']) < 100:
                                self.warnings.append("Probepkt nr %s: Blösse (iaufn=B), but ianteil = %s (should be 100)" % (
                                    pitem['nr'], klitem['ianteil']))
                            plotobs.forest_clearing = True
                            plotobs.save()
                        else:
                            try:
                                tree_obs = self.import_treeobs(plotobs, klitem, new_plotobs=po_created)
                            except ImportErrors:
                                self.errors.append("Unable to import 'kluppl' element for probepkt nr %s" % pitem['nr'])
                        kluppl.clear()
                    kopf.clear()
                    plotobs.fix_edge_factor()

                sys.stderr.write('.')
                elem.clear()
                self.current_plotobs = None

            impfile.ifile.close()
            if self.errors:
                raise ImportErrors(self.errors)
        if self.warnings:
            impfile.warnings = "\n".join(self.warnings)
            impfile.save()

    def import_plotobs(self, plot, data):
        # PlotObs dependencies: Municipality
        gem_code = data['wc']
        if len(data['wc']) != 5:
            if plot.nr == 153623:
                # Hardcoding a fix for an error in wep_schauenburg.xml
                gem_code = 'M4132'
            else:
                self.errors.append("Unknown municipality code: '%s'" % data['wc'])
                raise ImportErrors()
        if gem_code == 'L4253' and "liestal" in self.impfilename:
            # Another hardcoded fix where Liestal has been changed for Liesberg
            gem_code = 'L4410'
        if ord(gem_code[0]) > 57:  # does it start with a letter?
            gem_letter = gem_code[0]
            gem_plz = gem_code[1:]
        else:
            gem_letter = None
            gem_plz = gem_code[:4]
        try:
            # First letter seems to contain errors sometimes... use as last resort
            gem = self.get_object(Gemeinde, gem_plz, field='plz')
        except Gemeinde.MultipleObjectsReturned:
            gem = Gemeinde.objects.get(plz=gem_plz, name__startswith=gem_letter)
        if gem_letter and gem.name[0] != gem_letter:
            self.warnings.append("Municipality code '%s' for municipality %s doesn't start with %s" % (
                gem_code, gem.name, gem.name[0]))

        # PlotObs identification
        invdatum = int(data['invdatum'])
        try:
            plotobs = plot.plotobs_set.get(year=invdatum)
            created = False
        except PlotObs.DoesNotExist:
            plotobs = PlotObs(plot=plot, year=invdatum, municipality=gem)
            created = True

        self.current_plotobs = plotobs
        if not plot.sealevel:
            plot.sealevel = self.to_int(data['iseeh'], 'iseeh')
            plot.save()
        if data['iexp'] and not plot.exposition:
            plot.exposition = data['iexp']
            plot.save()
        if data['ineig'] != '' and not plot.slope:
            plot.slope = self.to_int(data['ineig'], 'ineig')
            plot.save()
        if data['besitzart'] not in (None, ''):
            if data['besitzart'] in ('B', 'C', 'D', 'F'):
                besitzart = 'A'
            else:
                besitzart = data['besitzart']
            plotobs.owner_type = self.get_object(OwnerType, besitzart)
        plotobs.region = self.get_object(Region, data['region'])
        plotobs.sector = self.get_object(Sector, data['gebiet'])
        plotobs.area = self.to_int(data['tg'], 'tg')
        plotobs.subsector = data['sg'].strip()
        plotobs.evaluation_unit = data['ae']
        plotobs.gap = self.get_object(Gap, data['ina'], force_upper=True)
        befeinh = data['befeinh'].strip()
        if len(befeinh) > 0 and befeinh != '0':
            if befeinh[0] == '2':
                self.warnings.append("Value '2' is not valid for devel_stage, not imported (PlotObs %s)" % plotobs)
            else:
                plotobs.stand_devel_stage = self.get_object(DevelStage, befeinh[0], zero_is_none=True)
        if len(befeinh) > 1:
            plotobs.stand_forest_mixture = self.get_object(ForestMixture, befeinh[1])
        if len(befeinh) > 2:
            plotobs.stand_crown_closure = self.get_object(CrownClosure, befeinh[2])
        plotobs.soil_compaction = self.get_object(SoilCompaction, data['ik1'])
        plotobs.forest_form = self.get_object(ForestForm, data['ik2'])
        plotobs.regen_type = self.get_object(RegenType, data['ik3'], force_upper=True)
        plotobs.forest_mixture = self.get_object(ForestMixture, data['ik4'])
        plotobs.crown_closure = self.get_object(CrownClosure, data['ik5'])
        plotobs.stand_structure = self.get_object(StandStructure, data['iff'])
        self.set_data(plotobs, data['ifaktor'], 'forest_edgef')
        if not 0 <= float(plotobs.forest_edgef) <= 1:
            self.errors.append("Forest edge faktor (ifaktor) should be between 0 and 1 (%s instead)" % plotobs.forest_edgef)
            raise ImportErrors()
        plotobs.gwl = data['gwl']
        plotobs.relief = self.get_object(Relief, data['relief'])
        plotobs.geology = self.get_object(Geology, data['geologie'])
        plotobs.acidity = self.get_object(Acidity, data['aciditaet'])
        plotobs.remarks = data['remarks']

        # Unknown fields
        for field_name in ('iprobenr', 'ipsi1', 'ipsi2', 'ikalter', 'ik6', 'ik7', 'ik8', 'ik9', 'ik10',
                           'abt', 'ikflag', 'irevf', 'iabt', 'iuabt', 'iufl', 'ibest',
                           'istao', 'ib', 'ic', 'ipsf1', 'ipsf2', 'iflae', 'itflae', 'izeitpkt', 'ifoa', 'idistr',
                           'ia', 'ie', 'isgr', 'ibgr', 'ibgr2', 'ibgr3', 'ibgr4', 'ibgr5',
                           'ibgr6', 'ibgr7', 'ibgr8', 'ibgr9'):
            self.set_data(plotobs, data[field_name], field_name)

        plotobs.id_2 = data['id']  # Exception because of conflict for 'id' field name

        plotobs.save()
        return plotobs, created

    def import_treeobs(self, plotobs, data, new_plotobs=True):
        try:
            data['iba'] = 'NHB' if data['iba'] == 'NULL' else data['iba']
            tree_spec = self.get_object(TreeSpecies, data['iba'], field='abbrev__iexact')
        except TreeSpecies.DoesNotExist:
            self.errors.append("TreeSpecies with abbrev '%s' does not exist." % data['iba'])
            return

        data['iaufn'] = data['iaufn'].upper()
        if plotobs.plot.nr == 343611 and data['iaufn'] in ('', None):
            # Hardcoding an error in wep_sissach.xml
            data['iaufn'] = 'K'
        if data['iaufn'] in ('K', 'N'):
            # Tree identification
            tree, created = plotobs.plot.tree_set.get_or_create(
                spec=tree_spec, nr=data['ibaumnr'], azimuth=self.to_int(data['iazimut'], 'iazimut'), distance=data['iradius'])

            # TreeObs creation
            if new_plotobs:
                treeobs = TreeObs(obs=plotobs, tree=tree)
                try:
                    treeobs.validate_unique()
                except ValidationError:
                    self.warnings.append("Duplicate TreeObs ignored (tree %s for obs %s)" % (treeobs.tree, treeobs.obs))
                    return
            else:
                # get_or_create does not like some empty fields (at this stage)
                try:
                    treeobs = TreeObs.objects.get(obs=plotobs, tree=tree)
                except TreeObs.DoesNotExist:
                    treeobs = TreeObs(obs=plotobs, tree=tree)
            treeobs.survey_type = self.get_object(SurveyType, data['iaufn'])
            treeobs.layer = self.get_object(Layer, data['ibs'], zero_is_none=True)
            treeobs.dbh = self.to_int(data['ibhd'], 'ibhd')
            treeobs.rank = self.get_object(Rank, data['isoz'])
            treeobs.vita = self.get_object(Vita, data['ileblauf'])
            treeobs.damage = self.get_object(Damage, data['ischad1'])
            treeobs.damage_cause = self.get_object(DamageCause, data['ischad2'])
            if data['ix'] not in (None, '', '0'):
                treeobs.crown_form = self.get_object(CrownForm, data['ix'])
            if data['iy'] not in (None, '', '0'):
                treeobs.crown_length = self.get_object(CrownLength, data['iy'])
            treeobs.stem = self.get_object(Stem, data['iintens1'], force_upper=True)

            # Unknown fields
            for field_name in ('iverbiss', 'iflag', 'ibanr', 'ialter', 'ibhd2', 'ikreis',
                               'inadelv', 'ivergilb', 'itoth', 'schicht', 'iv', 'iw', 'iz',
                               'ianteil', 'ih', 'ibon', 'ikron', 'iintens2', 'izstam', 'ianzahl', 'iu'):
                self.set_data(treeobs, data[field_name], field_name)
            treeobs.save()

        elif data['iaufn'] == 'Q':
            regenobs, created = RegenObs.objects.get_or_create(
                obs=plotobs, spec=tree_spec, perc=Decimal(data['ianteil']))
        else:
            self.errors.append("Unsupported survey_type value: '%s'" % data['iaufn'])
            raise ImportErrors()

    def to_int(self, value, key):
        if value in ('', None):
            return None
        try:
            return int(value)
        except ValueError:
            value = value.rstrip('0')
            if value.endswith('.'):
                value = value[:-1]
            try:
                return int(value)
            except ValueError:
                self.warnings.append(
                    "%sUnable to transform value '%s' to integer from key '%s' (setting 0 instead)" % (
                        "Plotobs %s: " % self.current_plotobs if self.current_plotobs else '', value, key))
                return 0
