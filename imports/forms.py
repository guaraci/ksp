from django import forms

from .imports import Importer
from .models import FileImport


class ImportForm(forms.ModelForm):
    class Meta:
        model = FileImport
        fields = ['ifile']

    def clean_ifile(self):
        imp_file = self.cleaned_data['ifile']
        Importer.check_validity(imp_file)
        return imp_file

    def save(self):
        self.instance.name = self.instance.ifile.name
        instance = super(ImportForm, self).save()
        try:
            Importer().do_import(instance)
        except Exception:
            instance.delete()
            raise
        return instance
