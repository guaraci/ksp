# ------------------------------------------------------ #
#    UNINE R&D WORKSHOP
#    Contributed by Ehsan Farhadi
# ------------------------------------------------------ #
from django.db import models

from ksp_bl.models import (
    Acidity, EXPO_CHOICES, Geology, Region, Relief, TreeSpecies,
)

 
class TotalIncrement(models.Model):
    """
    Table in page 156 of the NFI book
    coefficients for calculating GWL
    """
    factor_combination = models.SmallIntegerField(verbose_name="factor_combination_number")
    k = models.FloatField()
    m = models.FloatField()
    p = models.FloatField()
    c = models.FloatField()
    minhoe = models.IntegerField()
    maxhoe = models.IntegerField()

    class Meta:
        db_table = 'total_increment'


class NFICoefficient(models.Model):
    """
    Table in page 170 of the NFI book
    NFI Coefficients based on tariff number
    """
    tariff_number = models.SmallIntegerField(default=0)
    b0 = models.FloatField()
    b1 = models.FloatField()
    b2 = models.FloatField()
    b3 = models.FloatField()
    b4 = models.FloatField()
    b5 = models.FloatField()
    b6 = models.FloatField()
    b7 = models.FloatField()

    class Meta:
        db_table = 'nfi_coefficients'


class FactorCombination(models.Model):
    """
    Table in page 155 of the NFI book
    for calculating GWL
    """
    region = models.ForeignKey(Region, null=True, blank=True, on_delete=models.SET_NULL)
    acidity = models.ForeignKey(Acidity, verbose_name="Azidität", null=True, blank=True,
        on_delete=models.SET_NULL)
    relief = models.ForeignKey(Relief, null=True, blank=True, on_delete=models.SET_NULL)
    geology = models.ForeignKey(Geology, verbose_name="Geologie", null=True, blank=True,
        on_delete=models.SET_NULL)
    exposition = models.CharField(max_length=1, null=True, blank=True, choices=EXPO_CHOICES, default='')
    factor_combination = models.SmallIntegerField(null=True, blank=True) 

    class Meta:
        db_table = 'factor_combination'
  

class TariffNumber(models.Model):
    """
    Table in page 168 of the NFI book
    defining a tariff number based on tree specie and region of it.
    """

    tariff_number = models.SmallIntegerField()
    spec = models.ForeignKey(TreeSpecies, null=True, blank=True, on_delete=models.SET_NULL)
    region = models.ForeignKey(Region, null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        db_table = 'tariff_number'


class Number(models.Model):
    """
    table only with numbers
    to be used in Ddom function to pick 100 thickest trees in hectare
    """
    number = models.IntegerField(verbose_name='number')

    class Meta:
        db_table = 'numbers'
