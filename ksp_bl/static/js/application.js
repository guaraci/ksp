'use strict';
import { Map } from './modules/map.js';
import { FormSet } from './modules/forms.js';
import { MobileApp } from './modules/app.js';

/*
 * Map class
 */
class KSPMap extends Map {
    static CENTER_INITIAL = [47.47, 7.65];
    static ZOOM_INITIAL = 13;
    static ZOOM_PLOT = 20;

    setupLayers() {
        this.orthoLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
            layers: 'orthofotos_swissimage_2018_group',
            minZoom: 11,
            maxZoom: 21,
            maxNativeZoom: 18,
            useCache: true,
            crossOrigin: true
        });
        this.grundLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
            layers: 'grundkarte_sw_group',
            minZoom: 11,
            maxZoom: 21,
            maxNativeZoom: 18,
            useCache: true,
            crossOrigin: true
        });
        this.kurvLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
            layers: 'hoehenkurven_2018_raster',
            minZoom: 11,
            maxZoom: 21,
            maxNativeZoom: 18,
            format: 'image/png',
            transparent: true,
            useCache: true,
            crossOrigin: true
        });
        this.baseLayers = [this.grundLayer, this.kurvLayer];
    }
}


class KSPFormSet extends FormSet {
    initHandlers() {
        super.initHandlers();
        document.getElementById('id_slope').addEventListener('change', (ev) => {
            this.syncSlopeAndRadius(ev.target.value);
        });
        // Show warning when forest edge factor is lower than 0.6
        document.getElementById('id_forest_edgef').addEventListener('input', (ev) => {
            if (parseFloat(ev.target.value) < 0.6) {
                document.getElementById('forest_edgef_warning').style.display = 'block';
            } else {
                document.getElementById('forest_edgef_warning').style.display = 'none';
            }
        });
    }

    initForms(obs) {
        super.initForms(obs);
        // Slope value on the left synced with the slope input
        this.syncSlopeAndRadius(document.getElementById('id_slope').value);
    }

    syncSlopeAndRadius(slopeVal) {
        document.getElementById('plot-slope').innerHTML = slopeVal;
        // Adapt radius to new slope
        var radius = 9.77;
        if (slopeVal > 20) {
            let rounded = parseInt(Math.round(slopeVal/5.0)*5);
            // slopeMapping is globally defined in index.html
            radius = slopeMapping[rounded];
        }
        document.getElementById('plot-radius').innerHTML = radius + 'm';
        // Adapt circle radius on the map
        this.app.map.vLayers.plotCircle.setRadius(radius);
        this.app.currentPlot.properties.radius = radius;
    }

    showStep(step) {
        super.showStep(step);
        if (step == this.FINAL_STEP) {
            // Handle exact coords warning
            const hasExactCoords = (
                document.getElementById('id_exact_long').value.length > 0 && document.getElementById('id_exact_lat').value.length > 0
            );
            document.getElementById('coords_missing_warning').style.display = hasExactCoords ? 'none' : 'block';
        }
    }
}


class KSPApp extends MobileApp {
    constructor() {
        super();
        this.mapClass = KSPMap;
        this.formSet = new KSPFormSet(this);
    }
}

window.app = new KSPApp();

document.addEventListener("DOMContentLoaded", (event) => {
    window.app.init();
});
