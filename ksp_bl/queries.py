from django.db.models import Avg, Count, F, FloatField, Q, Sum, Value
from django.db.models.functions import Cast
from django.urls import reverse, reverse_lazy

from gemeinde.models import Gemeinde
from observation.queries import (
    GigantenQuery, Groupment, GroupmentRegion, GroupmentSpec, GroupmentSpecGroup,
    GemeindePerimGroup, HeutOwnerPerimGroup, HistOwnerPerimGroup, PerimeterSetBase,
    PerimGroup, PerimGroupBase, QueryData, ZuwachsQuery
)
from observation.models.db_views import BodenSchaden
from .db_views import (
    BWARTEN, BWNATURN, BWSTRU1M, Biotopwert, Eschentriebsterben,
    HolzProduktionBL, HolzProduktionProSpecBL, HolzProduktionProSpecGroupBL,
    TotholzBL, TotholzProSpecBL, TotholzProSpecGroupBL,
    NutzungBL, NutzungProSpecBL, NutzungProSpecGroupBL,
    EinwuchsBL, EinwuchsProSpecBL, EinwuchsProSpecGroupBL,
)
from .models import AdminRegion, GefahrPotential, Owner, PlotObs


class QueryDataBL(QueryData):
    def get_ordered_fields(self, model):
        # If present, volumen_rel_wsl should always come immediately after volumen_rel.
        fields = super().get_ordered_fields(model)
        if 'volumen_rel_wsl' in fields:
            field_list = list(fields.keys())
            field_list.remove('volumen_rel_wsl')
            field_list.insert(field_list.index('volumen_rel') + 1, 'volumen_rel_wsl')
            fields = {key: fields[key] for key in field_list}
        return fields


class BodenschadenQuery(QueryDataBL):
    model = BodenSchaden
    descr = 'Bodenschäden'
    plot_obs = 'id__'
    map_color_fields = []
    annot_map = {'code0': Sum, 'code1': Sum, 'code2': Sum}
    groupables = ['spec', 'specgroup', 'otype', 'ostatus']
    category = 'Naturschutz'

    def add_final_annots2(self):
        annots, names = super().add_final_annots2()
        annots.extend([(
            Cast(F('code0__sum'), output_field=FloatField()) /
            Cast(F('id__count'), output_field=FloatField()) *
            Value(100.0)
        ), (
            Cast(F('code1__sum'), output_field=FloatField()) /
            Cast(F('id__count'), output_field=FloatField()) *
            Value(100.0)
        ), (
            Cast(F('code2__sum'), output_field=FloatField()) /
            Cast(F('id__count'), output_field=FloatField()) *
            Value(100.0)
        )])
        # mandatory default_alias
        annots[-3].default_alias = 'code0_perc'
        annots[-2].default_alias = 'code1_perc'
        annots[-1].default_alias = 'code2_perc'
        names.extend([
            "nicht, wenig verdichtet (0-10%) (in %)",
            "teilweise verdichtet (10-30%) (in %)",
            "flächig verdichtet >30% (in %)",
        ])
        return annots, names


class EschentriebQuery(QueryDataBL):
    model = Eschentriebsterben
    descr = 'Eschentriebsterben'
    plot_obs = 'plot_obs_id__'
    plot_prefix = 'plot_obs__plot'
    annot_map = {'volumen_m3': Sum, 'volumen_m3_wsl': Sum}
    category = 'Waldgesundheit'

    def build_aggregations(self, query, aggrs):
        query, aggr_crits, field_names = super().build_aggregations(query, aggrs)
        # Force the ash_dieback grouping.
        aggr_crits.append('ash_dieback__description')
        field_names.append('*Eschentriebsterben')
        return query, aggr_crits, field_names

    def add_final_annots(self):
        annots, names = super().add_final_annots()
        annots.append(Count('id'))
        names.append('Stammzahl')
        return annots, names


VIEW_MAP = {
    'BWNATURN': QueryDataBL(
        model=BWNATURN, descr='Naturnähe des Nadelholzanteils (BWNATURN)',
        plot_obs='plot_obs__', order_by='plot_obs',
        map_color_fields=['bwnaturn', 'fichte_perc', 'nadel_perc', 'nadel_wtanne_perc'],
        annot_map={'bwnaturn': Avg},
        groupables=['bwnaturn'],
        category='Naturschutz',
    ),
    'BWARTEN': QueryDataBL(
        model=BWARTEN, descr='Gehölzartenvielfalt (BWARTEN)',
        plot_obs='plot_obs__', plot_prefix='plot_obs__plot__', order_by='plot_obs',
        map_color_fields=['bwarten', 'num_spec', 'special_spec'],
        annot_map={'bwarten': Avg, 'num_spec': Avg, 'special_spec': Avg},
        groupables=['bwarten'],
        category='Naturschutz',
    ),
    'BWSTRU': QueryDataBL(
        model=BWSTRU1M, descr='Strukturvielfalt (BWSTRU1M)',
        plot_obs='id__', plot_prefix='id__plot__',
        map_color_fields=['bwstru1m'],
        annot_map={
            'bwstru1m': Avg, 'stand_devel_stage': Avg,
            'stand_crown_closure': Avg, 'stand_structure': Avg
        },
        groupables=['bwstru1m'],
        category='Naturschutz',
    ),
    'Biotopwert': QueryDataBL(
        model=Biotopwert, descr='Biotopwert (BIOLFI1M)',
        plot_obs='id__',
        map_color_fields=['biolfi1m', 'bioklass'],
        annot_map={'biolfi1m': Avg},
        groupables=['otype', 'ostatus', 'bioklass'],
        category='Naturschutz',
    ),
    'Stammzahl': QueryDataBL(
        model=HolzProduktionBL, descr='Stammzahl, Volumen, Grundfläche',
        model_by_spec=HolzProduktionProSpecBL, model_by_spec_group=HolzProduktionProSpecGroupBL,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={
            'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'volumen_rel_wsl': Avg, 'grundflaeche_rel': Avg
        },
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
    ),
    'Totholz': QueryDataBL(
        model=TotholzBL, descr='Totholz',
        model_by_spec=TotholzProSpecBL, model_by_spec_group=TotholzProSpecGroupBL,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={
            'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'volumen_rel_wsl': Avg, 'grundflaeche_rel': Avg
        },
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Naturschutz',
    ),
    'Einwuchs': QueryDataBL(
        model=EinwuchsBL, descr='Einwuchs',
        model_by_spec=EinwuchsProSpecBL, model_by_spec_group=EinwuchsProSpecGroupBL,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={
            'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'volumen_rel_wsl': Avg, 'grundflaeche_rel': Avg
        },
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
    ),
    'Nutzung': QueryDataBL(
        model=NutzungBL, descr='Nutzung',
        model_by_spec=NutzungProSpecBL, model_by_spec_group=NutzungProSpecGroupBL,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={
            'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'volumen_rel_wsl': Avg, 'grundflaeche_rel': Avg
        },
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
    ),
    'Zuwachs': ZuwachsQuery(),
    'Bodenschäden': BodenschadenQuery(),
    'Giganten': GigantenQuery(),
    'Eschentriebsterben': EschentriebQuery(),
    # Essentially to query verbose field names for some groupable keys not on views
    'plotobs': QueryDataBL(model=PlotObs, category=''),
}


class SchutzwaldPerimGroup(PerimGroupBase):
    title = "Schutzwald"
    items = GefahrPotential.objects.filter(pk__gt=0).order_by('pk')
    short_name = 'schutzwald'
    infos_url = reverse_lazy('doc-specific', args=['schutzwald'])
    _infos = "Schutzwald/Gefahrpotential"

    def filter_query(self, pks, plot_prefix=None, **kwargs):
        filtre = Q(**{
           f'{plot_prefix}{Plot.geom_field}__within':
                AdminRegion.objects.filter(
                    schutzwald__haupt_gef_pot__pk__in=pks
                ).aggregate(geom=Union('geom'))['geom']
        })
        gef_qs = GefahrPotential.objects.filter(pk__in=pks).values('name')
        return filtre, ("Schutzwald", ", ".join([gef['name'] for gef in gef_qs]))


class PerimeterSet(PerimeterSetBase):
    @classmethod
    def get_perimeters(cls):
        return [
            GemeindePerimGroup(
                "Gemeinden",
                Gemeinde.objects.all().order_by('name'),
                _infos=("Politische Gemeinden der beiden Basel sowie einzelne "
                        "Gemeinden aus den Nachbarkantonen, in den Kontrollstichproben "
                        "durch das Amt für Wald beider Basel erhoben wurden."),
                infos_url=reverse('doc-specific', args=['gemeinden']),
                short_name="gem",
            ),
            PerimGroup(
                "Forstkreise",
                AdminRegion.objects.filter(region_type__name='Forstkreis').order_by('name'),
                _infos="Alle Forstkreise der beiden Basel können ausgewählt werden.",
                infos_url=reverse('doc-specific', args=['forstkreise']),
                short_name="fkreis",
            ),
            PerimGroup(
                "Forstreviere",
                AdminRegion.objects.filter(region_type__name='Forstrevier').order_by('name'),
                _infos="Alle Forstreviere der beiden Basel können ausgewählt werden.",
                infos_url=reverse('doc-specific', args=['forstreviere']),
                short_name="frevier",
            ),
            PerimGroup(
                "Jagdreviere",
                AdminRegion.objects.filter(region_type__name='Jagdrevier').order_by('name'),
                _infos="Alle Jagdreviere der beiden Basel können ausgewählt werden.",
                infos_url=reverse('doc-specific', args=['jagdreviere']),
                short_name="jrevier",
            ),
            PerimGroup(
                "WEP",
                AdminRegion.objects.filter(region_type__name='WEP').order_by('name'),
                _infos="Alle WEPs der beiden Basel können ausgewählt werden.",
                infos_url=reverse('doc-specific', args=['wep']),
                short_name="wep",
            ),
            HistOwnerPerimGroup(
                _infos=("Wichtigste Waldeigentümer der beiden Basel können ausgewählt werden. "
                        "(Eigentümer zum Zeitpunkt der Inventarisierung)"),
            ),
            HeutOwnerPerimGroup(
                _infos=("Wichtigste Waldeigentümer der beiden Basel können ausgewählt werden. "
                        "(Aktueller Stand der Eigentumsverhältnisse)"),
            ),
            SchutzwaldPerimGroup(),
            PerimGroup(
                "Freiperimeter",
                AdminRegion.objects.filter(region_type__name='Freiperimeter').order_by('name'),
                _infos="Website-Administrator definierte Regionen.",
                infos_url=reverse('doc-specific', args=['freiperimeter']),
                short_name="freiperimeter",
            ),
        ]


def get_groupments():
    return {
        'perims': [
            Groupment('municipality', 'Gemeinde'),
            GroupmentRegion('forstkreis', 'Forstkreis'),
            GroupmentRegion('forstrevier', 'Forstrevier'),
            GroupmentRegion('jagdrevier', 'Jagdrevier'),
            GroupmentRegion('wep', 'WEP'),
            Groupment(
                'eigentumer-name', 'Hist. Eigentümer', accessor='owner__name',
                docs_title="Gruppierung nach wichtigsten Waldeigentümern der beiden Basel. (Eigentümer zum Zeitpunkt der Inventarisierung) Klicken Sie für weitere Informationen.",
                docs_url='/docs/eigentuemer/',
            ),
            GroupmentRegion(
                'eigentumer-geom', 'Heut. Eigentümer',
                docs_title="Gruppierung nach wichtigsten Waldeigentümern der beiden Basel. (Aktueller Stand der Eigentumsverhältnisse). Klicken Sie für weitere Informationen.",
                docs_url='/docs/eigentuemer/',
            ),
            GroupmentRegion(
                'schutzwald', 'Gefahrenprozess (Schutzwald)',
                accessor='plotinregionview__adminregion__schutzwald__haupt_gef_pot__name', plot_prefix=True,
                docs_title="Es werden folgende Gefahrenprozesse unterschieden: Lawine (inkl. Nassschneerutsche), Sturz (Steinschlag, inkl. Eisschlag), Hangmure / Rutschung, Gerinneprozesse"),
            GroupmentRegion('freiperimeter', 'Freiperimeter'),
        ],
        'periods': [
            Groupment('year', 'Jahr', checked=True),
            Groupment(
                'inv_period', 'Aufnahmeperiode', accessor='inv_team__period',
                docs_title="0:Pratteln (1988), Ormalingen (1991), 1: 1987 bis 2000, 2: 2002 bis 2014",
                docs_url=reverse('doc-specific', args=['aufnahmeperiode'])),
            Groupment(
                'latestinv', 'Nur letztes Inventar',
                docs_title="nur Punkte vom letzten Inventar"),
        ],
        'bestand': [
            Groupment(
                'stand_devel_stage', "Entwicklungsstufe",
                docs_title="Bestimmte Etappe der Entwicklung eines Waldbestandes charakterisiert durch dessen Grösse.",
                docs_url=reverse('doc-specific', args=['entwicklungsstufe'])),
            Groupment(
                'stand_forest_mixture', "Mischungsgrad",
                docs_title="In einem Bestand oder in einer Bestandesschicht vorhandene Baumarten.",
                docs_url=reverse('doc-specific', args=['mischungsgrad'])),
            Groupment(
                'stand_crown_closure', "Schlussgrad",
                docs_title="Ist ein Mass für die Intensität der Kronenberührungen innerhalb eines Bestandes.",
                docs_url=reverse('doc-specific', args=['schlussgrad'])),
        ],
        'phytosoc': [
            Groupment(
                'phyto_code', "Phytosoziologie", accessor='phytosoc__code', plot_prefix=True,
                docs_title="Waldstandorte beider Basel nach Ellenberg und Klötzli"),
            Groupment(
                'inc_class', "Ertragsklasse", accessor='phytosoc__inc_class', plot_prefix=True,
                docs_title="Ertragsfähigkeit für die wichtigsten Waldstandorte nach Ellenberg und Klötzli"),
            Groupment(
                'ecol_grp', "Ökologische Gruppe", accessor='phytosoc__ecol_grp', plot_prefix=True,
                docs_title="Ökologische Gruppen der wichtigsten Waldstandorte nach Ellenberg und Klötzli"),
        ],
        'various': [
            GroupmentSpec('spec', "Baumart"),
            GroupmentSpecGroup('group', "Baumartgruppe"),
            Groupment(
                'otype', "Eigentümertyp", accessor='owner__otype', fkey_target='__name',
                docs_title="Die Waldeigentümer werden in verschiedene Kategorien (Tabelle owner_type) gruppiert. Damit kann die Auswertung pro Eigentümertyp umgesetzt werden (Waldentwicklungsplanung). Die Erläuterungen zum Eigentümer sind unter Eigentümer aufgeführt."
            ),
            Groupment(
                'ostatus', "Eigentümer Status", accessor='owner__otype__status',
                docs_title="Die Waldeigentümer werden in die beiden Kategorien i) öffentlich (Einwohnergemeinden, "
                           "Bürgergemeinden, Burgerkoorporationen, Bund, Kanton) und ii) privat (Privatwaldbesitzer, Stiftungen)."),
            Groupment('bwnaturn', "Naturnähe des Nadelholzanteils (BWNATURN)"),
            Groupment('bwarten', "Gehölzartenvielfalt (BWARTEN)"),
            Groupment('bwstru1m', "Strukturvielfalt (BWSTRU1M)"),
            Groupment('bioklass', "Biotopwert Klasse"),
        ],
    }
