import json
from datetime import date
from pathlib import Path

from django.contrib.auth import get_user_model
from django.contrib.gis.geos import MultiPolygon, Point
from django.http import QueryDict
from django.test import TestCase
from django.urls import reverse

from gemeinde.models import Gemeinde
from observation.common_models import (
    Inventory, Owner, Plot, PlotObs, SurveyType, Tree, TreeObs, Vita,
)
from ksp_bl.queries import EschentriebQuery, VIEW_MAP
from observation.views import view_tuple


class ObservationTests(TestCase):
    databases = ['default', 'afw']
    fixtures = ['gemeinde_tests.json']

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = get_user_model().objects.create_user(username='user', password='password')
        SurveyType.objects.bulk_create([
            SurveyType(code='K', description='Kontrollstichprobenaufnahme'),
            SurveyType(code='Q', description='Verjüngung'),
            SurveyType(code='B', description='Blösse'),
            SurveyType(code='N', description='nicht Wald'),
        ])
        Vita.objects.bulk_create([
            Vita(code='c', description='Probebaum wurde genutzt'),
            Vita(code='e', description='Probebaum ist eingewachsen'),
            Vita(code='l', description='Probebaum lebt'),
            Vita(code='m', description='Probebaum ist ein Dürrständer'),
            Vita(code='x', description='Probebaum ist nicht mehr auffindba'),
            Vita(code='y', description='Probebaum liegt ausserhalb des Probekreises'),
            Vita(code='z', description='liegender, toter Baum'),
        ])

    def setUp(self):
        self.client.force_login(self.user)

    def test_homepage(self):
        response = self.client.get(reverse('home'))
        self.assertContains(response,
            '<tr><td valign="top"><a href="/gemeinde/15/">Böckten</a></td>'
            '<td>-</td><td>-</td><td>-</td></tr>',
            html=True
        )

    def test_datagrid_metadata(self):
        response = self.client.get(reverse('data_grid') + "?gem=15&bio=BWARTEN&metadata=1&format=json")
        result = json.loads(response.content.decode('utf-8'))
        self.assertEqual(result, {
            'map_color_fields': [
                ['bwarten', 'Gehölzartenvielfalt (BWARTEN)'],
                ['num_spec', 'Anzahl Baumarten'],
                ['special_spec', 'Special Species'],
            ],
            'gemeinden': ['Böckten'],
        })

    def test_group_by_inv_period(self):
        response = self.client.get(reverse('data_grid') + "?gem=1&bio=BWSTRU&yaggr=inv_period&stddev=1")
        self.assertNotContains(response, 'Fehler')
        self.assertContains(response, '<th class="aggr">*Aufnahmeperiode</th>', html=True)

    def test_group_by_wep(self):
        query = VIEW_MAP['BWSTRU']
        data = query.build_query(QueryDict('gem=1'), aggrs=['year', 'wep'])
        self.assertQuerysetEqual(data['query'], [])

    def test_eschentriebsterben(self):
        query = EschentriebQuery()
        data = query.build_query(QueryDict('gem=1'), aggrs=[])
        self.assertQuerysetEqual(data['query'], [])
        data = query.build_query(QueryDict('gem=1'), aggrs=['year'])
        self.assertQuerysetEqual(data['query'], [])

    def test_bodenschaeden(self):
        query = VIEW_MAP['Bodenschäden']
        data = query.build_query(QueryDict('gem=1'), aggrs=[])
        self.assertQuerysetEqual(data['query'], [])

    def test_plotobs_detail(self):
        municip = Gemeinde.objects.first()
        owner = Owner.objects.create(nr=1, name='TestOwner', geom=MultiPolygon(municip.the_geom))
        plot = Plot.objects.create(nr=1, sealevel=740, the_geom=municip.the_geom.point_on_surface)
        tree1 = Tree.objects.create(plot=plot, nr=1, azimuth=29, distance=92, )
        inv = Inventory.objects.create(
            name='Inventory', municipality=Gemeinde.objects.first(),
            inv_from='2019-04-01', inv_to='2019-10-10', period=3
        )
        plotobs = PlotObs.objects.create(
            plot=plot, municipality=Gemeinde.objects.first(), inv_team=inv,
            year=2013, forest_edgef=0.75, gwl=3.4
        )
        self.assertEqual(plotobs.owner, owner)  # Automatic
        treeobs = TreeObs.objects.create(
            tree=tree1, obs=plotobs, dbh=25, survey_type=SurveyType.objects.first(),
            vita=Vita.objects.get(code='l'),
        )

        response = self.client.get(reverse('plotobs_detail', args=[plotobs.pk]))
        self.assertContains(response, '"distance": "92.00"')

        response = self.client.get(reverse('plotobs_detail_json', args=[plotobs.pk]))
        resp = response.json()
        self.assertEqual(resp['features'][0]['properties']['type'], 'center')
        self.assertEqual(resp['features'][0]['properties']['year'], '2013')

    def test_point_calculation(self):
        plot = Plot.objects.create(nr=1, sealevel=740, the_geom=Point(2609200, 1257800, srid=2056))
        tree1 = Tree.objects.create(plot=plot, nr=1, azimuth=29, distance=92)
        self.assertAlmostEqual(tree1.point.distance(plot.the_geom), 9.2)
        tree2 = Tree.objects.create(plot=plot, nr=2, azimuth=229, distance=92)
        self.assertAlmostEqual(tree2.point.distance(plot.the_geom), 9.2)
        self.assertAlmostEqual(tree1.point.distance(tree2.point), 9.2 * 2)

    def test_view_definition(self):
        oid = view_tuple('bv_web_homepage_holzproduktion').oid
        response = self.client.get(reverse('view_def', args=[oid]))
        self.assertContains(response, 'SELECT row_number()')
        # Non-existent view
        response = self.client.get(reverse('view_def', args=['999999999']), follow=True)
        self.assertEqual(response.status_code, 404)

    def test_table_definition(self):
        response = self.client.get(reverse('table_def', args=['tree']))
        self.assertContains(response, 'CREATE TABLE public.tree')
        # Lookup tables are redirected to the model view which includes table data
        response = self.client.get(reverse('table_def', args=['lt_vita']), follow=True)
        self.assertContains(response, 'CREATE TABLE public.lt_vita')
        self.assertContains(response, 'Probebaum wurde genutzt')

    def test_getting_elevation(self):
        plot = Plot.objects.create(nr=1, the_geom=Point(2609200, 1257800, srid=2056))
        plot.set_elevation_from_swisstopo()
        self.assertEqual(plot.sealevel, 405)

    def test_import_new_plots(self):
        this_year = date.today().year
        inv = Inventory.objects.create(
            name='Inventory', municipality=Gemeinde.objects.get(name='Bubendorf'),
            inv_from=f'{this_year}-04-01', inv_to=f'{this_year}-10-10', period=3
        )
        shape_path = Path(__file__).parent / 'neue_plots.zip'
        with shape_path.open(mode='rb') as fh:
            response = self.client.post(reverse('inventory-new-plots-shape', args=[inv.pk]), data={
                'shapefile': fh,
            })
        resp = response.json()
        self.assertEqual(resp['new'], 53)  # Tested against simplified Bubendorf geometry in gemeinde_tests.json
        self.assertEqual(resp['existing'], 0)
        self.assertEqual(resp['outside'], 85)

    def test_divide_plot_density(self):
        this_year = date.today().year
        inv = Inventory.objects.create(
            name='Inventory', municipality=Gemeinde.objects.get(name='Böckten'),
            inv_from=f'{this_year}-04-01', inv_to=f'{this_year}-10-10', period=3
        )
        # Create a 4x4 plot grid
        x_base, y_base = 2629800, 1257500
        plots = []
        for x_incr in range(0, 400, 100):
            for y_incr in range(0, 400, 100):
                plots.append(Plot(nr=1, the_geom=Point(x_base + x_incr, y_base + y_incr, srid=2056)))
        Plot.objects.bulk_create(plots)
        response = self.client.post(reverse('inventory-divide-density', args=[inv.pk]))
        inv.refresh_from_db()
        self.assertEqual(len(inv.excluded_plots), 8)
