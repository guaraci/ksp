from observation.views import GemeindenView
from .db_views import HomepageView


class BLGemeindenView(GemeindenView):
    db_view = HomepageView

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data_columns = GemeindenView.data_columns[:2] + [
            {'title': 'Volume (m<sup>3</sup>/ha) [WSL]', 'fname': 'volumen_ha_wsl',
             'doc_title': 'Berechnung mit dem prov. WSL Tarif 2020. Nur orientierend.'}
        ] + GemeindenView.data_columns[-1:]
        self.data_columns[1]['title'] = 'Volume (m<sup>3</sup>/ha) [ET]'
