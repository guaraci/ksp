from django import forms

from observation.forms import PickDateWidget
from .models import Inventory


class InventoryForm(forms.ModelForm):
    class Meta:
        model = Inventory
        fields = ['team', 'inv_from', 'inv_to', 'default_density', 'typ', 'remarks']
        widgets = {
            'inv_from': PickDateWidget,
            'inv_to': PickDateWidget,
        }


