from django.db import models

from gemeinde.models import Gemeinde
from observation.models.db_views import (
    BaseHolzProduktion, ModelProSpec, ModelProSpecGroup,
)
from .models import (
    CrownClosure, DevelStage, FoliageDensity, OwnerType, Plot, PlotObs, StandStructure
)


class HomepageView(models.Model):
    gemeinde = models.CharField(max_length=50, db_column="gemeindename")
    jahr = models.SmallIntegerField("Aufnahmejahr", db_column="year")
    probepunkte = models.IntegerField("Anzahl Probepunkte", db_column="Anzahl Probepunkte")
    waldflaeche = models.DecimalField("theoretische Waldfläche pro ha", max_digits=5, decimal_places=1,
        db_column="theoretische Waldfläche ha")
    probebaum_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    stammzahl_ha = models.IntegerField("Stammzahl pro ha", db_column="Stammzahl pro ha")
    stammzahl_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler")
    volumen_ha = models.DecimalField("Volumen [m3 pro ha]", max_digits=5, decimal_places=1,
        db_column="Volumen pro ha")
    volumen_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler2")
    volumen_ha_wsl = models.DecimalField("Volumen [m3 pro ha] (WSL)", max_digits=5, decimal_places=1,
        db_column="Volumen pro ha (WSL)")
    grundflaeche_ha = models.DecimalField("Grundflaeche [m2 pro ha]", max_digits=5, decimal_places=1,
        db_column="Grundflaeche pro ha")
    grundflaeche_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler3")
    inv_period = models.SmallIntegerField()
    waldflaeche_bestand = models.IntegerField("Waldfläche gemäss Bestandeskarte")

    class Meta:
        db_table = 'bv_web_homepage_holzproduktion'
        managed = False


class VolumenWSLMixin(models.Model):
    volumen_rel_wsl = models.FloatField("Volume (m3/ha) [WSL]: WSL Tarif 2020", db_column="Volumen pro ha (WSL)")

    class Meta:
        abstract=True


class HolzProduktionBL(VolumenWSLMixin, BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs"
        managed = False


class HolzProduktionProSpecBL(VolumenWSLMixin, ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec"
        managed = False


class HolzProduktionProSpecGroupBL(VolumenWSLMixin, ModelProSpecGroup):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup"
        managed = False


class TotholzBL(VolumenWSLMixin, BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_totholz"
        managed = False


class TotholzProSpecBL(VolumenWSLMixin, ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec_totholz"
        managed = False


class TotholzProSpecGroupBL(VolumenWSLMixin, ModelProSpecGroup):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup_totholz"
        managed = False


class EinwuchsBL(VolumenWSLMixin, BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_einwuchs"
        managed = False


class EinwuchsProSpecBL(VolumenWSLMixin, ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs"
        managed = False


class EinwuchsProSpecGroupBL(VolumenWSLMixin, ModelProSpecGroup):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup_einwuchs"
        managed = False


class NutzungBL(VolumenWSLMixin, BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_nutzung"
        managed = False


class NutzungProSpecBL(VolumenWSLMixin, ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec_nutzung"
        managed = False


class NutzungProSpecGroupBL(VolumenWSLMixin, ModelProSpecGroup):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup_nutzung"
        managed = False


class BWARTEN(models.Model):
    plot_obs = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", on_delete=models.DO_NOTHING)
    bwarten = models.IntegerField(db_column='BWARTEN', verbose_name="Gehölzartenvielfalt (BWARTEN)")
    num_spec = models.IntegerField(db_column='Anzahl Baumarten', verbose_name="Anzahl Baumarten")
    special_spec = models.IntegerField(db_column='Special Species', verbose_name="Special Species")

    class Meta:
        db_table = 'bv_bwarten_anz_baumarten_special_species'
        managed = False


class BWNATURN(models.Model):
    plot_obs = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    code = models.CharField(max_length=6)
    bwnaturn = models.IntegerField(db_column="BWNATURN", verbose_name="Naturnähe des Nadelholzanteils (BWNATURN)")
    fichte_perc = models.FloatField(db_column="%% Basalflächenanteil der Fichte",
        verbose_name="% Basalflächenanteil der Fichte")
    nadel_perc = models.FloatField(db_column="%% Basalflächenanteil Nadelholz",
        verbose_name="% Basalflächenanteil Nadelholz")
    nadel_wtanne_perc = models.FloatField(db_column="%% Basalflächenanteil Nadelholz ohne Tanne",
        verbose_name="% Basalflächenanteil Nadelholz ohne Tanne")

    class Meta:
        db_table = 'bv_bwnaturn_ndh_fi_ndh_ohne_ta'
        managed = False


class BWSTRU1M(models.Model):
    id = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", db_column='id',
        on_delete=models.DO_NOTHING)
    stand_devel_stage = models.ForeignKey(DevelStage, verbose_name="Entwicklungsstufe",
        on_delete=models.DO_NOTHING)
    stand_crown_closure = models.ForeignKey(CrownClosure, verbose_name="Schlussgrad des Bestandes",
        on_delete=models.DO_NOTHING)
    stand_structure = models.ForeignKey(StandStructure, verbose_name="Schichtung des Bestandes",
        on_delete=models.DO_NOTHING)
    bwstru1m = models.IntegerField(db_column="BWSTRU1M", verbose_name="Strukturvielfalt (BWSTRU1M)")

    class Meta:
        db_table = 'bv_bwstru1m'
        managed = False


class Biotopwert(models.Model):
    id = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", db_column='id',
        on_delete=models.DO_NOTHING)
    municipality = models.ForeignKey(Gemeinde, verbose_name="Gemeinde", on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")
    owner_type = models.ForeignKey(OwnerType, verbose_name="Besitzertyp", null=True, blank=True,
        on_delete=models.DO_NOTHING)
    biolfi1m = models.FloatField(verbose_name="Biotopwert (BIOLFI1M)", db_column="BIOLFI1M")
    bioklass = models.SmallIntegerField(verbose_name="Biotopwert Klasse", db_column="Biotopwert Klassen")

    class Meta:
        db_table = "bv_biotopwert_klassen_pro_plotobs_year_ownertype"
        managed = False


class Eschentriebsterben(models.Model):
    #id = TreeObs.id
    plot_obs = models.ForeignKey(PlotObs, on_delete=models.DO_NOTHING, db_column='obs_id')
    volumen_m3 = models.DecimalField("Volumen [m3]", max_digits=10, decimal_places=4)
    volumen_m3_wsl = models.DecimalField("Volumen [m3] (WSL)", max_digits=10, decimal_places=4)
    ash_dieback = models.ForeignKey(FoliageDensity, verbose_name="Eschentriebsterben",
        on_delete=models.DO_NOTHING)

    class Meta:
        db_table = "bv_eschentriebsterben"
        managed = False
