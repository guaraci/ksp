from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_bl', '0004_inventory_typ'),
    ]

    operations = [
        migrations.CreateModel(
            name='Biotopwert',
            fields=[
                ('id', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='ksp_bl.plotobs', verbose_name='OBS ID')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('biolfi1m', models.FloatField(db_column='BIOLFI1M', verbose_name='Biotopwert (BIOLFI1M)')),
                ('bioklass', models.SmallIntegerField(db_column='Biotopwert Klassen', verbose_name='Biotopwert Klasse')),
            ],
            options={
                'db_table': 'bv_biotopwert_klassen_pro_plotobs_year_ownertype',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BWARTEN',
            fields=[
                ('plot_obs', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='ksp_bl.plotobs', verbose_name='OBS ID')),
                ('bwarten', models.IntegerField(db_column='BWARTEN', verbose_name='Gehölzartenvielfalt (BWARTEN)')),
                ('num_spec', models.IntegerField(db_column='Anzahl Baumarten', verbose_name='Anzahl Baumarten')),
                ('special_spec', models.IntegerField(db_column='Special Species', verbose_name='Special Species')),
            ],
            options={
                'db_table': 'bv_bwarten_anz_baumarten_special_species',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BWNATURN',
            fields=[
                ('plot_obs', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='ksp_bl.plotobs', verbose_name='OBS ID')),
                ('code', models.CharField(max_length=6)),
                ('bwnaturn', models.IntegerField(db_column='BWNATURN', verbose_name='Naturnähe des Nadelholzanteils (BWNATURN)')),
                ('fichte_perc', models.FloatField(db_column='%% Basalflächenanteil der Fichte', verbose_name='% Basalflächenanteil der Fichte')),
                ('nadel_perc', models.FloatField(db_column='%% Basalflächenanteil Nadelholz', verbose_name='% Basalflächenanteil Nadelholz')),
                ('nadel_wtanne_perc', models.FloatField(db_column='%% Basalflächenanteil Nadelholz ohne Tanne', verbose_name='% Basalflächenanteil Nadelholz ohne Tanne')),
            ],
            options={
                'db_table': 'bv_bwnaturn_ndh_fi_ndh_ohne_ta',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BWSTRU1M',
            fields=[
                ('id', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='ksp_bl.plotobs', verbose_name='OBS ID')),
                ('bwstru1m', models.IntegerField(db_column='BWSTRU1M', verbose_name='Strukturvielfalt (BWSTRU1M)')),
            ],
            options={
                'db_table': 'bv_bwstru1m',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='EinwuchsBL',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, related_name='+', serialize=False, to='ksp_bl.plotobs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_einwuchs',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='EinwuchsProSpecBL',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='EinwuchsProSpecGroupBL',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_specgroup_einwuchs',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Eschentriebsterben',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('volumen_m3', models.DecimalField(decimal_places=4, max_digits=10, verbose_name='Volumen [m3]')),
                ('volumen_m3_wsl', models.DecimalField(decimal_places=4, max_digits=10, verbose_name='Volumen [m3] (WSL)')),
            ],
            options={
                'db_table': 'bv_eschentriebsterben',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HolzProduktionBL',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, related_name='+', serialize=False, to='ksp_bl.plotobs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HolzProduktionProSpecBL',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HolzProduktionProSpecGroupBL',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_specgroup',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HomepageView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gemeinde', models.CharField(db_column='gemeindename', max_length=50)),
                ('jahr', models.SmallIntegerField(db_column='year', verbose_name='Aufnahmejahr')),
                ('probepunkte', models.IntegerField(db_column='Anzahl Probepunkte', verbose_name='Anzahl Probepunkte')),
                ('waldflaeche', models.DecimalField(db_column='theoretische Waldfläche ha', decimal_places=1, max_digits=5, verbose_name='theoretische Waldfläche pro ha')),
                ('probebaum_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('stammzahl_ha', models.IntegerField(db_column='Stammzahl pro ha', verbose_name='Stammzahl pro ha')),
                ('stammzahl_stdf', models.FloatField(db_column='%% Standardfehler', verbose_name='Sf [%]')),
                ('volumen_ha', models.DecimalField(db_column='Volumen pro ha', decimal_places=1, max_digits=5, verbose_name='Volumen [m3 pro ha]')),
                ('volumen_stdf', models.FloatField(db_column='%% Standardfehler2', verbose_name='Sf [%]')),
                ('volumen_ha_wsl', models.DecimalField(db_column='Volumen pro ha (WSL)', decimal_places=1, max_digits=5, verbose_name='Volumen [m3 pro ha] (WSL)')),
                ('grundflaeche_ha', models.DecimalField(db_column='Grundflaeche pro ha', decimal_places=1, max_digits=5, verbose_name='Grundflaeche [m2 pro ha]')),
                ('grundflaeche_stdf', models.FloatField(db_column='%% Standardfehler3', verbose_name='Sf [%]')),
                ('inv_period', models.SmallIntegerField()),
                ('waldflaeche_bestand', models.IntegerField(verbose_name='Waldfläche gemäss Bestandeskarte')),
            ],
            options={
                'db_table': 'bv_web_homepage_holzproduktion',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='NutzungBL',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, related_name='+', serialize=False, to='ksp_bl.plotobs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_nutzung',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='NutzungProSpecBL',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec_nutzung',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='NutzungProSpecGroupBL',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_specgroup_nutzung',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TotholzBL',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, related_name='+', serialize=False, to='ksp_bl.plotobs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_totholz',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TotholzProSpecBL',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec_totholz',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TotholzProSpecGroupBL',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volume (m3/ha) [ET]:\n Einheitstarif BL/BS')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
                ('volumen_rel_wsl', models.FloatField(db_column='Volumen pro ha (WSL)', verbose_name='Volume (m3/ha) [WSL]: WSL Tarif 2020')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_specgroup_totholz',
                'managed': False,
            },
        ),
    ]
