import django.contrib.auth.models
import django.contrib.gis.db.models.fields
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('gemeinde', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'db_table': 'auth_user',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Acidity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Acidität',
                'verbose_name_plural': 'Aciditäten',
                'db_table': 'lt_acidity',
            },
        ),
        migrations.CreateModel(
            name='AdminRegion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=10, unique=True)),
                ('name', models.CharField(max_length=100)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'adminregion',
            },
        ),
        migrations.CreateModel(
            name='CrownClosure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Schlussgrad',
                'verbose_name_plural': 'Schlussgrade',
                'db_table': 'lt_crown_closure',
            },
        ),
        migrations.CreateModel(
            name='CrownClosure2015',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Schlussgrad vor 2015',
                'verbose_name_plural': 'Schlussgrade vor 2015',
                'db_table': 'lt_crown_closure_2015',
            },
        ),
        migrations.CreateModel(
            name='CrownForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'verbose_name': 'Kronenform',
                'verbose_name_plural': 'Kronenformen',
                'db_table': 'lt_crown_form',
            },
        ),
        migrations.CreateModel(
            name='CrownLength',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'verbose_name': 'Kronenlänge',
                'verbose_name_plural': 'Kronenlängen',
                'db_table': 'lt_crown_len',
            },
        ),
        migrations.CreateModel(
            name='Damage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Schadenart',
                'verbose_name_plural': 'Schadenarten',
                'db_table': 'lt_damage',
            },
        ),
        migrations.CreateModel(
            name='DamageCause',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Schadensursache',
                'verbose_name_plural': 'Schadensursachen',
                'db_table': 'lt_damage_cause',
            },
        ),
        migrations.CreateModel(
            name='DevelStage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=2, unique=True)),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Entwicklungsstufe',
                'verbose_name_plural': 'Entwicklungsstufen',
                'db_table': 'lt_devel_stage',
                'ordering': ('code',),
            },
        ),
        migrations.CreateModel(
            name='DevelStage2015',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Entwicklungsstufe vor 2015',
                'verbose_name_plural': 'Entwicklungsstufen vor 2015',
                'db_table': 'lt_devel_stage_2015',
            },
        ),
        migrations.CreateModel(
            name='FoliageDensity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_foliagedensity',
            },
        ),
        migrations.CreateModel(
            name='ForestForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Waldform',
                'verbose_name_plural': 'Waldformen',
                'db_table': 'lt_forest_form',
            },
        ),
        migrations.CreateModel(
            name='ForestMixture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
            ],
            options={
                'verbose_name': 'Mischungsgrad',
                'verbose_name_plural': 'Mischungsgrade',
                'db_table': 'lt_forest_mixture',
            },
        ),
        migrations.CreateModel(
            name='Gap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_gap',
            },
        ),
        migrations.CreateModel(
            name='GefahrPotential',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'gefahrpotential',
            },
        ),
        migrations.CreateModel(
            name='Geology',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Geologie',
                'verbose_name_plural': 'Geologie',
                'db_table': 'lt_geology',
            },
        ),
        migrations.CreateModel(
            name='Inventory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Inventar')),
                ('team', models.CharField(max_length=100, verbose_name='Aufnahmeteam')),
                ('period', models.SmallIntegerField(choices=[(0, 'Alte Inventare (Ormalingen, Pratteln)'), (1, 'Erste Phase (1987-1999)'), (2, 'Zweite Phase (2000-2014)'), (3, 'Dritte Phase (2017-)')], verbose_name='Aufnahmeperiode')),
                ('ordering', models.SmallIntegerField(null=True, verbose_name='Aufnahmereihenfolge')),
                ('inv_from', models.DateField(verbose_name='Von')),
                ('inv_to', models.DateField(verbose_name='Bis')),
                ('default_density', models.IntegerField(blank=True, choices=[(10000, '1/ha (10000m2)'), (20000, '0,5/ha (20000m2)')], null=True, verbose_name='StandardDichte')),
                ('excluded_plots', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), blank=True, null=True, size=None, verbose_name='Ausgeschlossene pünkte')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
                ('municipality', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='gemeinde.gemeinde', verbose_name='Gemeinde')),
            ],
            options={
                'verbose_name': 'Inventar',
                'verbose_name_plural': 'Inventare',
                'db_table': 'inventory',
            },
        ),
        migrations.CreateModel(
            name='Layer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'db_table': 'lt_layer',
            },
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(blank=True, max_length=10)),
                ('name', models.CharField(max_length=100, unique=True)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056)),
                ('bp_pflichtig', models.BooleanField(default=False, verbose_name='betriebsplanpflichtig')),
                ('updated', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'owner',
            },
        ),
        migrations.CreateModel(
            name='OwnerType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_owner_type',
            },
        ),
        migrations.CreateModel(
            name='OwnerType2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
                ('status', models.CharField(choices=[('o', 'Öffentlich'), ('p', 'Privat')], max_length=1)),
            ],
            options={
                'db_table': 'owner_type',
            },
        ),
        migrations.CreateModel(
            name='Phytosoc',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=6)),
                ('description', models.CharField(max_length=200, verbose_name='Beschreibung')),
                ('inc_class', models.SmallIntegerField(verbose_name='Ertragsklasse')),
                ('ecol_grp', models.CharField(max_length=1, verbose_name='Ökologische Gruppe')),
                ('bwnatur', models.SmallIntegerField(blank=True, db_column='BWNATURN_WSL', null=True)),
                ('tarif_klass20', models.CharField(blank=True, max_length=10)),
            ],
            options={
                'verbose_name': 'Phytosoziologie',
                'verbose_name_plural': 'Phytosoziologie',
                'db_table': 'phytosoc',
            },
        ),
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=15)),
                ('the_geom', django.contrib.gis.db.models.fields.PointField(srid=2056)),
                ('point_exact', django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=2056)),
                ('slope', models.SmallIntegerField(blank=True, null=True, verbose_name='Neigung')),
                ('exposition', models.CharField(blank=True, choices=[('_', 'flach'), ('N', 'Norden'), ('S', 'Süden')], default='', max_length=1)),
                ('sealevel', models.SmallIntegerField(blank=True, null=True, verbose_name='Höhe ü. Meer')),
                ('checked', models.BooleanField(default=False)),
                ('phytosoc', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.phytosoc')),
            ],
            options={
                'db_table': 'plot',
            },
        ),
        migrations.CreateModel(
            name='PlotObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.SmallIntegerField(db_index=True, verbose_name='Aufnahmejahr')),
                ('density', models.IntegerField(null=True, verbose_name='Dichte (m2)')),
                ('area', models.SmallIntegerField(blank=True, null=True)),
                ('subsector', models.CharField(blank=True, max_length=2)),
                ('evaluation_unit', models.CharField(blank=True, max_length=4, verbose_name='Auswertungseinheit')),
                ('forest_clearing', models.BooleanField(default=False)),
                ('stand', models.SmallIntegerField(blank=True, null=True)),
                ('forest_edgef', models.DecimalField(decimal_places=1, max_digits=2, verbose_name='Waldrandfaktor')),
                ('gwl', models.FloatField(verbose_name='Gesamtwuchsleistung (gwl)')),
                ('remarks', models.TextField(blank=True, verbose_name='Bermerkungen')),
                ('iprobenr', models.SmallIntegerField(blank=True, null=True, verbose_name='alte Probennummer')),
                ('acidity', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.acidity', verbose_name='Azidität')),
                ('crown_closure', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='ksp_bl.crownclosure')),
                ('forest_form', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.forestform', verbose_name='Waldform')),
                ('forest_mixture', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='ksp_bl.forestmixture')),
                ('gap', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.gap', verbose_name='Blösse')),
                ('geology', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.geology', verbose_name='Geologie')),
                ('inv_team', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='ksp_bl.inventory')),
                ('municipality', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='gemeinde.gemeinde', verbose_name='Gemeinde')),
                ('owner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_bl.owner', verbose_name='Eigentümer')),
                ('owner_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.ownertype', verbose_name='Besitzertyp')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_bl.plot', verbose_name='Aufnahmepunkt (plot)')),
            ],
            options={
                'db_table': 'plot_obs',
            },
        ),
        migrations.CreateModel(
            name='Rank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'verbose_name': 'Soziale Stellung',
                'verbose_name_plural': 'Soziale Stellungen',
                'db_table': 'lt_rank',
            },
        ),
        migrations.CreateModel(
            name='RegenFegen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'lt_regenfegen',
            },
        ),
        migrations.CreateModel(
            name='RegenType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Verjüngungsart',
                'verbose_name_plural': 'Verjüngungsarten',
                'db_table': 'lt_regen_type',
            },
        ),
        migrations.CreateModel(
            name='RegenValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=20)),
            ],
            options={
                'verbose_name': 'Verjüngungswert',
                'verbose_name_plural': 'Verjüngungswerte',
                'db_table': 'lt_regenvalue',
            },
        ),
        migrations.CreateModel(
            name='RegenVerbiss',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'lt_regenverbiss',
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_region',
            },
        ),
        migrations.CreateModel(
            name='RegionType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
            options={
                'verbose_name': 'Regiontyp',
                'verbose_name_plural': 'Regiontypen',
                'db_table': 'regiontype',
            },
        ),
        migrations.CreateModel(
            name='Relief',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_relief',
            },
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_sector',
            },
        ),
        migrations.CreateModel(
            name='SoilCompaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Bodenverdichtung',
                'verbose_name_plural': 'Bodenverdichtungen',
                'db_table': 'lt_soil_compaction',
            },
        ),
        migrations.CreateModel(
            name='StandStructure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Bestandesstruktur',
                'verbose_name_plural': 'Bestandesstrukturen',
                'db_table': 'lt_stand_struct',
            },
        ),
        migrations.CreateModel(
            name='Stem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
                ('dead_wood_volume', models.DecimalField(decimal_places=2, max_digits=3)),
            ],
            options={
                'verbose_name': 'Schaft',
                'verbose_name_plural': 'Schäfte',
                'db_table': 'lt_stem',
            },
        ),
        migrations.CreateModel(
            name='SurveyType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_survey_type',
            },
        ),
        migrations.CreateModel(
            name='TreeGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25, unique=True)),
            ],
            options={
                'db_table': 'tree_group',
            },
        ),
        migrations.CreateModel(
            name='UpdatedValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('table_name', models.CharField(max_length=50)),
                ('row_id', models.IntegerField()),
                ('field_name', models.CharField(max_length=50)),
                ('old_value', models.CharField(max_length=255)),
                ('new_value', models.CharField(max_length=255)),
                ('stamp', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(blank=True, default='')),
            ],
            options={
                'db_table': 'updated_value',
            },
        ),
        migrations.CreateModel(
            name='Vita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Lebenslauf',
                'verbose_name_plural': 'Lebensläufe',
                'db_table': 'lt_vita',
            },
        ),
        migrations.CreateModel(
            name='WaldBestandKarte',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(srid=2056)),
                ('entwicklungstufe', models.ForeignKey(blank=True, db_column='entw', null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.develstage')),
                ('mischungsgrad', models.ForeignKey(blank=True, db_column='mg', null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.forestmixture')),
                ('schlussgrad', models.ForeignKey(blank=True, db_column='sg', null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.crownclosure')),
            ],
            options={
                'db_table': 'waldbestandkarte',
            },
        ),
        migrations.CreateModel(
            name='TreeSpecies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('species', models.CharField(max_length=100)),
                ('code', models.SmallIntegerField(blank=True, help_text='National inventory value', null=True)),
                ('abbrev', models.CharField(max_length=4, unique=True)),
                ('tarif_klass20', models.CharField(blank=True, choices=[('fichte', 'Fichte (HAHBART1)'), ('tanne', 'Tanne (HAHBART2)'), ('buche', 'Buche (HAHBART7)'), ('ubrige_laub', 'übrige Laubhölzer (HAHBART12)'), ('ubrige_nad', 'übrige Nadelhölzer (HAHBART6)')], max_length=15)),
                ('is_tree', models.BooleanField(default=True, help_text='True if this is a real tree')),
                ('regen_position', models.PositiveSmallIntegerField(blank=True, help_text='Position in regeneration forms, if any (0-based)', null=True)),
                ('is_historic', models.BooleanField(default=False, help_text='True if the species should not be used for new observations')),
                ('is_special', models.BooleanField(default=False)),
                ('group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.treegroup')),
            ],
            options={
                'db_table': 'tree_spec',
            },
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.PositiveIntegerField(help_text='tree number')),
                ('azimuth', models.SmallIntegerField(help_text='angle in Grads')),
                ('distance', models.DecimalField(decimal_places=2, help_text='[in dm]', max_digits=6)),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_bl.plot')),
                ('spec', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_bl.treespecies')),
            ],
            options={
                'verbose_name': 'Baum',
                'verbose_name_plural': 'Bäume',
                'db_table': 'tree',
            },
        ),
        migrations.CreateModel(
            name='Schutzwald',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('haupt_gef_pot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_bl.gefahrpotential', verbose_name='Hauptgefahrpotential')),
                ('region', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_bl.adminregion')),
            ],
            options={
                'db_table': 'schutzwald',
            },
        ),
        migrations.CreateModel(
            name='RegenObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('perc', models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True)),
                ('fegen', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.regenfegen', verbose_name='Fegen Kategorie')),
                ('obs', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_bl.plotobs')),
                ('perc_an', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='regenobs_an', to='ksp_bl.regenvalue', verbose_name='Anwuchs (10-40cm)')),
                ('perc_auf', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='regenobs_auf', to='ksp_bl.regenvalue', verbose_name='Aufwuchs (41-130cm)')),
                ('spec', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='ksp_bl.treespecies')),
                ('verbiss', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.regenverbiss', verbose_name='Verbisskategorie')),
            ],
            options={
                'db_table': 'regen_obs',
            },
        ),
        migrations.AddField(
            model_name='plotobs',
            name='regen_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.regentype', verbose_name='Verjüngungsart'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.region'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='relief',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.relief'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='sector',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.sector', verbose_name='Sektor'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='soil_compaction',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.soilcompaction', verbose_name='Bodenverdichtung'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_crown_closure',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='plotobs_stand', to='ksp_bl.crownclosure', verbose_name='Schlussgrad'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_crown_closure2015',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.crownclosure2015', verbose_name='Schlussgrad vor 2015'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_devel_stage',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.develstage', verbose_name='Entwicklungsstufe'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_devel_stage2015',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.develstage2015', verbose_name='Entwicklungsstufe vor 2015'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_forest_mixture',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='plotobs_stand', to='ksp_bl.forestmixture', verbose_name='Mischungsgrad'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_structure',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.standstructure'),
        ),
        migrations.AddField(
            model_name='owner',
            name='otype',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_bl.ownertype2', verbose_name='Besitzertyp'),
        ),
        migrations.AddField(
            model_name='adminregion',
            name='region_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_bl.regiontype'),
        ),
        migrations.CreateModel(
            name='TreeObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dbh', models.SmallIntegerField(verbose_name='BHD')),
                ('stem_forked', models.BooleanField(blank=True, null=True, verbose_name='Zwiesel')),
                ('stem_height', models.IntegerField(blank=True, null=True, verbose_name='Stammhöhe')),
                ('woodpecker', models.BooleanField(blank=True, null=True, verbose_name='Spechtlöcher')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
                ('ash_dieback', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.foliagedensity', verbose_name='Eschentriebsterben')),
                ('crown_form', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.crownform', verbose_name='Kronenform')),
                ('crown_length', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.crownlength', verbose_name='Kronenlänge')),
                ('damage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.damage', verbose_name='Schaden')),
                ('damage_cause', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.damagecause', verbose_name='Ursache')),
                ('layer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.layer', verbose_name='Schicht')),
                ('obs', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_bl.plotobs')),
                ('rank', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.rank', verbose_name='Stellung')),
                ('stem', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_bl.stem', verbose_name='Schaftbruch')),
                ('survey_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_bl.surveytype')),
                ('tree', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_bl.tree')),
                ('vita', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='ksp_bl.vita', verbose_name='Lebenslauf')),
            ],
            options={
                'db_table': 'tree_obs',
                'unique_together': {('obs', 'tree')},
            },
        ),
        migrations.AddConstraint(
            model_name='plotobs',
            constraint=models.CheckConstraint(check=models.Q(('forest_edgef__gte', 0), ('forest_edgef__lte', 1)), name='forest_edgef_betw_0_1'),
        ),
        migrations.AlterUniqueTogether(
            name='plotobs',
            unique_together={('plot', 'year')},
        ),
        migrations.AddConstraint(
            model_name='inventory',
            constraint=models.UniqueConstraint(fields=('name', 'inv_from'), name='name_year_unique'),
        ),
    ]
