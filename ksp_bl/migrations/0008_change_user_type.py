from django.db import migrations


def change_user_type(apps, schema_editor):
    ContentType = apps.get_model('contenttypes', 'ContentType')
    ct = ContentType.objects.filter(
        app_label='auth',
        model='user'
    ).first()
    if ct:
        ct.app_label = 'ksp_bl'
        ct.save()


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_bl', '0007_treespecies_color'),
    ]

    operations = [
        migrations.RunPython(change_user_type),
    ]
