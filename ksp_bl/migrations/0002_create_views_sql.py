from pathlib import Path
from django.db import models, migrations

sql_dir = Path(__file__).parent.parent / 'sql_views'


def read_sql(dir_):
    for path in sorted(dir_.glob('*.sql')):
        with path.open('r') as fh:
            yield fh.read()


class Migration(migrations.Migration):
    """
    We read each .sql file in ../sql_views/*.sql and create a RunSQL operation
    for each file.
    """

    dependencies = [
        ('ksp_bl', '0001_initial'),
        ('ksp_bl', '0001_initial_functions'),
    ]

    operations = [migrations.RunSQL(sql_code) for sql_code in read_sql(sql_dir)]
