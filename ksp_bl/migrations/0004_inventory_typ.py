from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_bl', '0003_update_views_sql'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='typ',
            field=models.CharField(choices=[('ksp', 'Standard KSP'), ('co2', 'Simplified CO2')], default='ksp', max_length=20, verbose_name='Inventartyp'),
        ),
    ]
