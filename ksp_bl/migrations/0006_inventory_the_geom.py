import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_bl', '0005_unmanaged_models'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='the_geom',
            field=django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=2056, help_text="Only provide a geometry if it's different from the municipality geometry"),
        ),
    ]
