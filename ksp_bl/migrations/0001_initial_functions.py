from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_bl', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(
            """CREATE OR REPLACE FUNCTION ksp_lokale_dichte(edgef numeric) RETURNS numeric LANGUAGE sql AS $function$
SELECT CASE WHEN $1=1 THEN 100.0 / 3.0
            WHEN $1=0 THEN 0
            ELSE 100.0 / 3.0 * 1 / $1
       END;
$function$;
        COMMENT ON FUNCTION public.ksp_lokale_dichte(numeric) IS 'Repräsentationsfläche eines Aufnahmeplots in Abhängigkeit des Waldrandfaktors. Die lokale Dichte bezeichnet die am Stichprobenpunkt festgestellte (gemessene) räumlich Dichte der Zielvariable pro Flächeneinheit (normalerweise pro Hektar).';
            """,
            "DROP FUNCTION ksp_lokale_dichte(edgef numeric)"
        ),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_volume_ha(diameter smallint, dichte numeric) RETURNS numeric AS $$
  SELECT CASE WHEN $1 = 0 THEN 0 ELSE (($1 * $1 * 0.0011) - (@$1 * 0.0011) - 0.079) * $2 END AS volume_ha;
$$ LANGUAGE SQL;""",
            "DROP FUNCTION ksp_volume_ha(diameter smallint, dichte numeric)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_grundflaeche_bl(diameter smallint) RETURNS double precision AS $$
  SELECT pi() * $1 * $1 / 4 / 10000;
$$ LANGUAGE SQL IMMUTABLE;
COMMENT ON FUNCTION public.ksp_grundflaeche_bl(smallint) IS 'Stammquerschnittsfläche eines Baumes in 1.3 m Höhe (BHD-Messstelle) bzw. Summe der Stammquerschnittsflächen aller Bäume eines Bestandes.';""",
            "DROP FUNCTION ksp_grundflaeche_bl(diameter smallint)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_grundflaeche_ha(diameter smallint, dichte numeric) RETURNS double precision AS $$
  SELECT pi() * $1 * $1 / 4 / 10000 * $2;
$$ LANGUAGE SQL IMMUTABLE;""",
            "DROP FUNCTION ksp_grundflaeche_ha(diameter smallint, dichte numeric)"),


        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_tarif_volume(diameter smallint) RETURNS numeric AS $$
  SELECT CASE WHEN $1 = 0 THEN 0 ELSE (($1 * $1 * 0.0011) - (@$1 * 0.0011) - 0.079) END AS volume_baum;
$$
  LANGUAGE SQL IMMUTABLE""",
            "DROP FUNCTION ksp_tarif_volume(smallint)"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION ksp_tarif_volume(diameter smallint, tarif_klass varchar(15)) RETURNS numeric AS $$
  SELECT CASE WHEN $1 = 0 THEN 0 ELSE (($1 * $1 * 0.0011) - (@$1 * 0.0011) - 0.079) END AS volume_baum;
$$
  LANGUAGE SQL IMMUTABLE""",
            "DROP FUNCTION ksp_tarif_volume(smallint, varchar(15))"),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION public.ksp_waldf_from_bestand(gemeinde_id int)
    RETURNS integer AS $$
    SELECT ST_Area(ST_Intersection(gem.the_geom, ST_Union(ST_Makevalid(ST_Simplify(bestand.geom, 0.1)))))::integer
        FROM waldbestandkarte bestand
        INNER JOIN gemeindegrenzen_bsbl gem ON ST_Intersects(gem.the_geom, bestand.geom)
    WHERE gem.gid=gemeinde_id GROUP BY gem.gid;
$$ LANGUAGE SQL STABLE""",
            "DROP FUNCTION ksp_waldf_from_bestand(int)"
        ),

        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION public.ksp_volume_tarif_wsl_bl_2020(
    dbh integer,
    z25 integer,
    spec_tarif character,
    phyto_tarif character)
    RETURNS double precision
    LANGUAGE 'sql'
    COST 100
    IMMUTABLE PARALLEL UNSAFE
AS $BODY$
    SELECT CASE WHEN dbh <= 0 THEN 0
        ELSE exp(-12.22
            +3.813*ln(dbh)
            +-0.006532*(ln(dbh))^4
            +-0.000264*z25 
            + COALESCE(
             ((spec_tarif='fichte')::integer * 0.06619)
            + ((spec_tarif='tanne')::integer * 0.09481) 
            + ((spec_tarif='ubrige_nad')::integer * -0.01783) 
            + ((spec_tarif='ubrige_laub')::integer * -0.09982),0)
            + COALESCE(
             ((phyto_tarif='vege3')::integer * -0.09471) 
            + ((phyto_tarif='vege4')::integer * 0.01676) 
            + ((phyto_tarif='vege6')::integer * 0.05912) 
            + ((phyto_tarif='vege8')::integer * -0.06575) 
            + ((phyto_tarif='vege10')::integer * 0.03885)
            + ((phyto_tarif='vege14')::integer * -0.0835)
            + ((phyto_tarif='vege16')::integer * -0.01799)
            + ((phyto_tarif='vege17')::integer * -0.07469)
            + ((phyto_tarif='vege24')::integer * -0.03796) 
            + ((phyto_tarif='vege32')::integer * 0.009075)
            + ((phyto_tarif='vege37')::integer * 0.07523) 
            + ((phyto_tarif='vege38')::integer * -0.0235),0))
        END
$BODY$;""",
            "DROP FUNCTION ksp_volume_tarif_wsl_bl_2020(int, int, character, character)"
        ),
    ]
