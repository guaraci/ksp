import json
from collections import OrderedDict
from datetime import date

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.db.models.functions import Transform
from django.contrib.postgres.fields import ArrayField
from django.core.mail import mail_admins
from django.db import models, ProgrammingError
from django.db.utils import ConnectionDoesNotExist
from django.urls import reverse

from colorful.fields import RGBColorField

from gemeinde.models import Gemeinde
from observation.model_utils import (
    CodeDescriptionBase, PlotBase, PlotObsBase, TreeMixin
)

OBSERVATION_FIXTURES = (
    'acidity.json', 'crown_form.json', 'crown_length.json', 'crown_closure.json',
    'damage.json', 'damage_cause.json', 'foliage_density.json', 'forest_form.json',
    'forest_mixture.json', 'gap.json', 'devel_stage.json', 'geology.json',
    'layer.json', 'owner_type.json', 'phytosoc.json', 'rank.json', 'regen_fegen.json',
    'regen_type.json', 'regen_value.json', 'regen_verbiss.json', 'region.json',
    'relief.json', 'sector.json', 'soil_compaction.json', 'stand_structure.json',
    'stem.json', 'survey_type.json', 'vita.json', 'tree_species.json',
)


class User(AbstractUser):
    class Meta:
        db_table = 'auth_user'


class Phytosoc(models.Model):
    code = models.CharField(max_length=6)
    description = models.CharField("Beschreibung", max_length=200)
    inc_class = models.SmallIntegerField("Ertragsklasse")
    ecol_grp = models.CharField("Ökologische Gruppe", max_length=1)
    bwnatur = models.SmallIntegerField(db_column="BWNATURN_WSL", blank=True, null=True)
    tarif_klass20 = models.CharField(max_length=10, blank=True)

    class Meta:
        db_table = 'phytosoc'
        verbose_name = "Phytosoziologie"
        verbose_name_plural = "Phytosoziologie"

    def __str__(self):
        return self.code

EXPO_CHOICES = (
    ('_', "flach"),
    ('N', "Norden"),
    ('S', "Süden"),
)

class Plot(PlotBase):
    nr = models.CharField(max_length=15)
    the_geom = gis_models.PointField(srid=2056)
    point_exact = gis_models.PointField(srid=2056, blank=True, null=True)
    phytosoc = models.ForeignKey(Phytosoc, blank=True, null=True, on_delete=models.SET_NULL)
    slope = models.SmallIntegerField("Neigung", null=True, blank=True)
    exposition = models.CharField(max_length=1, choices=EXPO_CHOICES, blank=True, default='')
    sealevel = models.SmallIntegerField("Höhe ü. Meer", blank=True, null=True)
    checked = models.BooleanField(default=False)

    geom_field = 'the_geom'

    class Meta:
        db_table = 'plot'

    def __str__(self):
        return str(self.nr)

    def save(self, *args, **kwargs):
        if self.pk is None and self.phytosoc is None:
            # When creating a plot, try getting phytosoc from the afw database
            self.phytosoc = self.search_phytosoc()
        super().save(*args, **kwargs)

    def as_geojson(self, dumped=True, srid=None, geom_field='the_geom', for_mobile=False):
        geom = getattr(self, geom_field)
        if (srid is not None and srid != geom.srid):
            geom.transform(srid)
        urlname = 'mobile_plotobs_detail_json' if for_mobile else 'plotobs_detail_json'
        geojson = {
            "type": "Feature",
            "id": self.pk,
            "properties": {
                "id": self.pk,
                "nr": self.nr,
                "slope": self.slope,
                "exposition": self.exposition,
                "sealevel": self.sealevel,
                "radius": self.radius,
                "obsURLs": {str(obs.year): reverse(urlname, args=[obs.pk])
                            for obs in self.plotobs_set.all()},
                "plotData": [
                    {"field": "nr", "label": "Nr", "showEmpty": True},
                    {"field": "sealevel", "label": "Höhe", "unit": "m", "showEmpty": True},
                    {"field": "exposition", "label": "Expo", "showEmpty": True},
                    {"field": "slope", "label": "Neigung", "unit": "%", "showEmpty": True},
                    {"field": "radius", "label": "Radius", "unit": "m", "showEmpty": True},
                ],
            },
            "geometry": json.loads(geom.json),
        }
        return geojson

    def search_phytosoc(self):
        """
        Try to get the phytosociology value correponding to this plot from the
        afw database. Return None if not found.
        """
        if 'afw' in settings.INSTALLED_APPS:
            from afw.models import LutPflanzensoziologie, PflanzensoziologieLaufend
            sozio = PflanzensoziologieLaufend.objects.filter(geom__contains=self.the_geom).only('code').first()
            if sozio:
                try:
                    return Phytosoc.objects.get(code=sozio.code.farbwert)
                except Phytosoc.DoesNotExist:
                    return None
        return None


# **********************
# plot_obs lookup tables
# **********************

class OwnerType(CodeDescriptionBase):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_owner_type'


class Region(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_region'


class Sector(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_sector'


class Gap(CodeDescriptionBase):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_gap'


class DevelStage(CodeDescriptionBase):
    code = models.CharField(max_length=2, unique=True)
    description = models.CharField("Beschreibung", max_length=100)
    bwstru1_val = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        db_table = 'lt_devel_stage'
        ordering = ('code',)
        verbose_name = "Entwicklungsstufe"
        verbose_name_plural = "Entwicklungsstufen"


class DevelStage2015(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField("Beschreibung", max_length=100)
    bwstru1_val = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        db_table = 'lt_devel_stage_2015'
        verbose_name = "Entwicklungsstufe vor 2015"
        verbose_name_plural = "Entwicklungsstufen vor 2015"


class SoilCompaction(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_soil_compaction'
        verbose_name = 'Bodenverdichtung'
        verbose_name_plural = 'Bodenverdichtungen'


class ForestForm(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_forest_form'
        verbose_name = "Waldform"
        verbose_name_plural = "Waldformen"


class RegenType(CodeDescriptionBase):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_regen_type'
        verbose_name = "Verjüngungsart"
        verbose_name_plural = "Verjüngungsarten"


class ForestMixture(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField("Beschreibung", max_length=100)

    class Meta:
        db_table = 'lt_forest_mixture'
        verbose_name = "Mischungsgrad"
        verbose_name_plural = "Mischungsgrade"


class CrownClosure(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField("Beschreibung", max_length=100)
    bwstru1_val = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        db_table = 'lt_crown_closure'
        verbose_name = "Schlussgrad"
        verbose_name_plural = "Schlussgrade"


class CrownClosure2015(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField("Beschreibung", max_length=100)
    bwstru1_val = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        db_table = 'lt_crown_closure_2015'
        verbose_name = "Schlussgrad vor 2015"
        verbose_name_plural = "Schlussgrade vor 2015"


class StandStructure(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)
    bwstru1_val = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        db_table = 'lt_stand_struct'
        verbose_name = "Bestandesstruktur"
        verbose_name_plural = "Bestandesstrukturen"


class Relief(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_relief'


class Acidity(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_acidity'
        verbose_name = 'Acidität'
        verbose_name_plural = 'Aciditäten'


class Geology(CodeDescriptionBase):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_geology'
        verbose_name = 'Geologie'
        verbose_name_plural = 'Geologie'


class InventoryManager(models.Manager):
    def get_by_natural_key(self, name, team):
        return self.get(name=name)


class Inventory(models.Model):
    PERIOD_CHOICES = (
        (0, 'Alte Inventare (Ormalingen, Pratteln)'),
        (1, 'Erste Phase (1987-1999)'),
        (2, 'Zweite Phase (2000-2014)'),
        (3, 'Dritte Phase (2017-)'),
    )
    DENSITY_CHOICES = (
        (10000, '1/ha (10000m2)'),
        (20000, '0,5/ha (20000m2)'),
    )
    INVENTAR_TYP_CHOICES = (
        ('ksp', 'Standard KSP'),
        ('co2', 'Simplified CO2'),
    )
    name = models.CharField("Inventar", max_length=150)
    team = models.CharField("Aufnahmeteam", max_length=100)
    period = models.SmallIntegerField("Aufnahmeperiode", choices=PERIOD_CHOICES)
    ordering = models.SmallIntegerField("Aufnahmereihenfolge", null=True)
    inv_from = models.DateField("Von")
    inv_to = models.DateField("Bis")
    municipality = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', on_delete=models.PROTECT, blank=True, null=True)
    typ = models.CharField("Inventartyp", max_length=20, choices=INVENTAR_TYP_CHOICES, default='ksp')
    default_density = models.IntegerField("StandardDichte", choices=DENSITY_CHOICES, null=True, blank=True)
    excluded_plots = ArrayField(
        models.IntegerField(), null=True, blank=True, verbose_name="Ausgeschlossene pünkte"
    )
    the_geom = gis_models.PolygonField(
        srid=2056, blank=True, null=True,
        help_text="Only provide a geometry if it's different from the municipality geometry"
    )
    remarks = models.TextField("Bemerkungen", blank=True)

    objects = InventoryManager()

    class Meta:
        db_table = 'inventory'
        verbose_name = 'Inventar'
        verbose_name_plural = 'Inventare'
        constraints = [models.UniqueConstraint(fields=['name', 'inv_from'], name='name_year_unique')]

    def __str__(self):
        return "%s (%s – %s)" % (self.name, self.inv_from, self.inv_to)

    @property
    def year(self):
        return self.inv_from.year if self.inv_from else None

    @property
    def edit_url(self):
        return reverse('inventory-edit', args=[self.pk])

    @property
    def plotobs_url(self):
        return reverse('plotobs') + f'?year={self.year}&gem={self.municipality_id}'

    def layer_urls(self):
        return {'BestandesKarte': reverse('obs-bestand', args=[self.pk])}

    @property
    def geom(self):
        if self.the_geom:
            return self.the_geom
        if self.municipality:
            return self.municipality.the_geom
        return self.plotobs_set.values('inv_team').annotate(coll=gis_models.Collect('plot__the_geom'))[0]['coll'].envelope

    def geom_geojson(self, dumped=True, srid=None):
        if not self.the_geom and self.municipality:
            return self.municipality.as_geojson(dumped=dumped, srid=srid)
        geom = self.geom
        if (srid is not None and srid != geom.srid):
            geom.transform(srid)

        geojson = {
            "type": "Feature",
            "id": self.pk,
            "crs": {"type": "name", "properties": {"name": "urn:x-ogc:def:crs:EPSG:%s" % geom.srid}},
            "properties": {
                "id": self.pk,
                "name": self.name,
            },
            "geometry": json.loads(geom.json),
        }
        if dumped:
            return json.dumps(geojson)
        return geojson


    @property
    def previous(self):
        if self.municipality:
            return Inventory.objects.filter(municipality=self.municipality
                ).exclude(pk=self.pk).order_by('-inv_from').first()
        else:
            return Inventory.objects.filter(name=self.name
                ).exclude(pk=self.pk).order_by('-inv_from').first()

    @property
    def is_passed(self):
        return date.today() > self.inv_to

    def natural_key(self):
        return (self.name, self.team)

    def get_plots(self, srid=None):
        plot_qs = Plot.objects.filter(
            the_geom__within=self.geom
        ).annotate(
            plotgeom=Transform('the_geom', srid), plotgeomexact=Transform('point_exact', srid)
        )
        if self.excluded_plots:
            plot_qs = plot_qs.exclude(pk__in=self.excluded_plots)
        return plot_qs


class OwnerType2(models.Model):
    # modelled after the afw common.lut_egKategorie table.
    name = models.CharField(max_length=100, unique=True)
    status = models.CharField(max_length=1, choices=(('o', 'Öffentlich'), ('p', 'Privat')))

    class Meta:
        db_table = 'owner_type'

    def __str__(self):
        return self.name


class Owner(models.Model):
    # Unfortunately, there is no unique number per owner currently in the afw database, which is the source.
    nr = models.CharField(max_length=10, blank=True)
    name = models.CharField(max_length=100, unique=True)
    otype = models.ForeignKey(
        OwnerType2, on_delete=models.PROTECT, null=True, blank=True, verbose_name="Besitzertyp"
    )
    geom = gis_models.MultiPolygonField(srid=2056, null=True, blank=True)
    bp_pflichtig = models.BooleanField("betriebsplanpflichtig", default=False)
    updated = models.DateTimeField(blank=True, null=True)  # Set by script in populate_regions

    class Meta:
        db_table = 'owner'

    def __str__(self):
        return "%s (%s)" % (self.name, self.nr)


class PlotObs(PlotObsBase):
    plot = models.ForeignKey(Plot, verbose_name="Aufnahmepunkt (plot)", on_delete=models.CASCADE)
    year = models.SmallIntegerField(verbose_name='Aufnahmejahr', db_index=True)
    inv_team = models.ForeignKey(Inventory, on_delete=models.PROTECT)
    owner = models.ForeignKey(Owner, verbose_name="Eigentümer", null=True, blank=True, on_delete=models.PROTECT)
    owner_type = models.ForeignKey(OwnerType, verbose_name="Besitzertyp", null=True, blank=True, on_delete=models.SET_NULL)
    municipality = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', on_delete=models.PROTECT)
    density = models.IntegerField("Dichte (m2)", null=True)
    region = models.ForeignKey(Region, null=True, blank=True, on_delete=models.SET_NULL)
    sector = models.ForeignKey(Sector, verbose_name="Sektor", null=True, blank=True, on_delete=models.SET_NULL)
    area = models.SmallIntegerField(null=True, blank=True)
    subsector = models.CharField(max_length=2, blank=True)
    evaluation_unit = models.CharField("Auswertungseinheit", max_length=4, blank=True)
    forest_clearing = models.BooleanField(default=False)
    gap = models.ForeignKey(Gap, verbose_name="Blösse", null=True, blank=True, on_delete=models.SET_NULL)
    stand = models.SmallIntegerField(null=True, blank=True)  # To be removed
    stand_devel_stage = models.ForeignKey(DevelStage, verbose_name="Entwicklungsstufe",
        null=True, blank=True, on_delete=models.SET_NULL)
    stand_devel_stage2015 = models.ForeignKey(DevelStage2015, verbose_name="Entwicklungsstufe vor 2015",
        null=True, blank=True, on_delete=models.SET_NULL)
    stand_forest_mixture = models.ForeignKey(ForestMixture, verbose_name="Mischungsgrad",
        related_name="plotobs_stand", null=True, blank=True, on_delete=models.SET_NULL)
    stand_crown_closure = models.ForeignKey(CrownClosure, verbose_name="Schlussgrad",
        related_name="plotobs_stand", null=True, blank=True, on_delete=models.SET_NULL)
    stand_crown_closure2015 = models.ForeignKey(CrownClosure2015, verbose_name="Schlussgrad vor 2015",
        null=True, blank=True, on_delete=models.SET_NULL)
    soil_compaction = models.ForeignKey(SoilCompaction, verbose_name="Bodenverdichtung",
        null=True, blank=True, on_delete=models.SET_NULL)
    forest_form = models.ForeignKey(ForestForm, verbose_name="Waldform", null=True, blank=True, on_delete=models.SET_NULL)
    regen_type = models.ForeignKey(RegenType, verbose_name="Verjüngungsart", null=True, blank=True, on_delete=models.SET_NULL)
    # This field is not used, `stand_forest_mixture` contains the trusted data (#2).
    forest_mixture = models.ForeignKey(ForestMixture, null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+')
    # This field is not used, `stand_crown_closure` contains the trusted data (#2).
    crown_closure = models.ForeignKey(CrownClosure, null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+')
    stand_structure = models.ForeignKey(StandStructure, null=True, blank=True, on_delete=models.SET_NULL)
    # 0<val<1 is enforced at database level
    forest_edgef = models.DecimalField("Waldrandfaktor", max_digits=2, decimal_places=1)
    gwl = models.FloatField("Gesamtwuchsleistung (gwl)")
    relief = models.ForeignKey(Relief, null=True, blank=True, on_delete=models.SET_NULL)
    geology = models.ForeignKey(Geology, verbose_name="Geologie", null=True, blank=True, on_delete=models.SET_NULL)
    acidity = models.ForeignKey(Acidity, verbose_name="Azidität", null=True, blank=True, on_delete=models.SET_NULL)
    remarks = models.TextField("Bermerkungen", blank=True)
    # Legacy field
    iprobenr = models.SmallIntegerField("alte Probennummer", blank=True, null=True)

    class Meta:
        db_table = 'plot_obs'
        unique_together = ("plot", "year")
        constraints = [
            models.CheckConstraint(
                check=models.Q(forest_edgef__gte=0, forest_edgef__lte=1), name='forest_edgef_betw_0_1'
            ),
        ]

    def __str__(self):
        return "%s (%s)" % (self.plot.nr, self.year)

    def get_absolute_url(self):
        return reverse('plotobs_detail', args=[self.pk])

    def save(self, *args, **kwargs):
        if self.pk is None and self.owner is None:
            # When creating a plotobs, try getting owner automatically.
            self.owner = Owner.objects.filter(geom__contains=self.plot.the_geom).order_by('-updated').first()
        super().save(*args, **kwargs)

    def visible_fields(self, exclude_empty=True):
        """
        Return a list of interesting/visible field names for the plot, having a
        non-empty value.
        """
        return [
            field.name
            for field in self._meta.fields
            if (not field.name.startswith('i') and not field.name.endswith('2015')
                and (not exclude_empty or getattr(self, field.name) not in (None, '')))
        ]

    def fix_edge_factor(self):
        """
        When forest edge factor is 0 for an non-clearing plot, it should be
        corrected to 1 (100%).
        """
        if self.forest_edgef == 0 and not self.forest_clearing:
            self.forest_edgef = 1
            self.save()


class UpdatedValue(models.Model):
    """
    This table logs updated values, differing fomr those originally imported.
    """
    table_name = models.CharField(max_length=50)
    row_id = models.IntegerField()
    field_name = models.CharField(max_length=50)
    old_value = models.CharField(max_length=255)
    new_value = models.CharField(max_length=255)
    stamp = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(blank=True, default='')

    class Meta:
        db_table = 'updated_value'

    def __str__(self):
        return "%s.%s.%s (%s)" % (self.table_name, self.field_name, self.row_id, self.stamp)

    @classmethod
    def add(self, obj, field_name, old_val, what):
        UpdatedValue.objects.create(
            table_name=obj._meta.db_table, row_id=obj.pk, field_name=field_name,
            old_value=old_val, new_value=getattr(obj, field_name), comment=what
        )

    @classmethod
    def deleted(self, obj, comment):
        UpdatedValue.objects.create(
            table_name=obj._meta.db_table, row_id=obj.pk, field_name='',
            old_value='', new_value='', comment=comment,
        )


class TreeGroup(models.Model):
    name = models.CharField(max_length=25, unique=True)

    class Meta:
        db_table = 'tree_group'

    def __str__(self):
        return self.name


class TreeSpecies(models.Model):
    TARIF20_CHOICES = (
        ('fichte', 'Fichte (HAHBART1)'),
        ('tanne', 'Tanne (HAHBART2)'),
        ('buche', 'Buche (HAHBART7)'),
        ('ubrige_laub', 'übrige Laubhölzer (HAHBART12)'),
        ('ubrige_nad', 'übrige Nadelhölzer (HAHBART6)'),
    )
    species = models.CharField(max_length=100)
    code = models.SmallIntegerField(blank=True, null=True,
        help_text="National inventory value")
    abbrev = models.CharField(max_length=4, unique=True)
    group = models.ForeignKey(TreeGroup, null=True, blank=True, on_delete=models.SET_NULL)
    tarif_klass20 = models.CharField(max_length=15, choices=TARIF20_CHOICES, blank=True)
    is_tree = models.BooleanField(default=True, help_text="True if this is a real tree")
    regen_position = models.PositiveSmallIntegerField(null=True, blank=True, help_text="Position in regeneration forms, if any (0-based)")
    color = RGBColorField("Farbe", blank=True)
    is_historic = models.BooleanField(
        default=False, help_text="True if the species should not be used for new observations"
    )
    is_special = models.BooleanField(default=False)

    name_field = 'species'

    class Meta:
        db_table = 'tree_spec'

    def __str__(self):
        return self.species


class Tree(TreeMixin, models.Model):
    plot = models.ForeignKey(Plot, on_delete=models.CASCADE)
    spec = models.ForeignKey(TreeSpecies, blank=True, null=True, on_delete=models.PROTECT)
    nr = models.PositiveIntegerField(help_text='tree number')
    azimuth = models.SmallIntegerField(help_text='angle in Grads')
    distance = models.DecimalField(max_digits=6, decimal_places=2, help_text='[in dm]')

    class Meta:
        db_table = 'tree'
        verbose_name = 'Baum'
        verbose_name_plural = 'Bäume'

    def __str__(self):
        return "Tree (%s) on plot %s (az:%d dist:%dm)" % (self.spec, self.plot.nr, self.azimuth, self.distance/10)


class RegenValue(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=20)

    class Meta:
        db_table = 'lt_regenvalue'
        verbose_name = 'Verjüngungswert'
        verbose_name_plural = 'Verjüngungswerte'


class RegenVerbiss(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=20)

    class Meta:
        db_table = 'lt_regenverbiss'


class RegenFegen(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=20)

    class Meta:
        db_table = 'lt_regenfegen'


class RegenObs(models.Model):
    obs = models.ForeignKey(PlotObs, on_delete=models.CASCADE)
    spec = models.ForeignKey(TreeSpecies, on_delete=models.PROTECT)
    perc = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    perc_an = models.ForeignKey(RegenValue, null=True, blank=True,
        verbose_name="Anwuchs (10-40cm)", related_name="regenobs_an", on_delete=models.SET_NULL)
    perc_auf = models.ForeignKey(RegenValue, null=True, blank=True,
        verbose_name="Aufwuchs (41-130cm)", related_name="regenobs_auf", on_delete=models.SET_NULL)
    verbiss = models.ForeignKey(RegenVerbiss, null=True, blank=True,
        verbose_name="Verbisskategorie", on_delete=models.SET_NULL)
    fegen = models.ForeignKey(RegenFegen, null=True, blank=True,
        verbose_name="Fegen Kategorie", on_delete=models.SET_NULL)

    class Meta:
        db_table = 'regen_obs'

    def __str__(self):
        return "Plot %s (obs %s), %s" % (self.obs.plot, self.obs, self.spec)


# **********************
# tree_obs lookup tables
# **********************

class SurveyType(CodeDescriptionBase):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_survey_type'


class Layer(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)
    explanation = models.TextField(default='', blank=True)

    class Meta:
        db_table = 'lt_layer'


class Rank(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)
    explanation = models.TextField(default='', blank=True)

    class Meta:
        db_table = 'lt_rank'
        verbose_name = 'Soziale Stellung'
        verbose_name_plural = 'Soziale Stellungen'


class Vita(CodeDescriptionBase):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_vita'
        verbose_name = 'Lebenslauf'
        verbose_name_plural = 'Lebensläufe'


class Damage(CodeDescriptionBase):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_damage'
        verbose_name = 'Schadenart'
        verbose_name_plural = 'Schadenarten'


class DamageCause(CodeDescriptionBase):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_damage_cause'
        verbose_name = 'Schadensursache'
        verbose_name_plural = 'Schadensursachen'


class CrownForm(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)
    explanation = models.TextField(default='', blank=True)

    class Meta:
        db_table = 'lt_crown_form'
        verbose_name = 'Kronenform'
        verbose_name_plural = 'Kronenformen'


class CrownLength(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)
    explanation = models.TextField(default='', blank=True)

    class Meta:
        db_table = 'lt_crown_len'
        verbose_name = 'Kronenlänge'
        verbose_name_plural = 'Kronenlängen'


class Stem(CodeDescriptionBase):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=100)
    dead_wood_volume = models.DecimalField(max_digits=3, decimal_places=2)

    class Meta:
        db_table = 'lt_stem'
        verbose_name = 'Schaft'
        verbose_name_plural = 'Schäfte'


class FoliageDensity(CodeDescriptionBase):
    code = models.SmallIntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_foliagedensity'


class TreeObs(models.Model):
    obs = models.ForeignKey(PlotObs, on_delete=models.CASCADE)
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE)
    survey_type = models.ForeignKey(SurveyType, on_delete=models.PROTECT, null=True, blank=True)
    layer = models.ForeignKey(Layer, verbose_name="Schicht", null=True, blank=True,
        on_delete=models.SET_NULL)
    dbh = models.SmallIntegerField("BHD")
    rank = models.ForeignKey(Rank, verbose_name="Stellung", null=True, blank=True,
        on_delete=models.SET_NULL)
    vita = models.ForeignKey(Vita, verbose_name="Lebenslauf", on_delete=models.PROTECT)
    damage = models.ForeignKey(Damage, verbose_name="Schaden", null=True, blank=True,
        on_delete=models.SET_NULL)
    damage_cause = models.ForeignKey(DamageCause, verbose_name="Ursache", null=True, blank=True,
        on_delete=models.SET_NULL)
    crown_length = models.ForeignKey(CrownLength, verbose_name="Kronenlänge", null=True, blank=True,
        on_delete=models.SET_NULL)
    crown_form = models.ForeignKey(CrownForm, verbose_name="Kronenform", null=True, blank=True,
        on_delete=models.SET_NULL)
    stem = models.ForeignKey(Stem, verbose_name="Schaftbruch", null=True, blank=True,
        on_delete=models.SET_NULL)
    stem_forked = models.BooleanField("Zwiesel", null=True, blank=True)
    stem_height = models.IntegerField("Stammhöhe", null=True, blank=True)
    woodpecker = models.BooleanField("Spechtlöcher", null=True, blank=True)
    ash_dieback = models.ForeignKey(FoliageDensity, verbose_name="Eschentriebsterben",
        null=True, blank=True, on_delete=models.SET_NULL)
    remarks = models.TextField("Bemerkungen", blank=True)

    class Meta:
        db_table = 'tree_obs'
        unique_together = [('obs', 'tree')]

    def __str__(self):
        return "Tree obs %s" % (self.id,)

    def save(self, *args, **kwargs):
        if self.survey_type_id is None:
            self.survey_type = SurveyType.objects.filter(code='K').first()
        return super().save(*args, **kwargs)


class RegionType(models.Model):
    name = models.CharField(max_length=20)

    class Meta:
        db_table = 'regiontype'
        verbose_name = 'Regiontyp'
        verbose_name_plural = 'Regiontypen'

    def __str__(self):
        return self.name


class AdminRegion(models.Model):
    nr = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=100)
    region_type = models.ForeignKey(RegionType, null=True, on_delete=models.PROTECT)
    geom = gis_models.MultiPolygonField(srid=2056, null=True, blank=True)

    geom_field = 'geom'

    class Meta:
        db_table = 'adminregion'

    def __str__(self):
        return "%s %s (%s)" % (self.region_type.name, self.name, self.nr)


class GefahrPotential(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'gefahrpotential'

    def __str__(self):
        return self.name


class Schutzwald(models.Model):
    region = models.ForeignKey(AdminRegion, on_delete=models.CASCADE)
    haupt_gef_pot = models.ForeignKey(GefahrPotential, on_delete=models.CASCADE,
        verbose_name="Hauptgefahrpotential")

    class Meta:
        db_table = 'schutzwald'

    def __str__(self):
        return "Schutzwald %s (%s)" % (self.region.name, self.haupt_gef_pot)


class WaldBestandKarte(models.Model):
    """
    More or less equivalent to afw.WaldBestandLaufend, which is currently outdated.
    Only fields useful for KSP have been extracted from the source shape.
    """
    entwicklungstufe = models.ForeignKey(DevelStage, blank=True, null=True,
        db_column='entw', on_delete=models.SET_NULL)
    mischungsgrad = models.ForeignKey(ForestMixture, blank=True, null=True,
        db_column='mg', on_delete=models.SET_NULL)
    schlussgrad = models.ForeignKey(CrownClosure, blank=True, null=True,
        db_column='sg', on_delete=models.SET_NULL)
    geom = gis_models.PolygonField(srid=2056)

    class Meta:
        db_table = 'waldbestandkarte'
