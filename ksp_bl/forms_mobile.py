from django import forms
from django.contrib.gis.geos import Point
from django.forms.renderers import get_default_renderer

from mobile.forms import RadioNullBoolean, RadioSelectLabelAfter, SpeciesSelect
from .models import (
    Plot, PlotObs, RegenObs, RegenValue, RegenVerbiss, RegenFegen, Relief, Stem,
    TreeObs, TreeSpecies
)


class DevelStageSelect(forms.Select):
    def create_option(self, *args, **kwargs):
        opt = super().create_option(*args, **kwargs)
        if 'historisch' in opt['label']:
            opt['attrs']['disabled'] = True
        return opt


class POFormStep1(forms.ModelForm):
    forest_edgef = forms.DecimalField(
        label="Waldrandfaktor", min_value=0.5, max_value=1.0, max_digits=2, decimal_places=1
    )
    template_name = 'mobile/form_step1.html'

    help_content = {
        'forest_edgef': {
            'title': "Abstandsberechnung für Waldrandfaktor",
            'template': 'mobile/forest_edgef.html',
        }
    }

    class Meta:
        model = PlotObs
        fields = (
            'municipality', 'inv_team', 'forest_edgef', 'stand_devel_stage',
            'stand_forest_mixture', 'stand_crown_closure', 'soil_compaction',
            'forest_form', 'regen_type',
            'relief'  # Only used when receiving data, not when producing the form (as present in PlotForm)
        )
        widgets = {
            'stand_devel_stage': DevelStageSelect,
            'inv_team': forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        co2_ignored_fields = [
            'stand_forest_mixture', 'stand_crown_closure', 'soil_compaction',
            'forest_form', 'regen_type',
        ]
        if not kwargs['instance']:
            del self.fields['relief']
        # get_from_mobile_data is resilient to missing inv_team
        self.fields['inv_team'].required = False
        if kwargs['instance'] and kwargs['instance'].inv_team.typ == 'co2':
            for fname in co2_ignored_fields:
                del self.fields[fname]
        self.fields['municipality'].can_be_locked = True
        self.fields['municipality'].queryset = self.fields['municipality'].queryset.order_by('name')


class RegenForm(forms.ModelForm):
    class Meta:
        model = RegenObs
        exclude = ('perc',)
        widgets = {'obs': forms.HiddenInput, 'spec': forms.HiddenInput}

    def __init__(self, *args, spec=None, **kwargs):
        self.species = spec
        if 'initial' not in kwargs:
            kwargs['initial'] = {
                'spec': self.species.pk,
                'perc_an': RegenValue.objects.get(code=1).pk,
                'perc_auf': RegenValue.objects.get(code=1).pk,
                'verbiss': RegenVerbiss.objects.get(code=1).pk,
                'fegen': RegenFegen.objects.get(code=1).pk,
            }
        super().__init__(*args, **kwargs)

    def has_changed(self):
        # This form should always be saved, even if initial data didn't change
        return True


class RegenFormset(forms.BaseInlineFormSet):
    form = RegenForm
    model = RegenObs
    fk = RegenObs._meta.get_field('obs')
    validate_min = False
    validate_max = False
    extra = 0
    can_order = False
    can_delete = False
    edit_only = False
    template_name = 'ksp_bl/form_step2.html'

    def __init__(self, *args, **kwargs):
        kwargs['queryset'] = RegenObs.objects.none()
        kwargs['prefix'] = 'regenform'
        super().__init__(*args, **kwargs)
        self.renderer = get_default_renderer()
        self.species = list(TreeSpecies.objects.filter(regen_position__isnull=False).order_by('regen_position'))

    @property
    def min_num(self):
        return len(self.species)

    @property
    def max_num(self):
        return len(self.species)

    @property
    def absolute_max(self):
        return len(self.species)

    def get_form_kwargs(self, index):
        return {'spec': self.species[index]}


class PlotForm(forms.ModelForm):
    exact_long = forms.FloatField(label="Genaue Länge", required=False)
    exact_lat = forms.FloatField(label="Genaue Breite", required=False)
    # A field borrowed from PlotObs
    relief = forms.ModelChoiceField(
        queryset=Relief.objects.all(), widget=RadioSelectLabelAfter, required=False, blank=True
    )
    template_name = 'mobile/form_step_plot.html'

    class Meta:
        model = Plot
        fields = ('exact_long', 'exact_lat', 'exposition', 'slope')
        widgets = {
            'exposition': RadioSelectLabelAfter,
        }

    def save(self, *args, **kwargs):
        plot = super().save(*args, **kwargs)
        lon, lat = self.cleaned_data.get('exact_long'), self.cleaned_data.get('exact_lat')
        if lon and lat:
            plot.point_exact = Point(lon, lat, srid=2056)
            plot.set_elevation_from_swisstopo(save=False)
            plot.save()
        return plot


class RemarkForm(forms.ModelForm):
    template_name = 'mobile/form_step_remarks.html'

    class Meta:
        model = PlotObs
        fields = ['remarks']


TREE_FIELDS = ('species', 'nr', 'distance', 'azimuth')


class TreeForm(forms.ModelForm):
    species = forms.ModelChoiceField(
        queryset=TreeSpecies.objects.filter(is_tree=True).order_by('species'),
        widget=SpeciesSelect
    )
    nr = forms.IntegerField()
    distance = forms.DecimalField(label="Distanz", max_digits=6, decimal_places=2)
    azimuth = forms.IntegerField(min_value=0, max_value=400)
    dbh = forms.IntegerField(label="BHD", min_value=12, max_value=200)
    perim = forms.IntegerField(label="Umfang", max_value=900, required=False)

    class Meta:
        model = TreeObs
        fields = TREE_FIELDS + (
            'tree', 'vita', 'dbh', 'layer', 'rank', 'damage', 'damage_cause',
            'crown_length', 'crown_form', 'stem', 'stem_forked', 'stem_height',
            'woodpecker', 'ash_dieback', 'remarks',
        )
        widgets = {
            'tree': forms.HiddenInput,
            'stem_forked': RadioNullBoolean,
            'woodpecker': RadioNullBoolean,
        }

    def __init__(self, *args, **kwargs):
        if 'initial' not in kwargs:
            kwargs['initial'] = {
                'stem': Stem.objects.get(code='B').pk,
            }
        super().__init__(*args, **kwargs)
        co2_ignored_fields = [
            'layer', 'rank', 'damage', 'damage_cause',
            'crown_length', 'crown_form', 'stem', 'stem_forked',
            'woodpecker', 'ash_dieback'
        ]
        if kwargs.get('instance') and kwargs['instance'].obs.inv_team.typ == 'co2':
            for fname in co2_ignored_fields:
                del self.fields[fname]

        # azimuth unit depends on user choice
        self.fields['distance'].unit = 'dm'
        self.fields['dbh'].unit = 'cm'

    def other_fields(self):
        for field in self.visible_fields():
            if field.name not in TREE_FIELDS + ('perim',):
                yield field

    def add_error(self, field, error):
        try:
            vita_code = self.fields['vita'].clean(self.data['vita']).code
        except Exception:
            pass
        else:
            if field in ('dbh', 'azimuth') and vita_code in ('c', 'x', 'y'):
                # Ignore dbh/azimuth errors for cut/not considered trees
                self.cleaned_data[field] = self.data[field]
                return
        super().add_error(field, error)

    def save(self, **kwargs):
        # Save tree fields if changed
        tree_changed = False
        for fname in TREE_FIELDS:
            tree_field = 'spec' if fname == 'species' else fname
            if getattr(self.instance.tree, tree_field) != self.cleaned_data[fname]:
                setattr(self.instance.tree, tree_field, self.cleaned_data[fname])
                tree_changed = True
        if tree_changed:
            self.instance.tree.save()
        return super().save(**kwargs)


def get_step_forms(data=None, instance=None):
    regen_data = {
        k: (v[0] if isinstance(v, list) else v)
        for k, v in data.items() if k.startswith('regenform')
    } if data else None
    forms = [
        {'label': 'Schritt 1', 'form': POFormStep1(data=data, instance=instance)},
        {'label': 'Schritt 2 - Verjüngung', 'form': RegenFormset(data=regen_data, instance=instance)},
        {'label': 'Schritt 3', 'form': PlotForm(data=data, instance=instance.plot if instance else None)},
        {'label': 'Schritt 4 - Bemerkungen', 'form': RemarkForm(data=data, instance=instance)},
    ]
    if instance is not None and instance.inv_team.typ == 'co2':
        # Skip RegenObs for co2 inventories
        forms.pop(1)
    return forms


def get_tree_form():
    return TreeForm()
