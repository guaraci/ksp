from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.forms import OSMWidget
from django.forms import TextInput
from django.utils.safestring import SafeString

from observation.admin import CoordField, MyOSMWidget, export_to_geojson, export_to_json
from . import models


@admin.register(models.User)
class UserAdmin(DjangoUserAdmin):
    pass


@admin.register(models.Inventory)
class InventoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'municipality', 'inv_from', 'inv_to', 'period', 'ordering')
    list_filter = ('period',)
    search_fields = ('name',)
    ordering = ('name', 'inv_from')
    formfield_overrides = {
        gis_models.PolygonField: {'widget': MyOSMWidget},
    }


class RegenObsInline(admin.TabularInline):
    model = models.RegenObs
    extra = 0



@admin.register(models.Plot)
class PlotAdmin(admin.ModelAdmin):
    list_display = ('nr', 'sealevel')
    search_fields = ('nr',)

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        if db_field.name in ('the_geom', 'point_exact'):
            return CoordField(widget=TextInput(attrs={'size': 60}), required=(db_field.name == 'the_geom'), **kwargs)
        return super().formfield_for_dbfield(db_field, request, **kwargs)


@admin.register(models.PlotObs)
class PlotObsAdmin(admin.ModelAdmin):
    list_display = ('plot', 'year', 'municipality')
    search_fields = ('plot__nr',)
    raw_id_fields = ('plot', 'owner')
    actions = [export_to_json, export_to_geojson]
    inlines = [RegenObsInline]
    fieldsets = (
        (None, {
            # Omitted: forest_mixture, crown_closure
            'fields': (
                'plot', 'year', 'inv_team', 'owner', 'owner_type', 'municipality', 'region',
                'sector', 'area', 'subsector', 'evaluation_unit', 'forest_clearing',
                'gap', 'stand', 'stand_devel_stage', 'stand_devel_stage2015',
                'stand_forest_mixture', 'stand_crown_closure', 'stand_crown_closure2015',
                'soil_compaction', 'forest_form', 'regen_type', 'stand_structure',
                'forest_edgef', 'gwl', 'relief', 'geology', 'acidity', 'remarks',
                'iprobenr',
            )
        }),
    )


@admin.register(models.TreeSpecies)
class TreeSpeciesAdmin(admin.ModelAdmin):
    list_display = (
        'species', 'abbrev', 'code', 'color_colored', 'group', 'tarif_klass20', 'is_tree',
        'is_historic', 'is_special',
    )
    ordering = ('species',)

    @admin.display(description='Farbe')
    def color_colored(self, obj):
        if obj.color:
            return SafeString(f'<span style="color: {obj.color}">{obj.color}</span>')
        return ''


@admin.register(models.Tree)
class TreeAdmin(admin.ModelAdmin):
    raw_id_fields = ('plot',)
    search_fields = ('plot__nr',)


@admin.register(models.Phytosoc)
class PhytosocAdmin(admin.ModelAdmin):
    search_fields = ('code', 'description')
    list_display = ('code', 'description', 'inc_class', 'ecol_grp', 'bwnatur', 'tarif_klass20')
    ordering = ('code',)


@admin.register(models.TreeObs)
class TreeObsAdmin(admin.ModelAdmin):
    search_fields = ('id', 'obs__plot__nr',)
    raw_id_fields = ('obs', 'tree')


@admin.register(models.RegenObs)
class RegenObsAdmin(admin.ModelAdmin):
    list_display = ('obs', 'spec', 'perc')
    raw_id_fields = ('obs',)


@admin.register(
    models.DevelStage, models.DevelStage2015, models.CrownClosure, models.CrownClosure2015,
    models.StandStructure,
)
class LTWithBwstru1Admin(admin.ModelAdmin):
    list_display = ('description', 'code', 'bwstru1_val')
    ordering = ('id',)


@admin.register(models.OwnerType2)
class OwnerType2Admin(admin.ModelAdmin):
    list_display = ['name', 'status']


@admin.register(models.Owner)
class OwnerAdmin(admin.ModelAdmin):
    ordering = ['name']
    list_display = ['name', 'nr', 'otype', 'updated', 'area']
    list_filter = ['otype']
    search_fields = ['name']
    formfield_overrides = {
        gis_models.MultiPolygonField: {'widget': OSMWidget},
    }

    def area(self, obj):
        if obj.geom:
            return str(round(obj.geom.area/10000, 2)) + ' ha'
        return '-'


@admin.register(models.AdminRegion)
class AdminRegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'nr', 'region_type')
    list_filter = ('region_type',)
    ordering = ('name',)
    search_fields = ('name', 'nr')
    formfield_overrides = {
        gis_models.GeometryField: {'widget': OSMWidget},
    }

    #def get_readonly_fields(self, request, obj=None):
    #    fields = super().get_readonly_fields(request, obj=obj)
    #    if obj is not None and obj.region_type.name != 'Freiperimeter':
    #        fields += ('geom',)
    #    return fields

    def save_model(self, request, *args, **kwargs):
        self.message_user(
            request,
            "Don't forget to update the plot_in_region materialized view (REFRESH "
            "MATERIALIZED VIEW plot_in_region;) so as request grouping fully works "
            "with the new data.",
            messages.WARNING
        )
        super().save_model(request, *args, **kwargs)


admin.site.register(models.OwnerType)
admin.site.register(models.TreeGroup)
admin.site.register(models.Region)
admin.site.register(models.Sector)
admin.site.register(models.Gap)
admin.site.register(models.SoilCompaction)
admin.site.register(models.ForestForm)
admin.site.register(models.RegenType)
admin.site.register(models.RegenValue)
admin.site.register(models.ForestMixture)
admin.site.register(models.Relief)
admin.site.register(models.Acidity)
admin.site.register(models.Geology)
admin.site.register(models.SurveyType)
admin.site.register(models.Layer)
admin.site.register(models.Rank)
admin.site.register(models.Vita)
admin.site.register(models.Damage)
admin.site.register(models.DamageCause)
admin.site.register(models.CrownForm)
admin.site.register(models.CrownLength)
admin.site.register(models.Stem)
admin.site.register(models.FoliageDensity)
admin.site.register(models.UpdatedValue)
admin.site.register(models.RegionType)
