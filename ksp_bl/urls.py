from django.conf import settings
from django.urls import include, path

from observation.views import InventoryEditView
from . import views

urlpatterns = [
    path('', views.BLGemeindenView.as_view(), name='home'),
    path('inventory/new/<int:gem_pk>/', InventoryEditView.as_view(), name='inventory-create'),
    path('inventory/<int:pk>/edit/', InventoryEditView.as_view(), name='inventory-edit'),

    path('afw/', include('afw.urls')),

    path('mobile/', include('mobile.urls')),
]

if settings.REDIRECT_TILES:
    # Redirect map tiles in development
    from mobile import views as mobile_views

    urlpatterns.append(
        path('geowms.bl.ch/<path:path>', mobile_views.map_redirect)
    )
