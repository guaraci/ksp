CREATE OR REPLACE VIEW public.bv_giganten AS
 SELECT tree_obs.id,
    tree_spec.species,
    tree_obs.dbh,
    plot_obs.id as plot_obs_id,
    plot_obs.year,
    plot_obs.plot_id,
    tree_obs.vita_id,
    st_x(plot.the_geom) AS st_x,
    st_y(plot.the_geom) AS st_y,
    plot.sealevel,
    plot_obs.municipality_id,
    gemeindegrenzen_bsbl.gemeindename
   FROM tree_obs
     LEFT JOIN plot_obs ON plot_obs.id = tree_obs.obs_id
     LEFT JOIN plot ON plot_obs.plot_id = plot.id
     LEFT JOIN gemeindegrenzen_bsbl ON plot_obs.municipality_id = gemeindegrenzen_bsbl.gid
     LEFT JOIN tree ON tree_obs.tree_id = tree.id
     LEFT JOIN lt_vita ON tree_obs.vita_id = lt_vita.id
     LEFT JOIN tree_spec ON tree.spec_id = tree_spec.id
  WHERE tree_obs.dbh >= 80 AND lt_vita.code::text = 'l'::text
  ORDER BY tree_obs.dbh DESC;
