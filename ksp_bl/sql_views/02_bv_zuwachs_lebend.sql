CREATE OR REPLACE VIEW public.bv_zuwachs_lebend AS
 SELECT bv_a.id,
    bv_a.tree_id,
    bv_a.obs_id,
    inv0.ordering AS invent0,
    inv1.ordering AS invent1,
    bv_b.dbh AS dbh0,
    bv_a.dbh AS dbh1,
    bv_b.volume AS vol0,
    bv_a.volume AS vol1,
    bv_b.surface AS grundf0,
    bv_a.surface AS grundf1,
    bv_a.dbh - bv_b.dbh AS dbh_diff,
    poa.year - pob.year AS year_diff,
    bv_a.surface - bv_b.surface AS grundf_diff,
    GREATEST(0::double precision, (bv_a.volume - bv_b.volume)::double precision / NULLIF(poa.year - pob.year, 0)::double precision) AS growth_year,
    GREATEST(0::double precision, (bv_a.volume_wsl - bv_b.volume_wsl)::double precision / NULLIF(poa.year - pob.year, 0)::double precision) AS growth_year_wsl
   FROM public.bv_grundf_vol_pro_treeobs bv_a
     JOIN public.bv_grundf_vol_pro_treeobs bv_b ON bv_a.tree_id = bv_b.tree_id
     LEFT JOIN plot_obs poa ON bv_a.obs_id = poa.id
     LEFT JOIN inventory inv1 ON poa.inv_team_id = inv1.id
     LEFT JOIN plot_obs pob ON bv_b.obs_id = pob.id
     LEFT JOIN inventory inv0 ON pob.inv_team_id = inv0.id
     LEFT JOIN lt_vita vitaa ON bv_a.vita_id = vitaa.id
     LEFT JOIN lt_vita vitab ON bv_b.vita_id = vitab.id
  WHERE vitaa.code = 'l' AND (vitab.code = ANY (ARRAY['e', 'l'])) AND inv1.ordering = (inv0.ordering + 1);
