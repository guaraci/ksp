CREATE MATERIALIZED VIEW IF NOT EXISTS public.gemeinden_with_flaeche AS
    SELECT gem.*, ksp_waldf_from_bestand(gem.gid) AS waldflaeche
    FROM gemeindegrenzen_bsbl gem;
