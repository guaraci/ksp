CREATE OR REPLACE VIEW bv_grundf_vol_yx_pro_plotobs_und_allspec AS
 SELECT (sub2.plot_obs_id || '-'::text) || sub2.tree_spec_id AS id,
    sub2.plot_id,
    sub2.plot_obs_id,
    sub2.year,
    sub2.tree_spec_id,
    sub2.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(sub2.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha",
    COALESCE(subq."Volumen m3 (WSL)", 0) AS "Volumen m3 (WSL)",
    COALESCE(subq."Volumen m3 (WSL)" * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Volumen pro ha (WSL)"
   FROM ( SELECT bv.obs_id,
            bv.spec_id,
            count(*) AS "Anzahl Probebaeume",
            sum(bv.volume) AS "Volumen m3",
            sum(bv.volume_wsl) AS "Volumen m3 (WSL)",
            sum(bv.surface) AS "Grundflaeche m2"
           FROM public.bv_grundf_vol_pro_treeobs bv
          WHERE (bv.vita_id = 2 OR bv.vita_id = 3) AND bv.dbh > 11
          GROUP BY bv.obs_id, bv.spec_id) subq
     RIGHT JOIN ( SELECT plot_obs.id AS plot_obs_id,
            plot_obs.plot_id,
            plot_obs.year,
            plot_obs.forest_edgef,
            plot_obs.municipality_id,
            tree_spec.id AS tree_spec_id
           FROM plot_obs,
            tree_spec) sub2 ON sub2.plot_obs_id = subq.obs_id AND sub2.tree_spec_id = subq.spec_id;
