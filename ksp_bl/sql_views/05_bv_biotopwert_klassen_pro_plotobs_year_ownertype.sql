CREATE OR REPLACE VIEW bv_biotopwert_klassen_pro_plotobs_year_ownertype AS
 SELECT b.id,
    b.municipality_id,
    b.plot_id,
    b.year,
    b.owner_type_id,
    b."BIOLFI1M",
        CASE
            WHEN b."BIOLFI1M" > 2.6::double precision THEN 4
            WHEN b."BIOLFI1M" > 2.0::double precision AND b."BIOLFI1M" <= 2.6::double precision THEN 3
            WHEN b."BIOLFI1M" > 1.6::double precision AND b."BIOLFI1M" <= 2.0::double precision THEN 2
            WHEN b."BIOLFI1M" <= 1.6::double precision THEN 1
            ELSE 1000
        END AS "Biotopwert Klassen"
   FROM bv_biotopwert_bwnaturn_bwarten_bwstru1m b;
