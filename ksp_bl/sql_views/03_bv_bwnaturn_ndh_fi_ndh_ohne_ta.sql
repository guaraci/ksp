CREATE OR REPLACE VIEW bv_bwnaturn_ndh_fi_ndh_ohne_ta AS
 SELECT b.plot_obs_id,
    b.plot_id,
    e.code,
        CASE
            WHEN b."% Basalflächenanteil der Fichte" > 75::double precision AND e."BWNATURN_WSL" <= 3 THEN 1
            WHEN b."% Basalflächenanteil Nadelholz" > 75::double precision AND b."% Basalflächenanteil der Fichte" < 75::double precision AND (e."BWNATURN_WSL" = 2 OR e."BWNATURN_WSL" = 3) THEN 2
            WHEN b."% Basalflächenanteil Nadelholz ohne Tanne" > 75::double precision AND b."% Basalflächenanteil der Fichte" < 75::double precision AND e."BWNATURN_WSL" = 1 THEN 2
            WHEN b."% Basalflächenanteil Nadelholz" <= 75::double precision AND b."% Basalflächenanteil Nadelholz" > 10::double precision AND e."BWNATURN_WSL" = 2 THEN 3
            WHEN b."% Basalflächenanteil Nadelholz" <= 75::double precision AND b."% Basalflächenanteil Nadelholz" > 25::double precision AND e."BWNATURN_WSL" = 3 THEN 3
            WHEN b."% Basalflächenanteil Nadelholz ohne Tanne" <= 75::double precision AND b."% Basalflächenanteil Nadelholz ohne Tanne" > 25::double precision AND e."BWNATURN_WSL" = 1 THEN 3
            WHEN b."% Basalflächenanteil Nadelholz" <= 10::double precision AND e."BWNATURN_WSL" = 2 THEN 4
            WHEN b."% Basalflächenanteil Nadelholz" <= 25::double precision AND e."BWNATURN_WSL" = 3 THEN 4
            WHEN b."% Basalflächenanteil Nadelholz ohne Tanne" <= 25::double precision AND e."BWNATURN_WSL" = 1 THEN 4
            WHEN e."BWNATURN_WSL" = 4 THEN 4
            ELSE NULL::integer
        END AS "BWNATURN",
    b."% Basalflächenanteil der Fichte",
    b."% Basalflächenanteil Nadelholz",
    b."% Basalflächenanteil Nadelholz ohne Tanne"
   FROM bv_basalflaechenanteil_fi_ndh b
     JOIN plot_obs c ON b.plot_obs_id = c.id
     JOIN plot d ON d.id = c.plot_id
     LEFT JOIN phytosoc e ON e.id = d.phytosoc_id;
