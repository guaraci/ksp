CREATE OR REPLACE VIEW public.bv_eschentriebsterben AS
 SELECT
    bv.id,
    bv.obs_id,
    bv.volume AS volumen_m3,
    ash_dieback_id,
    bv.volume_wsl AS volumen_m3_wsl
   FROM public.bv_grundf_vol_pro_treeobs bv
     LEFT JOIN tree_obs ON bv.id = tree_obs.id
     LEFT JOIN tree ON bv.tree_id = tree.id
     LEFT JOIN tree_spec ON tree.spec_id = tree_spec.id
  WHERE tree_spec.species = 'Esche' AND tree_obs.ash_dieback_id IS NOT NULL;
