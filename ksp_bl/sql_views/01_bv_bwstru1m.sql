CREATE OR REPLACE VIEW bv_bwstru1m AS
 SELECT base.id,
    base.stand_devel_stage_id,
    base.stand_crown_closure_id,
    base.stand_structure_id,
    base.bwstru1_ds + base.bwstru1_cc + COALESCE(base.bwstru1_ss::integer,
        CASE
            WHEN base.ds_low THEN 1
            WHEN base.ds_middle AND base.cc_high THEN 3
            WHEN base.ds_middle AND base.cc_low THEN 1
            WHEN base.ds_high THEN 5
            ELSE NULL::integer
        END) AS "BWSTRU1M"
   FROM ( SELECT po.id,
            po.stand_devel_stage_id,
            po.stand_crown_closure_id,
            po.stand_structure_id,
            ds.code AS ds_code,
            COALESCE(ds.bwstru1_val, ds2015.bwstru1_val) AS bwstru1_ds,
            COALESCE(cc.bwstru1_val, cc2015.bwstru1_val) AS bwstru1_cc,
            ss.bwstru1_val AS bwstru1_ss,
            (ds.code::text = ANY (ARRAY['1'::character varying, '13'::character varying, '14'::character varying]::text[])) OR (ds2015.code = ANY (ARRAY[1, 3, 4])) AS ds_low,
            (ds.code::text = ANY (ARRAY['15'::character varying, '16'::character varying, '17'::character varying]::text[])) OR (ds2015.code = ANY (ARRAY[5, 6, 7])) AS ds_middle,
            ds.code::text = '18'::text OR ds2015.code = 8 AS ds_high,
            (cc.code = ANY (ARRAY[5, 6])) OR (cc2015.code = ANY (ARRAY[3, 4, 6])) AS cc_high,
            (cc.code = ANY (ARRAY[1, 2])) OR (cc2015.code = ANY (ARRAY[1, 2, 5])) AS cc_low
           FROM plot_obs po
             LEFT JOIN lt_devel_stage ds ON po.stand_devel_stage_id = ds.id
             LEFT JOIN lt_devel_stage_2015 ds2015 ON po.stand_devel_stage_id = ds2015.id
             LEFT JOIN lt_crown_closure cc ON po.stand_crown_closure_id = cc.id
             LEFT JOIN lt_crown_closure_2015 cc2015 ON po.stand_crown_closure_id = cc2015.id
             LEFT JOIN lt_stand_struct ss ON po.stand_structure_id = ss.id) base;
