CREATE OR REPLACE VIEW public.bv_latest_inventory AS 
 SELECT DISTINCT ON (inventory.municipality_id) inventory.id,
    inventory.municipality_id,
    inventory.inv_from,
    inventory.inv_to
   FROM inventory
  WHERE inventory.inv_to < now()
  ORDER BY inventory.municipality_id, inventory.inv_from DESC;
