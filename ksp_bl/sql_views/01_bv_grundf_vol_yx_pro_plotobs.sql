CREATE OR REPLACE VIEW bv_grundf_vol_yx_pro_plotobs AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(plot_obs.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha",
    plot_obs.forest_edgef,
    inventory.period AS inv_period,
    inventory.municipality_id AS inv_municipality_id,
    plot_obs.density,
    COALESCE(subq."Volumen m3 (WSL)" * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Volumen pro ha (WSL)"
   FROM ( SELECT bv.obs_id,
            count(*) AS "Anzahl Probebaeume",
            sum(bv.volume) AS "Volumen m3",
            sum(bv.volume_wsl) AS "Volumen m3 (WSL)",
            sum(bv.surface) AS "Grundflaeche m2"
           FROM public.bv_grundf_vol_pro_treeobs bv
          WHERE (bv.vita_id = 2 OR bv.vita_id = 3) AND bv.dbh > 11
          GROUP BY bv.obs_id
        ) subq
     RIGHT JOIN plot_obs ON subq.obs_id = plot_obs.id
     LEFT JOIN inventory ON plot_obs.inv_team_id = inventory.id;
