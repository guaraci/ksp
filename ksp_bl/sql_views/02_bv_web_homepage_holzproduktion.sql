CREATE OR REPLACE VIEW public.bv_web_homepage_holzproduktion
 AS
 SELECT row_number() OVER () AS id,
    d.gemeindename,
    b.inv_period,
    string_agg(DISTINCT b.year::text, '-'::text) AS year,
    sum(b."Anzahl Probebaeume")::integer AS "Anzahl Probebaeume",
    avg(b."Stammzahl pro ha")::integer AS "Stammzahl pro ha",
    stddev(b."Stammzahl pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Stammzahl pro ha")::double precision * 100::double precision AS "% Standardfehler",
    avg(b."Volumen pro ha")::numeric(5,1) AS "Volumen pro ha",
    stddev(b."Volumen pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Volumen pro ha")::double precision * 100::double precision AS "% Standardfehler2",
    avg(b."Volumen pro ha (WSL)")::numeric(5,1) AS "Volumen pro ha (WSL)",
    avg(b."Grundflaeche pro ha")::numeric(5,1) AS "Grundflaeche pro ha",
    stddev(b."Grundflaeche pro ha") / sqrt(count(*)::double precision) / avg(b."Grundflaeche pro ha") * 100::double precision AS "% Standardfehler3",
    sum(b.forest_edgef * b.density / 10000)::numeric(5,1) AS "theoretische Waldfläche ha",
    count(b.inv_period) AS "Anzahl Probepunkte",
    d.waldflaeche as waldflaeche_bestand
   FROM bv_grundf_vol_yx_pro_plotobs b
     FULL JOIN gemeinden_with_flaeche d ON b.inv_municipality_id = d.gid
  GROUP BY d.gemeindename, d.waldflaeche, b.inv_period
  ORDER BY d.gemeindename, b.inv_period;
