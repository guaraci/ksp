CREATE OR REPLACE VIEW public.bv_zuwachs_einwuchs AS 
 SELECT tobs.id,
    tobs.tree_id,
    tobs.obs_id,
    bv_secondobs.invent0,
    bv_secondobs.invent1,
    tobs.dbh,
    tobs.dbh - 11 AS dbh_diff,
    bv_secondobs.year_diff,
    GREATEST(0::double precision, (ksp_tarif_volume(tobs.dbh) - ksp_tarif_volume(11::smallint))::double precision / (bv_secondobs.year_diff::double precision / 2.0::double precision)) AS growth_year,
    GREATEST(0::double precision, (ksp_volume_tarif_wsl_bl_2020(tobs.dbh, plot.sealevel, tree_spec.tarif_klass20, phytosoc.tarif_klass20) - ksp_volume_tarif_wsl_bl_2020(11::smallint, plot.sealevel, tree_spec.tarif_klass20, phytosoc.tarif_klass20))::double precision / (bv_secondobs.year_diff::double precision / 2.0::double precision)) AS growth_year_wsl
   FROM tree_obs tobs
     JOIN tree ON tobs.tree_id = tree.id
     JOIN tree_spec ON tree.spec_id = tree_spec.id
     LEFT JOIN plot_obs po ON tobs.obs_id = po.id
     LEFT JOIN plot ON po.plot_id = plot.id
     LEFT JOIN phytosoc ON plot.phytosoc_id = phytosoc.id
     LEFT JOIN bv_secondobs ON po.id=bv_secondobs.id
     LEFT JOIN lt_vita vita ON tobs.vita_id = vita.id
  WHERE vita.code = 'e';
