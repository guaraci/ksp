CREATE OR REPLACE VIEW public.bv_bodenschaden AS
 SELECT po.id,
    po.plot_id,
    po.year,
    po.municipality_id,
    po.soil_compaction_id,
    (sc.code = 0)::integer AS code0,
    (sc.code = 1)::integer AS code1,
    (sc.code = 2)::integer AS code2
   FROM plot_obs po
     JOIN lt_soil_compaction sc ON po.soil_compaction_id = sc.id;
