CREATE OR REPLACE VIEW bv_bwarten_anz_baumarten_special_species AS
 SELECT c.plot_obs_id,
    (
        CASE
            WHEN count(*) <= 1 THEN 1::bigint
            WHEN count(*) >= 5 THEN 5::bigint
            ELSE count(*)
        END + max(
        CASE
            WHEN c.is_special = true THEN 2
            ELSE 0
        END))::integer AS "BWARTEN",
    count(*) AS "Anzahl Baumarten",
    max(
        CASE
            WHEN c.is_special = true THEN 2
            ELSE 0
        END) AS "Special Species"
   FROM ( SELECT a.plot_obs_id,
            b.is_special,
            a.year,
            a.tree_spec_id,
            a."Anzahl Probebaeume"
           FROM bv_grundf_vol_yx_pro_plotobs_und_allspec a
             JOIN tree_spec b ON a.tree_spec_id = b.id
          WHERE a."Anzahl Probebaeume" > 0) c
  GROUP BY c.plot_obs_id;
