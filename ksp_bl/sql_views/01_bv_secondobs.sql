CREATE OR REPLACE VIEW public.bv_secondobs AS 
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.year,
    plot_obs.municipality_id,
    plot_obs.forest_edgef,
    plot_obs.inv_team_id,
    inventory.ordering as invent1,
    prev_obs.ordering as invent0,
    plot_obs.year - prev_obs.year AS year_diff
   FROM plot_obs
     LEFT JOIN inventory ON plot_obs.inv_team_id = inventory.id
     LEFT JOIN ( SELECT pobs2.id,
            pobs2.plot_id,
            pobs2.year,
            inventory_1.ordering
           FROM plot_obs pobs2
             LEFT JOIN inventory inventory_1 ON pobs2.inv_team_id = inventory_1.id
        ) prev_obs ON plot_obs.plot_id = prev_obs.plot_id AND inventory.ordering = (prev_obs.ordering + 1)
  WHERE prev_obs.id IS NOT NULL;
