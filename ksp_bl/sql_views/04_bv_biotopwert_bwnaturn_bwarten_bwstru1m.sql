CREATE OR REPLACE VIEW bv_biotopwert_bwnaturn_bwarten_bwstru1m AS
 SELECT d.id,
    d.plot_id,
    d.municipality_id,
    d.year,
    d.owner_type_id,
    b."BWNATURN"::double precision / 4::double precision + a."BWARTEN"::double precision / 7::double precision + 2::double precision * (c."BWSTRU1M"::double precision / 18::double precision) AS "BIOLFI1M",
    a."BWARTEN",
    b."BWNATURN",
    c."BWSTRU1M"
   FROM bv_bwarten_anz_baumarten_special_species a
     JOIN bv_bwnaturn_ndh_fi_ndh_ohne_ta b ON a.plot_obs_id = b.plot_obs_id
     JOIN bv_bwstru1m c ON c.id = a.plot_obs_id
     JOIN plot_obs d ON d.id = a.plot_obs_id
  WHERE c."BWSTRU1M" IS NOT NULL;
