CREATE OR REPLACE VIEW public.bv_zuwachs_totgenutzt_pro_plotobs AS 
 SELECT leb_zuwachs.id,
    leb_zuwachs.plot_id,
    stem_vita.leb_perc,
    stem_vita.nutzung + stem_vita.tot AS mor_stem,
        CASE
            WHEN stem_vita.leb_perc < 0.5::double precision AND stem_vita.lebend < 7 THEN 0::double precision
            WHEN (stem_vita.nutzung + stem_vita.tot) = 0 THEN 0::double precision
            ELSE (stem_vita.nutzung + stem_vita.tot)::double precision * leb_zuwachs.growth_stem_ha / 2.0::double precision
        END AS mor_growth_ha
   FROM bv_zuwachs_lebend_pro_plotobs leb_zuwachs
     LEFT JOIN ( SELECT vi.id,
            vi.lebend,
            vi.nutzung,
            vi.tot,
                CASE
                    WHEN vi.lebend = 0 THEN 0.0::double precision
                    ELSE vi.lebend::double precision / (vi.lebend + vi.nutzung + vi.tot)::double precision
                END AS leb_perc
           FROM bv_stem_pro_plotobs_pro_vita vi) stem_vita ON leb_zuwachs.id = stem_vita.id;
