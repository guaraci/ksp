CREATE MATERIALIZED VIEW IF NOT EXISTS public.plot_in_region AS
 SELECT row_number() OVER ()::integer AS id,
    plot.id AS plot_id,
    adminregion.id AS adminregion_id,
    adminregion.name AS region_name,
    regiontype.name AS region_type
   FROM plot,
    adminregion
     LEFT JOIN regiontype ON adminregion.region_type_id = regiontype.id
  WHERE st_within(plot.the_geom, adminregion.geom)
WITH DATA;
