CREATE OR REPLACE VIEW public.bv_grundf_vol_yx_pro_plotobs_und_specgroup_nutzung
 AS
 SELECT (sub2.plot_obs_id || '-'::text) || sub2.tree_group_id AS id,
    sub2.plot_id,
    sub2.plot_obs_id,
    sub2.year,
    sub2.tree_group_id,
    subq.name,
    sub2.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0::bigint) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0::numeric) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0::double precision) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(sub2.forest_edgef), 0::numeric) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(sub2.forest_edgef), 0::numeric) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(sub2.forest_edgef)::double precision, 0::double precision) AS "Grundflaeche pro ha",
    COALESCE(subq."Volumen m3 (WSL)", 0) AS "Volumen m3 (WSL)",
    COALESCE(subq."Volumen m3 (WSL)" * ksp_lokale_dichte(sub2.forest_edgef), 0) AS "Volumen pro ha (WSL)"
   FROM ( SELECT tobsb.obs_id,
            bv.group_id,
            tree_group.name,
            count(*) AS "Anzahl Probebaeume",
            sum(bv.volume) AS "Volumen m3",
            sum(bv.volume_wsl) AS "Volumen m3 (WSL)",
            sum(bv.surface) AS "Grundflaeche m2",
            max(poa.year) - max(pob.year) AS year_diff
           FROM public.bv_grundf_vol_pro_treeobs bv
             LEFT JOIN tree_group ON bv.group_id = tree_group.id
             JOIN tree_obs tobsb ON bv.tree_id = tobsb.tree_id
             LEFT JOIN plot_obs poa ON bv.obs_id = poa.id
             LEFT JOIN inventory inv1 ON poa.inv_team_id = inv1.id
             LEFT JOIN plot_obs pob ON tobsb.obs_id = pob.id
             LEFT JOIN inventory inv0 ON pob.inv_team_id = inv0.id
         WHERE tobsb.vita_id = 1 AND inv1.ordering = (inv0.ordering - 1)
          GROUP BY tobsb.obs_id, bv.group_id, tree_group.name) subq
     RIGHT JOIN ( SELECT plot_obs.id AS plot_obs_id,
            plot_obs.plot_id,
            plot_obs.year,
            plot_obs.forest_edgef,
            plot_obs.municipality_id,
            tree_group.id AS tree_group_id
           FROM plot_obs,
            tree_group) sub2 ON sub2.plot_obs_id = subq.obs_id AND sub2.tree_group_id = subq.group_id;
