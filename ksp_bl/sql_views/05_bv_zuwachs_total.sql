CREATE OR REPLACE VIEW public.bv_zuwachs_total AS 
 SELECT leb_zuwachs.id,
    leb_zuwachs.plot_id,
    leb_zuwachs.nb_stem AS leb_stem,
    tot_zuwachs.leb_perc,
    leb_zuwachs.growth_ha AS leb_growth_ha,
    ein_zuwachs.nb_stem AS ein_stem,
    ein_zuwachs.growth_ha AS ein_growth_ha,
    tot_zuwachs.mor_stem,
    tot_zuwachs.mor_growth_ha,
    leb_zuwachs.growth_ha + ein_zuwachs.growth_ha + COALESCE(tot_zuwachs.mor_growth_ha, 0::double precision) AS growth_ha_total
   FROM bv_zuwachs_lebend_pro_plotobs leb_zuwachs
     LEFT JOIN bv_zuwachs_einwuchs_pro_plotobs ein_zuwachs ON leb_zuwachs.id = ein_zuwachs.id
     LEFT JOIN bv_zuwachs_totgenutzt_pro_plotobs tot_zuwachs ON leb_zuwachs.id = tot_zuwachs.id;
