import json
from functools import wraps

from django.contrib.gis.db.models.functions import Transform
from django.db import DatabaseError, connections
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from ksp_bl.models import Inventory

from .models import ErschliessungLaufend, WaldBestandLaufend


def check_afw_present(view_func):
    @wraps(view_func)
    def inner(*args, **kwargs):
        try:
            connections['afw'].ensure_connection()
        except DatabaseError:
            return JsonResponse({"type": "FeatureCollection", "features": []})
        return view_func(*args, **kwargs)
    return inner


@check_afw_present
def waldbestand(request, inventory_pk):
    inventory = get_object_or_404(Inventory, pk=inventory_pk)
    query = WaldBestandLaufend.objects.filter(geom__intersects=inventory.geom
        ).annotate(geom_wgs84=Transform('geom', 4326)
        ).select_related('entwicklungstufe', 'mischungsgrad', 'schlussgrad')
    # Transform to GeoJSON
    geojson = {"type": "FeatureCollection", "features": []}
    for bestand in query:
        geojson["features"].append({
            "type": "Feature",
            "id": bestand.pk,
            "properties": {
                "id": bestand.pk,
                "popupContent": "Entwicklungstufe: %s<br>Mischungsgrad: %s<br>Schlussgrad: %s" % (
                    bestand.entwicklungstufe, bestand.mischungsgrad, bestand.schlussgrad)
            },
            "geometry": json.loads(bestand.geom_wgs84.json),
        })
    return JsonResponse(geojson)


@check_afw_present
def erschliessung(request, inventory_pk):
    inventory = get_object_or_404(Inventory, pk=inventory_pk)
    query = ErschliessungLaufend.objects.filter(geom__intersects=inventory.geom
        ).annotate(geom_wgs84=Transform('geom', 4326))
    # Transform to GeoJSON
    geojson = {"type": "FeatureCollection", "features": []}
    for item in query:
        geojson["features"].append({
            "type": "Feature",
            "id": item.pk,
            "properties": {
                "id": item.pk,
            },
            "geometry": json.loads(item.geom_wgs84.json),
        })
    return JsonResponse(geojson)
