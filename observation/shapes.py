from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import Polygon, MultiPolygon


class ShapeImporter:
    def __init__(self, path, encoding="utf-8", multi=None):
        """Possible values for `multi`:
            - None: return the geometry as presented by the layer
            - True: force Polygons to Multipolygons
            - False: raise an error if the layer return Multipolygons
        """
        self.path = path
        self.multi = multi
        ds = DataSource(path, encoding=encoding)
        self.layer = ds[0]

    def __iter__(self):
        self._layer_iter = iter(self.layer)
        return self

    def __next__(self):
        feature = next(self._layer_iter)
        geom = feature.geom.geos
        if self.multi is True and isinstance(geom, Polygon):
            geom = MultiPolygon([geom])
        elif self.multi is False and isinstance(geom, MultiPolygon):
            raise ValueError(
                f"This feature geometry is MultiPolygon ({len(geom)}) while the importer only allows Polygons"
            )
        return feature, geom

    def fields(self):
        return dict(zip(imp.layer.fields, [ft.__name__ for ft in imp.layer.field_types]))
