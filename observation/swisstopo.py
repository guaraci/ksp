import json
from urllib import request
from urllib.error import HTTPError, URLError


def query_elevation(point, raise_exc=False):
    assert point.srid == 2056
    req = request.Request(
        f'https://api3.geo.admin.ch/rest/services/height?easting={int(point.x)}&northing={int(point.y)}'
    )
    try:
        response = request.urlopen(req, timeout=20)
    except (HTTPError, URLError) as exc:
        if raise_exc:
            raise Exception(f"Error accessing Swisstopo Height service: ({exc})")
        else:
            return None
    result = json.loads(response.read().decode('utf-8'))
    return float(result['height'])
