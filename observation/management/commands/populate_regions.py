from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry, MultiPolygon
from django.core.management import BaseCommand, CommandError
from django.db import connections
from django.utils import timezone

from observation.models import AdminRegion, Owner, OwnerType2, RegionType


class Command(BaseCommand):
    """
    Populate the AdminRegion and Owner models with data from the afw database.
    """
    def add_arguments(self, parser):
        parser.add_argument("--region",
            choices=['forstkreis', 'forstrevier', 'jagdrevier', 'wep', 'eigentumer', 'schutzwald'],
            action='append',
            help="Choose the region(s) to import from the afw database.")

    def handle(self, *args, **options):
        if not 'afw' in settings.DATABASES:
            raise CommandError("The afw database is not available in settings.")
        self.cursor = connections['afw'].cursor()

        if 'forstkreis' in options['region'] or not options['region']:
            self.import_forstkreis()
        if 'forstrevier' in options['region'] or not options['region']:
            self.import_forstrevier()
        if 'jagdrevier' in options['region'] or not options['region']:
            self.import_jagdrevier()
        if 'wep' in options['region']:  # or not options['region']:
            raise CommandError("WEP are currently manually adapted in the KSP database")
            #self.import_wep()
        if 'eigentumer' in options['region'] or not options['region']:
            self.import_eigentumer()
        if 'schutzwald' in options['region'] or not options['region']:
            self.import_schutzwald()
        self.cursor.close()

        self.stdout.write("Refreshing materialized view plot_in_region...")
        cursor = connections['default'].cursor()
        cursor.execute('''REFRESH MATERIALIZED VIEW plot_in_region;''')
        cursor.close()

    def import_forstkreis(self):
        self.stdout.write("Importing Forstkreis objects...")
        self.cursor.execute(
            '''SELECT lut_code, name FROM "common"."lut_forstkreis" ORDER BY lut_code''')
        rtype, _ = RegionType.objects.get_or_create(name='Forstkreis')
        AdminRegion.objects.filter(region_type=rtype).delete()
        for row in self.cursor.fetchall():
            lut_code, name = row
            if lut_code < 1:
                continue
            fkreis, created = AdminRegion.objects.get_or_create(nr=lut_code, name=name, region_type=rtype)
            self.cursor.execute(
                '''SELECT ST_Multi(ST_Union(f.geom)) FROM admin.admin_einteilung_blbs AS f WHERE f.forstkreis=%s GROUP BY f.forstkreis''',
                [lut_code]
            )
            geom = GEOSGeometry(self.cursor.fetchone()[0])
            fkreis.geom = geom
            fkreis.save()

    def import_forstrevier(self):
        self.stdout.write("Importing Forstrevier objects...")
        self.cursor.execute(
            '''SELECT lut_code, name, nr FROM "common"."lut_forstrevier" ORDER BY lut_code''')
        rtype, _ = RegionType.objects.get_or_create(name='Forstrevier')
        AdminRegion.objects.filter(region_type=rtype).delete()
        for row in self.cursor.fetchall():
            lut_code, name, nr = row
            if lut_code < 1:
                continue
            frevier, created = AdminRegion.objects.get_or_create(nr=nr or '', name=name, region_type=rtype)
            self.cursor.execute(
                '''SELECT ST_Multi(ST_Union(f.geom)) FROM admin.admin_einteilung_blbs AS f WHERE f.forstrevier=%s GROUP BY f.forstrevier''',
                [lut_code]
            )
            result = self.cursor.fetchone()
            if result:
                geom = GEOSGeometry(result[0])
                frevier.geom = geom
                frevier.save()

    def import_jagdrevier(self):
        self.stdout.write("Importing Jagdrevier objects...")
        self.cursor.execute(
            '''SELECT lut_code, name FROM "common"."lut_jagdrevier" ORDER BY lut_code''')
        rtype, _ = RegionType.objects.get_or_create(name='Jagdrevier')
        AdminRegion.objects.filter(region_type=rtype).delete()
        for row in self.cursor.fetchall():
            lut_code, name = row
            if lut_code < 1:
                continue
            jrevier, created = AdminRegion.objects.get_or_create(nr=lut_code, name=name, region_type=rtype)
            self.cursor.execute(
                '''SELECT ST_Multi(ST_Union(f.geom)) FROM admin.admin_einteilung_blbs AS f WHERE f.jagdrevier=%s GROUP BY f.jagdrevier''',
                [lut_code]
            )
            geom = GEOSGeometry(self.cursor.fetchone()[0])
            jrevier.geom = geom
            jrevier.save()

    def import_wep(self):
        self.stdout.write("Importing WEP objects...")
        self.cursor.execute(
            '''SELECT lut_code, name, array_agg(gemeinde_id_bfs) AS gemeinde_id_list
               FROM common.lut_wep_name AS wn
               LEFT JOIN common.lut_wep_perimeter AS wp ON (wn.lut_code=wp.wep_id) WHERE wn.lut_code>0
               GROUP BY lut_code, name ORDER BY wn.lut_code;''')
        rtype, _ = RegionType.objects.get_or_create(name='WEP')
        AdminRegion.objects.filter(region_type=rtype).delete()
        for lut_code, name, gemeinden in self.cursor.fetchall():
            wep = AdminRegion.objects.create(nr='wep%s' % lut_code, name=name, region_type=rtype)
            self.cursor.execute(
                '''SELECT ST_Multi(ST_Union(f.geom)) FROM  admin.admin_einteilung_blbs AS f
                   WHERE f.gemeinde_id_bfs IN %s''',
                [tuple(gemeinden)]
            )
            geom = GEOSGeometry(self.cursor.fetchone()[0])
            wep.geom = geom
            wep.save()

    def import_eigentumer(self):
        self.stdout.write("Importing Eigentümer objects...")
        self.cursor.execute(
            '''SELECT min(id) AS nr, min(trim(concat_ws(' ', akt_name, akt_vorname))) AS name,
                      lower(trim(concat_ws(' ', akt_name, akt_vorname))) AS name_lower,
                      eg_kategorie,
                      ST_Multi(ST_Union(f.geom)) AS geom
               FROM fp.v_waldeigentum_detail_201210 f
               GROUP BY name_lower, eg_kategorie''',
            []
        )
        rtype, _ = RegionType.objects.get_or_create(name='Eigentümer')
        AdminRegion.objects.filter(region_type=rtype).delete()
        Owner.objects.all().update(updated=None)
        private_geom = MultiPolygon()
        # We cannot delete existing owners as they are linked from PlotObs.
        for nr, eigen, _, kateg, geom in self.cursor.fetchall():
            geom = GEOSGeometry(geom)
            eigen = eigen.strip()
            if geom.area < 1:
                continue
            if kateg == 'Privat' and ((geom.area / 10000) < 25 or eigen == ''):
                # Private owners with less than 25 ha and unnamed owners are all grouped in the "Private" owner
                private_geom.extend(geom)
                continue
            try:
                owner = Owner.objects.get(name__iexact=eigen)
            except Owner.DoesNotExist:
                owner = Owner.objects.create(
                    nr='', name=eigen, geom=geom,
                    otype=OwnerType2.objects.get(name='Privat') if kateg == 'Privat' else None,
                    updated=timezone.now()
                )
                print(f"New owner '{eigen}' ({kateg}) created")
            else:
                owner.geom = geom
                owner.updated = timezone.now()
                owner.save()
            AdminRegion.objects.create(nr='eigen%s' % owner.pk, name=eigen, region_type=rtype, geom=owner.geom)

        private_owner = Owner.objects.get(name="Private")
        private_owner.geom = private_geom
        private_owner.updated = timezone.now()
        private_owner.save()
        AdminRegion.objects.create(nr='eigen_priv', name="Private", region_type=rtype, geom=private_owner.geom)

    def import_schutzwald(self):
        from observation.models import GefahrPotential, Schutzwald

        self.stdout.write("Importing Schutzwald objects...")
        rtype, _ = RegionType.objects.get_or_create(name='Schutzwald')
        # Filling GefahrPotential (shouldn't change much...)
        self.cursor.execute(
            '''SELECT lut_code, name FROM common.lut_ng_gefahrenpotential'''
        )
        for lut_code, name in self.cursor.fetchall():
            GefahrPotential.objects.get_or_create(id=lut_code, name=name)
        gef_pots = dict((gp.pk, gp) for gp in GefahrPotential.objects.all())

        Schutzwald.objects.all().delete()
        AdminRegion.objects.filter(region_type=rtype).delete()
        self.cursor.execute(
            '''SELECT wald.id, wald.name, wald.h_gef_pot, ST_Multi(wald.geom)
               FROM ng.schutzwald_laufend AS wald
               WHERE wald.geom IS NOT NULL AND wald.h_gef_pot IS NOT NULL''',
            []
        )
        for id_, name, gef_pot, geom in self.cursor.fetchall():
            wald = AdminRegion.objects.create(
                nr='sch%s' % id_, name=name, geom=GEOSGeometry(geom), region_type=rtype)
            swald = Schutzwald.objects.create(region=wald, haupt_gef_pot=gef_pots[gef_pot])
