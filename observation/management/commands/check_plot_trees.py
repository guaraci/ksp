import sys

from django.core.management import BaseCommand
from django.db.models import Count
from django.urls import reverse

from observation.models import Plot, UpdatedValue, Vita


class Command(BaseCommand):
    """
    Browse Plots and detect tree inconsistencies, and maybe fix some detectable things.
    """
    def add_arguments(self, parser):
        parser.add_argument('--interactive', action='store_true', default=False)
        parser.add_argument('--fix-trees', action='store_true', default=False)

    def handle(self, *args, **options):
        self.interactive = options['interactive']
        differents = 0
        for plot in Plot.objects.annotate(numobs=Count('plotobs')).filter(checked=False, numobs__gt=1):
            plotobss = list(plot.plotobs_set.all().order_by('year'))
            treeo_lists = [dict([(to.tree_id, to) for to in po.treeobs_set.select_related('tree')])
                           for po in plotobss]
            trees = set(plotobss[0].treeobs_set.select_related('tree', 'tree_spec', 'vita').values_list('tree', flat=True))
            identical = True
            # Compare -1/-2, -2/-3,...
            to_list = treeo_lists[-1]
            for idx in range(1, len(treeo_lists)):
                from_list = treeo_lists[-(idx + 1)]
                if set(from_list.keys()) != set(to_list.keys()):
                    same_keys = set(from_list.keys()).intersection(set(to_list.keys()))
                    only_in_1 = [to for k, to in from_list.items() if k not in same_keys]
                    only_in_2 = [to for k, to in to_list.items() if k not in same_keys]
                    if not only_in_1 and all([to.vita.code in ('e', 'm') if to.vita else False for to in only_in_2]):
                        # The second inventory has only additional new trees
                        continue
                    print("Trees are different for plot %s" % plot.pk)
                    identical = False
                    differents += 1
                    if options['fix_trees']:
                        changed = self.fix_trees(only_in_1, only_in_2)
                        if changed:
                            break  # Further changes could happen in a later run
                to_list = from_list
            if identical and not plot.checked:
                plot.checked = True
                plot.save()
        print(differents)

    def fix_trees(self, only_in_1, only_in_2):
        # same nr/azimuth/distance, different species
        paired = []
        changed = False
        for to in only_in_2:
            for from1 in only_in_1:
                # Same number, azimuth, distance, diameter same or greater
                if (from1.tree.nr == to.tree.nr and from1.tree.azimuth == to.tree.azimuth and
                        from1.tree.distance == to.tree.distance and from1.dbh <= to.dbh):
                    paired.append((from1, to))
                    break
                # Different number, but same species, azimuth, distance, diameter same or greater
                if (from1.tree.spec == to.tree.spec and from1.tree.azimuth == to.tree.azimuth and
                        from1.tree.distance == to.tree.distance and (from1.dbh <= to.dbh or to.dbh <= 0)):
                    paired.append((from1, to))
                    break
            # No pair found for only_in_2 item, maybe vita code is wrong
            if self.interactive and to.vita.code == 'l' and 10 <= to.dbh <= 20:
                if len(only_in_1) != 0:
                    print('http://ksp.guaraci.ch' + reverse('plot_detail', args=[to.obs.plot.pk]))
                    resp = input("Fix vita from 'l' to 'e' for %s?" % tree_repr(to))
                else:
                    resp = 'y'
                if resp == 'y':
                    to.vita = Vita.objects.get(code='e')
                    to.save()
                elif resp == 'q':
                    sys.exit(1)
        for pair in paired:
            if pair[0].tree.spec != pair[1].tree.spec and pair[1].tree.spec.species == "Totholz":
                new_spec = pair[0].tree.spec
            else:
                new_spec = None
            if self.interactive:
                resp = input("%s\n%s\nAre above trees the same?%s [y|n|q] " % (
                    tree_repr(pair[0]), tree_repr(pair[1]), ' (new spec: %s)' % new_spec if new_spec else '')
                )
            else:
                resp = 'y'
            if resp == 'y':
                changed = True
                old_tree = pair[0].tree
                pair[0].tree = pair[1].tree
                pair[0].save()
                msg = "Changed tree/nr/species from %d %s to %d %s" % (
                    old_tree.nr, old_tree.spec, pair[1].tree.nr, pair[1].tree.spec)
                UpdatedValue.add(pair[0], 'tree_id', old_tree.pk, msg)
                self.stdout.write(msg)
                if new_spec:
                    tree = pair[1].tree
                    old_spec = tree.spec
                    tree.spec = new_spec
                    tree.save()
                    msg = "Changed tree species from %s to %s" % (old_spec, new_spec)
                    UpdatedValue.add(tree, 'spec_id', old_spec.pk, msg)
                    self.stdout.write(msg)
                if old_tree.treeobs_set.count() == 0:
                    old_tree.delete()
            elif resp == 'q':
                sys.exit(1)
        return changed


def tree_repr(tree_obs):
    return "%s: <%s. %s (%s gon/%s dm) / ⌀%s- %s>" % (
        tree_obs.obs.year, tree_obs.tree.nr, tree_obs.tree.spec, tree_obs.tree.azimuth,
        tree_obs.tree.distance, tree_obs.dbh, tree_obs.vita.code,
    )
