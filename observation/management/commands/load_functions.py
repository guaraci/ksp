from pathlib import Path

from django.apps import apps
from django.conf import settings
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        sql_dir = Path(apps.get_app_config(settings.MAIN_APP).path) / 'sql_functions'
        connection = connections['default']
        for path in sorted(sql_dir.glob('*.sql')):
            with path.open('r') as fh:
                sql_code = fh.read()
                self.stdout.write(f"Running {path}")
                with connection.cursor() as cursor:
                    cursor.execute(sql_code)
        self.stdout.write(f"Done")
