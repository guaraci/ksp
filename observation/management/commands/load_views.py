from pathlib import Path

from django.apps import apps
from django.conf import settings
from django.core.management import BaseCommand
from django.db import connections


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--drop', action='store_true')
        
    def handle(self, *args, **options):
        app_config = apps.get_app_config(settings.MAIN_APP)
        readonly_group = getattr(app_config, 'read_only_psql_group', '')
        sql_dir = Path(app_config.path) / 'sql_views'
        paths = sorted(sql_dir.glob('*.sql'))
        connection = connections['default']

        if options['drop']:
            for path in reversed(paths):
                with path.open('r') as fh:
                    line1 = fh.readline()
                view_name = line1.split()[-1]
                if view_name.lower() == 'as':
                    view_name = line1.split()[-2]
                print(f'Dropping {view_name}')
                sql_code = f'DROP VIEW IF EXISTS {view_name};'
                with connection.cursor() as cursor:
                    cursor.execute(sql_code)

        for path in paths:
            with path.open('r') as fh:
                sql_code = fh.read()
                self.stdout.write(f"Running {path}")
                with connection.cursor() as cursor:
                    cursor.execute(sql_code)
                    if options['drop'] and readonly_group:
                        view_name = sql_code.split('\n')[0].split()[-1]
                        cursor.execute(
                            f'GRANT SELECT ON TABLE {view_name} TO {readonly_group};'
                        )
        self.stdout.write(f"Done")
