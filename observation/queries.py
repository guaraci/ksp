import importlib
import logging
import operator
from collections import OrderedDict
from dataclasses import dataclass
from functools import reduce
from itertools import chain

from django.conf import settings
from django.contrib.gis.db.models import Union
from django.core.exceptions import FieldDoesNotExist
from django.db.models import Avg, Count, Q, Value
from django.db.models.functions import Coalesce
from django.urls import reverse_lazy
from django.utils.translation import gettext as _

from .functions import StdErrAbs, StdErrRel
from .common_models import AdminRegion, Gemeinde, Owner, Plot, PlotObs, TreeSpecies
# database views:
from .models.db_views import Zuwachs, Giganten
from .models.db_views import LatestInventories

logger = logging.getLogger(__name__)
queries_module = importlib.import_module(f'{settings.MAIN_APP}.queries')


class PerimGroupBase:
    """A groupment of perimeter items to select a query perimeter."""
    def infos(self):
        return f'{self._infos} Klicken Sie für weitere Informationen.' if self.infos_url else self._infos

    def item_name(self, item):
        return item.name

    def pks_from_request(self, qdict):
        return [int(v) for v in qdict.getlist(self.short_name)]

    @property
    def is_adminregion(self):
        return getattr(self.items, 'model', None) == AdminRegion

    def __iter__(self):
        for item in self.items:
            yield {
                'name': self.item_name(item), 'pk': item.pk,
                'input_id': f'{self.short_name}{item.pk}', 'input_name': self.short_name
            }

    def filter_query(self, pks, **kwargs):
        if self.is_adminregion:
            filtre = Q(
                **{f'{kwargs["plot_prefix"]}{Plot.geom_field}__within':
                   AdminRegion.objects.filter(pk__in=pks).aggregate(geom=Union('geom'))['geom']}
            )
            reg_qs = AdminRegion.objects.filter(pk__in=pks).order_by('name')
            return filtre, ("Perimeter", ", ".join([reg.name for reg in reg_qs]))


@dataclass
class PerimGroup(PerimGroupBase):
    """A groupment of perimeter items to select a query perimeter."""
    title: str
    items: list  # generally a queryset
    _infos: str = ''
    infos_url: str = ''
    short_name: str = ''


@dataclass
class GemeindePerimGroup(PerimGroup):
    short_name: str = 'gem'

    def item_name(self, item):
        return item.name_with_k

    def filter_query(self, pks, **kwargs):
        filtre = Q(**{f'{kwargs["plot_obs_prefix"]}municipality__pk__in': pks})
        gem_qs = Gemeinde.objects.filter(pk__in=pks).order_by('name')
        return filtre, ("Gemeinden", ", ".join([gem.name for gem in gem_qs]))


class HistOwnerPerimGroup(PerimGroupBase):
    title = "Hist. Eigentümer"
    items = Owner.objects.all().order_by('name')
    short_name = 'eigentumer'
    infos_url = reverse_lazy('doc-specific', args=['eigentuemer'])

    def __init__(self, _infos):
        super().__init__()
        self._infos = _infos

    def filter_query(self, pks, **kwargs):
        filtre = Q(**{f'{kwargs["plot_obs_prefix"]}owner__pk__in': pks})
        owner_qs = self.items.filter(pk__in=pks)
        return filtre, ("Eigentümer", ", ".join(sorted([str(o) for o in owner_qs])))



class HeutOwnerPerimGroup(PerimGroupBase):
    title = "Heut. Eigentümer"
    items = Owner.objects.all().order_by('name')
    short_name = 'eigentumer-current'
    infos_url = reverse_lazy('doc-specific', args=['eigentuemer'])

    def __init__(self, _items=None, _infos=''):
        super().__init__()
        self._infos = _infos
        if _items:
            self.items = _items

    def filter_query(self, pks, **kwargs):
        filtre = Q(**{
            f'{kwargs["plot_prefix"]}{Plot.geom_field}__within':
            Owner.objects.filter(pk__in=pks).aggregate(geom=Union('geom'))['geom']
        })
        owner_qs = self.items.filter(pk__in=pks)
        return filtre, ("Eigentümer", ", ".join([o.name for o in owner_qs]))


class PerimeterSetBase:
    @classmethod
    def get_perimeters(cls):
        """Return a list of PerimGroup (or subclasses) instances."""
        raise NotImplementedError

    @classmethod
    def get_perimeters_from_request(cls, qdict):
        """
        Take request GET/POST params and map them to PerimGroup and related pks
        for each group.
        """
        return [gr for gr in [
            {'group': group, 'pks': group.pks_from_request(qdict)}
            for group in cls.get_perimeters()
        ] if gr['pks']]

    @classmethod
    def get_perimeter_from_key_value(cls, key, value):
        for pgroup in cls.get_perimeters():
            if key == pgroup.short_name:
                try:
                    return pgroup.items.get(pk=value)
                except ObjectDoesNotExist:
                    return None

    @classmethod
    def query_model_by_qdict(cls, base_qs, query_dict, **kwargs):
        """
        Return a tuple (
            <filtered queryset as of query_dict>,
            <human-readable string of chosen perimeters>
        ).
        """
        perim_groups = cls.get_perimeters_from_request(query_dict)
        filters = []
        perim_feedback = []
        # Group queries on AdminRegion
        ar_groups = [gr for gr in perim_groups if gr['group'].is_adminregion]
        if ar_groups:
            pks = list(chain(*[gr['pks'] for gr in ar_groups]))
            filtre, feedback = ar_groups[0]['group'].filter_query(pks, **kwargs)
            filters.append(filtre)
            perim_feedback.append(feedback)

        for group in perim_groups:
            if group['group'].is_adminregion:
                continue
            filtre, feedback = group['group'].filter_query(group['pks'], **kwargs)
            filters.append(filtre)
            perim_feedback.append(feedback)
        if not perim_feedback:
            raise ValueError(_("No selected perimeter"))
        return base_qs.filter(reduce(operator.or_, filters)), perim_feedback


@dataclass
class Field:
    name: str
    accessor: str
    fkey_target: str
    vname: str
    ftype: str = ''

    @classmethod
    def from_meta_field(cls, metaf):
        return cls(
            metaf.name,
            metaf.name,
            QueryData.get_fkey_target(metaf.name),
            metaf.verbose_name,
            metaf.get_internal_type()
        )


@dataclass
class Groupment:
    code: str
    label: str
    accessor: str = ''
    fkey_target: str = ''
    docs_title: str = ''
    docs_url: str = ''
    plot_prefix: bool = False
    checked: bool = False

    def apply_to(self, query_data, query):
        prefix = query_data.plot_prefix if self.plot_prefix else query_data.plot_obs
        aggr_crit = f'{prefix}{self.accessor or self.code}{self.fkey_target}'
        return aggr_crit, self.label, query


class GroupmentRegion(Groupment):
    """
    Groupement apply through the plotinregion materialized view.
    """
    @property
    def regiontype_name(self):
        return {
            'wep': 'WEP', 'eigentumer-geom': 'Eigentümer'
        }.get(self.code, self.code[0].upper() + self.code[1:])

    def apply_to(self, query_data, query):
        self.accessor = 'plotinregionview__region_name'
        self.plot_prefix = True
        crit, name, query = super().apply_to(query_data, query)
        query = query.filter(
            **{f'{query_data.plot_prefix}plotinregionview__region_type': self.regiontype_name}
        )
        return crit, name, query


class GroupmentSpec(Groupment):
    def apply_to(self, query_data, query):
        if not getattr(query_data, 'model_by_spec'):
            return
        specf = query_data.model_by_spec._meta.get_field('spec')
        query_data.coalesce_with_0 = True
        return f'{specf.name}__{TreeSpecies.name_field}', specf.verbose_name, query


class GroupmentSpecGroup(Groupment):
    def apply_to(self, query_data, query):
        if not getattr(query_data, 'model_by_spec_group'):
            return
        groupf = query_data.model_by_spec_group._meta.get_field('group')
        query_data.coalesce_with_0 = True
        return f'{groupf.name}__name', groupf.verbose_name, query


class QueryData:
    """
    'model': View class, 'descr': 'Verbose description',
    'model_by_spec': alternate model to use when grouping by species,
    'model_by_spec_group': alternate model to use when grouping by species group,
    'plot_obs': 'prefix to plot_obs.id',
    'annot_map' (optional): when an aggregation occurs, fields and corresponding annotation to use,
    'category': category name used to group queries on query page,
    """
    order_by = 'id'  # field on which to order
    plot_prefix = 'plot__'
    annot_map = {}
    # Mapping to get the 'interesting' field on a foreign key join
    fkey_map = {
        'stand_devel_stage': 'description',
        'devel_stage': 'description',
        'stand_crown_closure': 'description',
        'crown_closure': 'description',
        'stand_forest_mixture': 'description',
        'forest_mixture': 'description',
        'stand_structure': 'description',
        'municipality': 'name',
        'gemeinde': 'name',
        'plot': 'nr',
        'otype': 'name',
        'spec': TreeSpecies.name_field,
        'group': 'name',
        'vita': 'code',
        'ash_dieback': 'description',
    }

    def __init__(self, **kwargs):
        self.map_color_fields = []
        for key, val in kwargs.items():
            setattr(self, key, val)
        self.coalesce_with_0 = False

    def as_dict(self):
        return {key: getattr(self, key, '') for key in [
            'model', 'descr', 'plot_obs', 'plot_prefix', 'annot_map',
            'groupables', 'map_color_fields', 'order_by', 'category',
        ]}

    @classmethod
    def get_fkey_target(cls, fname):
        if fname in cls.fkey_map:
            return '__%s' % cls.fkey_map[fname]
        return ''

    def get_map_color_fields(self):
        return [
            (f.name, f.verbose_name) for f in self.model._meta.fields
            if f.name in self.map_color_fields
        ]

    def get_ordered_fields(self, model):
        return {
            f.name: Field.from_meta_field(f) for f in model._meta.fields
        }

    def get_view_model(self, aggrs):
        """Return a model class to base the query on, depending on passed `aggrs` values."""
        if aggrs and 'spec' in aggrs and hasattr(self, 'model_by_spec'):
            view_model = self.model_by_spec
        elif aggrs and 'group' in aggrs and hasattr(self, 'model_by_spec_group'):
            view_model = self.model_by_spec_group
        else:
            view_model = self.model
        return view_model

    def build_query(self, query_dict, aggrs, as_json=False, stddev=None):
        plot_obs_prefix = self.plot_obs
        if stddev is None:
            stddev = bool(query_dict.get('stddev')) and 'both' or None

        view_model = self.get_view_model(aggrs)
        query = view_model.objects.all()
        if query_dict.get('dbh'):
            query = query.as_dbh(int(query_dict['dbh']))
        self.fields = self.get_ordered_fields(view_model)
        if as_json:
            self.fields['the_geom'] = Field('the_geom', '%splot__the_geom' % self.plot_obs, '', 'Geometry')
        query, perim_feedback = queries_module.PerimeterSet.query_model_by_qdict(
            query, query_dict,
            plot_prefix=self.plot_prefix, plot_obs_prefix=plot_obs_prefix,
        )

        # If radio "latest inventories"
        latest_invs = False
        if aggrs and 'latestinv' in aggrs:
            latest_invs = True
            aggrs.remove('latestinv')
        if latest_invs:
            latest_inv_ids = LatestInventories.objects.values_list('pk', flat=True)
            query = query.filter(
                 **{'%sinv_team_id__in' % plot_obs_prefix: latest_inv_ids}
            )

        if aggrs is None:
            field_names = [f.vname for f in self.fields.values()]  # Extract verbose names
            field_list = [f.accessor + f.fkey_target for f in self.fields.values()]
            query = query.values_list(*field_list).order_by(self.order_by)
            query_model = query.model

        else:
            aggrs = aggrs or []
            if 'year' not in aggrs and 'inv_period' not in aggrs and not latest_invs:
                # Always add year, as grouping and mixing years makes no sense
                aggrs.append('year')

            # aggr_crits will be passed to values_list()
            query, aggr_crits, field_names = self.build_aggregations(query, aggrs)

            # Only get grouped-by fields + fields with an available aggregation
            all_aggr_fields = {}
            annot_map = self.annot_map.copy()
            wsl_volume = bool(query_dict.get('wsl_volume'))
            if 'volumen_rel_wsl' in annot_map and not wsl_volume:
                del annot_map['volumen_rel_wsl']
            all_aggr_fields.update(annot_map)
            annot_list = []
            for f in self.fields.values():
                # Last condition is a special case: when aggregating by bioklass,
                # no need to display the biolfi1m value.
                if (f.name not in all_aggr_fields or f.name in aggrs
                        or (f.name == 'biolfi1m' and 'bioklass' in aggrs)):
                    continue
                annot_alias = '%s__%s' % (f.name, all_aggr_fields[f.name].name.lower())
                if self.coalesce_with_0:
                    annot_list.append(
                        all_aggr_fields[f.name](Coalesce(f.accessor, Value(0.0 if f.ftype == 'FloatField' else 0)))
                    )
                    # mandatory default_alias
                    annot_list[-1].source_expressions[0].name = annot_alias
                else:
                    annot_list.append(all_aggr_fields[f.name](f.accessor))
                field_names.append(self.fields[f.name].vname)
                if stddev and getattr(all_aggr_fields[f.name], 'name', '') in {'Avg', 'Sum'}:
                    if stddev in ('abs', 'both'):
                        annot_list.append(StdErrAbs(f.accessor))
                        field_names.append("Sf")
                    if stddev in ('rel', 'both'):
                        annot_list.append(StdErrRel(f.accessor))
                        field_names.append("Sf [%]")

            annots, annot_names = self.add_final_annots()
            annot_list.extend(annots)
            field_names.extend(annot_names)
            query_model = query.model
            # If we used values() instead, we may have less problem with ordering, ordering would
            # be handled later in the template based on a field mapping
            if aggrs:
                query = query.values_list(*aggr_crits).annotate(*annot_list)
                # Annotations that must come after the first annotate call.
                annots2, annot_names2 = self.add_final_annots2()
                if annots2:
                    field_names.extend(annot_names2)
                    query = query.annotate(*annots2)
                query = query.order_by(*aggr_crits)
                logger.debug(str(query.query))
            else:
                # No groupment specified, but also no "None", let's aggregate
                query = [list(query.values_list(*aggr_crits).aggregate(*annot_list).values())]

        return {
            'query': query, 'field_names': field_names, 'ordering': self.get_ordering(field_names),
            'aggrs': aggrs, 'stddev': stddev is not None, 'latest_invs': latest_invs,
            'perimeter_feedback': perim_feedback, 'model': query_model,
        }

    def build_aggregations(self, query, aggrs):
        groupments = {}
        for gr_list in queries_module.get_groupments().values():
            groupments.update({gr.code: gr for gr in gr_list})

        aggr_crits = []
        field_names = []
        for fname in aggrs:
            # Privilege field join on plot_obs
            try:
                field = PlotObs._meta.get_field(fname)
                if field.auto_created and not field.concrete:
                    raise FieldDoesNotExist
                aggr_crits.append('%s%s%s' % (self.plot_obs, fname, self.get_fkey_target(fname)))
                field_names.append('*%s' % getattr(field, 'verbose_name', field.name))
            except FieldDoesNotExist:
                aggr_crit, field_name, query = groupments[fname].apply_to(self, query)
                aggr_crits.append(aggr_crit)
                field_names.append(f'*{field_name}')
        return query, aggr_crits, field_names

    def add_final_annots(self):
        return (
            [Count(self.plot_obs.rstrip('_') if self.plot_obs else 'id', distinct=True)],  # count on id/obs_id
            ['Anzahl KSP']
        )

    def add_final_annots2(self):
        return ([], [])

    def get_ordering(self, field_names):
        """Return a list of (index, [optional classes]) tuples."""
        results = []
        for idx, name in enumerate(field_names):
            if name == 'Sf [%]':
                classes = ['sf']
            elif name == 'Sf':
                classes = ['sf_abs']
            else:
                classes = ['aggr'] if name.startswith('*') else []
            results.append((idx, classes))
        return results


class ZuwachsQuery(QueryData):
    model = Zuwachs
    descr = 'Zuwachs'
    plot_obs = 'id__'
    map_color_fields = []
    annot_map = {'leb_growth_ha': Avg, 'ein_growth_ha': Avg, 'mor_growth_ha': Avg, 'growth_ha_total': Avg}
    groupables = ['otype', 'ostatus']
    category = 'Holzproduktion'

    def get_ordering(self, field_names):
        """Move Totaler Zuwachs first, minimize other columns."""
        indices = super().get_ordering(field_names)
        tot_idx = field_names.index('Totaler Zuwachs/ha/Jahr')
        leb_idx = field_names.index('Lebender Zuwachs/ha/Jahr')
        indices.insert(leb_idx, indices.pop(tot_idx))
        moved = 1
        if field_names[indices[tot_idx][0]] == 'Sf [%]':
            indices.insert(leb_idx + 1, indices.pop(tot_idx + 1))
            moved += 1
        return [
            (tpl[0], tpl[1] if i < (leb_idx + moved) else tpl[1] + ['minim'])
            for i, tpl in enumerate(indices)
        ]


class GigantenQuery(QueryData):
    model = Giganten
    descr = 'Giganten'
    plot_obs = 'plot_obs_id__'
    category = 'Naturschutz',

    def add_final_annots(self):
        return (
            [Count('id')],
            ['Anzahl Bäume']
        )
