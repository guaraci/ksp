import django.contrib.gis.db.models.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        (settings.MAIN_APP, '0002_create_views_sql'),
    ] if settings.MAIN_APP in ['ksp_bl', 'ksp_so'] else []

    operations = [
        migrations.CreateModel(
            name='HolzProduktion',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to=f'{settings.MAIN_APP}.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs',
                'managed': False,
            },
        ),

        migrations.CreateModel(
            name='EinwuchsProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs',
                'managed': False,
            },
        ),

        migrations.CreateModel(
            name='HolzProduktionProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='NutzungProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec_nutzung',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TotholzProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec_totholz',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Einwuchs',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to=f'{settings.MAIN_APP}.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_einwuchs',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Nutzung',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to=f'{settings.MAIN_APP}.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_nutzung',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Totholz',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to=f'{settings.MAIN_APP}.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_totholz',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Zuwachs',
            fields=[
                ('id', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to=f'{settings.MAIN_APP}.PlotObs')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=f'{settings.MAIN_APP}.Plot')),
                ('leb_stem', models.IntegerField(verbose_name='Lebende Stammzahl')),
                ('leb_growth_ha', models.FloatField(verbose_name='Lebender Zuwachs/ha/Jahr')),
                ('ein_stem', models.IntegerField(verbose_name='Einwuchs Stammzahl')),
                ('ein_growth_ha', models.FloatField(verbose_name='Einwuchs Zuwachs/ha/Jahr')),
                ('mor_stem', models.IntegerField(verbose_name='Tote Stammzahl')),
                ('mor_growth_ha', models.FloatField(verbose_name='Tote/genutzte Bäume Zuwachs/ha/Jahr')),
                ('growth_ha_total', models.FloatField(verbose_name='Totaler Zuwachs/ha/Jahr')),
            ],
            options={
                'db_table': 'bv_zuwachs_total',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BodenSchaden',
            fields=[
                ('id', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to=f'{settings.MAIN_APP}.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('code0', models.SmallIntegerField(verbose_name='nicht, wenig verdichtet (0-10%)')),
                ('code1', models.SmallIntegerField(verbose_name='teilweise verdichtet (10-30%)')),
                ('code2', models.SmallIntegerField(verbose_name='flächig verdichtet >30%')),
                ('code0_perc', models.FloatField(verbose_name='nicht, wenig verdichtet (0-10%) (in %)')),
                ('code1_perc', models.FloatField(verbose_name='teilweise verdichtet (10-30%) (in %)')),
                ('code2_perc', models.FloatField(verbose_name='flächig verdichtet >30% (in %)')),
            ],
            options={
                'db_table': 'bv_bodenschaden',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='EinwuchsProSpecGroup',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_specgroup_einwuchs',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='GemeindenWithFlaecheView',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(db_column='gemeindename', max_length=50, unique=True)),
                ('waldflaeche', models.IntegerField()),
            ],
            options={
                'db_table': 'gemeinden_with_flaeche',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Giganten',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('species', models.CharField(max_length=100)),
                ('dbh', models.SmallIntegerField(verbose_name='BHD')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('st_x', models.IntegerField(verbose_name='X Koord.')),
                ('st_y', models.IntegerField(verbose_name='Y Koord.')),
                ('sealevel', models.SmallIntegerField(null=True, verbose_name='Höhe ü. Meer')),
            ],
            options={
                'db_table': 'bv_giganten',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HolzProduktionProSpecGroup',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_specgroup',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='LatestInventories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('inv_from', models.DateField(verbose_name='Von')),
                ('inv_to', models.DateField(verbose_name='Bis')),
            ],
            options={
                'db_table': 'bv_latest_inventory',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='NutzungProSpecGroup',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_specgroup_nutzung',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TotholzProSpecGroup',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_specgroup_totholz',
                'managed': False,
            },
        ),
    ]
    if settings.MAIN_APP in ['ksp_bl', 'ksp_so']:
        operations.append(
            migrations.CreateModel(
                name='PlotInRegionView',
                fields=[
                    ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                    ('plot', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=f'{settings.MAIN_APP}.Plot')),
                    ('adminregion', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=f'{settings.MAIN_APP}.AdminRegion')),
                    ('region_name', models.CharField(max_length=100)),
                    ('region_type', models.CharField(max_length=20)),
                ],
                options={
                    'db_table': 'plot_in_region',
                    'managed': False,
                },
            )
        )

