from django.apps import apps

if apps.is_installed('ksp_so'):
    from ksp_so.models import *  # noqa
    from gemeinde.models import Gemeinde
elif apps.is_installed('ksp_bl'):
    from ksp_bl.models import *  # noqa
    from gemeinde.models import Gemeinde
elif apps.is_installed('ksp_zg'):
    from ksp_zg.models import *  # noqa
    from ksp_zg.models import Municipality as Gemeinde
elif apps.is_installed('valforet'):
    from valforet.models import *  # noqa
    from valforet.models import Municipality as Gemeinde
