from django.contrib.gis.db.models.functions import GeoFunc
from django.db.models import Aggregate, FloatField, IntegerField
from django.db.models.expressions import Func


class KSPVolumeBL(Func):
    function = 'ksp_tarif_volume'
    output_field = FloatField()
    arity = 1


class KSPGrundBL(Func):
    function = 'ksp_grundflaeche_bl'
    output_field = FloatField()
    arity = 1


class LocaleDichte(Func):
    function = 'ksp_lokale_dichte'
    output_field = FloatField()
    arity = 1


class WaldflaecheFromBestand(Func):
    function = 'ksp_waldf_from_bestand'
    output_field = IntegerField()
    arity = 1


class StdErrRel(Aggregate):
    """https://en.wikipedia.org/wiki/Standard_error#Relative_standard_error"""
    name = 'StdErrRel'
    template = 'stddev(%(expressions)s) / NULLIF(sqrt(count(*)), 0) / NULLIF(avg(%(expressions)s), 0) * 100.0'


class StdErrAbs(Aggregate):
    name = 'StdErrAbs'
    template = 'stddev(%(expressions)s) / NULLIF(sqrt(count(*)), 0)'


class X(GeoFunc):
    function = 'ST_X'
    output_field = FloatField()
    arity = 1


class Y(GeoFunc):
    function = 'ST_Y'
    output_field = FloatField()
    arity = 1
