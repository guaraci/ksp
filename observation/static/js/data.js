// From blue to red
//var colors = ['#0006E5', '#0058E7', '#00ACE9', '#00ECD5', '#00EE82', '#00F02D', '#28F300', '#80F500', '#DAF700', '#FABE00', '#FC6600', '#FF0B00'];
// Levels of red
var colors = ['#FFD6D4', '#FFB2AE', '#FF8D87', '#FF6961', '#D93A32', '#B2170E', '#8C0700'];

/* Default styles */
var fill = new ol.style.Fill({
    color: 'rgba(255,255,255,0.4)'
});
var stroke = new ol.style.Stroke({
    color: '#3399CC',
    width: 1.25
});
var styles = [
    new ol.style.Style({
      image: new ol.style.Circle({
        fill: fill,
        stroke: stroke,
        radius: 5
      }),
      fill: fill,
      stroke: stroke
    })
];


function mapObject() {
    this.form_data = null;
    this.proj_2056 = ol.proj.get('EPSG:2056');
    this.baselayer = new ol.layer.Tile({
        source: new ol.source.TileWMS({
          url: 'https://wms.geo.admin.ch',
          params: {
            'LAYERS': 'ch.swisstopo.swissimage',
            'FORMAT': 'image/png'
          },
        })
    });
    this.map = new ol.Map({
        target: 'map',
        layers: [
          this.baselayer
        ],
        view: new ol.View({
          projection: this.proj_2056,
          center: [2614294, 1260540],
          zoom: 11
        })
    });
    this.vector_layer = null;
    // Build styles for color levels
    this.color_styles = [];
    for (var i=0; i<colors.length; i++) {
        var cfill = new ol.style.Fill({color: colors[i]});
        this.color_styles.push(new ol.style.Style({
          image: new ol.style.Circle({
            fill: cfill,
            radius: 5
          }),
          fill: cfill,
          stroke: stroke
        }));
    }
}

mapObject.prototype.addLayer = function(form_data) {
    form_data += '&format=json';
    // Check that no aggregation is in effect
    if (form_data.indexOf('aggr=') >= 0) {
        $('div#client_error').html("Gruppiert Daten können auf der Karte nicht angezeigt werden").show();
        $('img#wait').hide();
        return;
    } else $('div#client_error').hide();

    if (this.vector_layer != null) this.map.removeLayer(this.vector_layer);
    this.vector_layer = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: $("#dataform").attr('action') + "?" + form_data,
            format: new ol.format.GeoJSON()
        })
    })
    this.map.addLayer(this.vector_layer);
    self = this;
    this.vector_layer.once("change", function(event) {
        // Zoom to vector layer points
        self.map.getView().fit(self.vector_layer.getSource().getExtent());
        // Extract and display column names from metadata request
        var cols = [];
        $.each(metadata['map_color_fields'], function(idx, el) {
            cols.push('<li><input type="radio" id="disp_'+el[0]+'" name="cols" value="'+el[1]+'"> <label for="disp_'+el[0]+'">'+el[1]+'</label></li>');
        });
        $('ul#cols').html(cols.join(''));
        $('img#wait').hide();
    });
    this.form_data = form_data;
    this.vector_layer.styled_with = 'default';
};

mapObject.prototype.styleMap = function(prop_key) {
    // Find all property values matching prop_key and color features accordingly
    var minim; var maxim;
    $.each(this.vector_layer.getSource().getFeatures(), function(idx, feature) {
        var value = parseFloat(feature.get(prop_key));
        if (isNaN(value)) return;
        if (typeof minim == "undefined") { minim = maxim = value; }
        else {
            minim = Math.min(minim, value);
            maxim = Math.max(maxim, value);
        }
    });
    var gap = maxim - minim;
    self = this;
    this.vector_layer.setStyle(function(feature, resolution) {
        var value = parseFloat(feature.get(prop_key));
        if (!isNaN(value)) {
            col_idx = Math.round((value - minim)/gap * (colors.length-1));
            return [self.color_styles[col_idx]];
        }
        else return styles;
    });
    this.vector_layer.styled_with = prop_key;
};

function populate_grouping() {
    // Dynamically populate grouping checkboxes depending on chosen views
    $("div.grp_optional").hide();
    const value = $('input[name=bio]:checked').val();
    const views = JSON.parse(document.getElementById('views-json').textContent);
    if (value) {
        // Specific grouping depending on the chosen view
        $.each($("input[name=bio]:checked"), function() {
            var keys = views[value]['groupables'];
            for (i=0; i<keys.length; i++) {
                $("div#grp_" + [keys[i]]).show();
            }
        });
        if (value == 'Giganten') {
            // Select no grouping by default
            $('#cb_no_aggr').prop('checked', true).trigger('change');
        }
    }
}

function highlight_perimeter_choices(choice_groups) {
    $.each(choice_groups, function(idx, val) {
        var num_checked = $(choice_groups[idx]).find('input:checked').length;
        var group = $(choice_groups[idx]);
        if (num_checked > 0) {
            group.addClass('selected');
            group.find('span.num_choices').html(num_checked);
            group.find('img.clear_choices').show();
        } else {
            group.removeClass('selected');
            group.find('span.num_choices').html('');
            group.find('img.clear_choices').hide();
        }
    });
}

var currentCharts = {};
var chartData = {
    stammzahl: {
        title: "Stammzahl pro Hektare in",
        colors: ['#36A2EB', '#12CBC4', '#9980FA', '#0652DD']
    },
    volumen: {
        title: "Volumen pro Hektare in",
        colors: ['#41B24C', '#829624', '#A3CB38', '#009432']
    },
    grundflaeche: {
        title: "Grundfläche pro Hektare in",
        colors: ['#FFCE56', '#EA2027', '#A46A14', '#FFC312']
    },
}

/*
 * Sort datasets by latest value of each subset (typically data from latest inventory)
 */
function sortData(labels, datasets) {
    const targetVals = datasets[datasets.length -1].data.map((item, idx) => [item.y, idx]);
    targetVals.sort((a, b) => b[0] - a[0]);
    for (let i = 0; i < datasets.length; i++) {
        // Sort data in accordance to the position in targetVals
        datasets[i].data = targetVals.map(val => datasets[i].data[val[1]]);
    }
    labels = targetVals.map(val => labels[val[1]]);
    return [labels, datasets];
}

function buildChart(chartEl, dataCol) {
    const ctx = chartEl.getContext('2d');
    const dataGrid = document.getElementById('data_grid');
    if (!dataGrid) return;

    // Parse headers to determine which columns contain interesting data
    const aggrHeaders = [];
    const dataCols = []
    dataGrid.querySelectorAll('th').forEach((head, idx) => {
        if (head.classList.contains('aggr')) aggrHeaders.push(head);
        else if (head.classList.contains('sf')) dataCols[dataCols.length - 1].sfRelIdx = idx;
        else if (head.classList.contains('sf_abs')) dataCols[dataCols.length - 1].sfAbsIdx = idx;
        else dataCols.push({head: head, colIdx: idx});
    });
    const targetData = dataCols[dataCol - 1];
    const sortByLatestValue = aggrHeaders[aggrHeaders.length - 1].textContent.includes('Baumart')

    const xLabel = aggrHeaders[aggrHeaders.length - 1].textContent;
    const yLabel = targetData.head.textContent;
    const perimeterNames = Array.from(document.querySelectorAll('.perimeter-name')).map(span => span.textContent).join(", ");

    let specData = {title: '', colors: chartData.stammzahl.colors};
    if (yLabel.includes('Stammzahl')) specData = chartData.stammzahl;
    else if (yLabel.includes('Volume')) specData = chartData.volumen;
    else if (yLabel.includes('Grundfl')) specData = chartData.grundflaeche;

    // Read HTML table into dataTable list of lists
    const dataTable = [];
    document.querySelectorAll('#data_grid tbody tr').forEach(tr => {
        let vals = Array.from(tr.querySelectorAll('td')).map((td, idx) => {
            if (idx < aggrHeaders.length) return td.textContent;
            else return parseFloat(td.textContent.replace(',', '.'));
        })
        dataTable.push(vals);
    });

    let colLabels = [];
    let datasets = [];
    if (aggrHeaders.length > 1) {
        // If we have more than one aggr, take the second as base and add groups for each interval
        const timeHeaderidx = 0;
        const groupHeaderidx = 1;
        const groupValues = [...new Set(dataTable.map(line => line[groupHeaderidx]))];
        const timeValues = [...new Set(dataTable.map(line => line[timeHeaderidx]))];
        timeValues.forEach((timeVal, idx) => {
            let dataset = {
                type: "barWithErrorBars",
                data: Array(groupValues.length).fill({}),
                label: `${aggrHeaders[0].textContent} ${timeVal}`,
                backgroundColor: [specData.colors[idx] + '33'],
                borderColor: [specData.colors[idx] + 'FF'],
                borderWidth: 2
            };
            dataTable.forEach(line => {
                if (line[timeHeaderidx] == timeVal) {
                    const val = line[targetData.colIdx],
                          sf = line[targetData.sfAbsIdx];
                    dataset.data[groupValues.indexOf(line[groupHeaderidx])] = {y: val, yMin: [val - sf], yMax: [val + sf]};
                }
            });
            datasets.push(dataset);
        });
        colLabels = groupValues;
    } else {
        let dataset = {
            type: "barWithErrorBars",
            label: 'Waldinvent. Kt Zug',
            data: [],
            backgroundColor: [specData.colors[0] + '33'],
            borderColor: [specData.colors[0] + 'FF'],
            borderWidth: 2
        };
        dataTable.forEach(line => {
            colLabels.push(line.slice(0, aggrHeaders.length).join(", "));
            const val = line[targetData.colIdx],
                  sf = line[targetData.sfAbsIdx];
            dataset.data.push({y: val, yMin: [val - sf], yMax: [val + sf]});
        });
        datasets.push(dataset);
    }
    const bioLabel = document.querySelector("input[name='bio']:checked").nextElementSibling.textContent;
    const graphTitle = (bioLabel.includes('Stammzahl,') ? '' : bioLabel + ' · ') + specData.title + ' ' + perimeterNames;
    if (sortByLatestValue) {
        [colLabels, datasets] = sortData(colLabels, datasets);
    }

    currentCharts[chartEl.id] = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: colLabels,
            datasets: datasets
        },
        options: {
            plugins: {
                title: {
                    display: true,
                    text: graphTitle,
                    font: {size: 20}
                },
                legend: {
                    display: datasets.length > 1
                },
                custom_canvas_background_color: {bgColor: '#f3f6ec'}
            },
            scales: {
                x: {
                    title: {
                        display: true,
                        text: xLabel,
                        font: {size: 18}
                    }
                },
                y: {
                    title: {
                        display: true,
                        text: yLabel,
                        font: {size: 18}
                    }
                    //beginAtZero: true
                }
            }
        },
        plugins: [{
            id: 'custom_canvas_background_color',
            beforeDraw: (chart, args, options) => {
              const ctx = chart.canvas.getContext('2d');
              ctx.save();
              ctx.globalCompositeOperation = 'destination-over';
              ctx.fillStyle = options.bgColor;
              ctx.fillRect(0, 0, chart.canvas.width, chart.canvas.height);
              ctx.restore();
            }
        }]
    });
}

function activateTab(tab) {
    if (tab.classList.contains('active')) return false;
    document.querySelectorAll('li.tab').forEach(tb => {
        tb.classList.remove('active');
        document.getElementById(tb.dataset.target).setAttribute('hidden', true);
    });
    tab.classList.add('active');
    document.getElementById(tab.dataset.target).removeAttribute('hidden');
    if (!isEmpty(currentCharts)) {
        Object.keys(currentCharts).forEach(key => {currentCharts[key].destroy();});
        currentCharts = {};
    }
    if (tab.dataset.target == 'mapmain') {
        if (map == null) {
            map = new mapObject();
        }
        if (form_data != null && map.form_data != form_data+'&format=json') {
            // Load data into the map
            map.addLayer(form_data);
        }
    } else if (tab.dataset.target == 'graphmain') {
        buildChart(document.getElementById('chart1'), 1);
        buildChart(document.getElementById('chart2'), 2);
        buildChart(document.getElementById('chart3'), 3);
    } else {
        $('div#client_error').hide();
    }
}

var map = null;
var data = null;
var form_data = null;
var metadata = null;
$(document).ready(function() {
    $(":button", "div#refresh").click(function (ev) {
        $('img#wait').show();
        const graphTab = document.querySelector('#graph-tab');
        if (graphTab) graphTab.setAttribute('hidden', true);
        activateTab(document.querySelector('#data-tab'));
        form_data = $("#dataform").serialize();
        $.getJSON($("#dataform").attr('action'), form_data + '&metadata=1&format=json', function(data) {
            metadata = data;
        });
        if ($('li#data-tab').hasClass('active')) {
            $("div#datamain").load($("#dataform").attr('action'), form_data, function(response, status, xhr) {
                $('img#wait').hide();
                if (status == "error") $('div#server_error').show();
                else {
                    $('div#server_error').hide();
                    $("table.tablesorter").tablesorter({
                        sortList: [[0,0]]
                        //widgets: ["filter"]
                    });
                    color_standardfehler($("td.sf"));
                    const checkedBio = document.querySelector("input[name='bio']:checked");
                    if (graphTab && checkedBio) {
                        if (document.querySelectorAll('#data_grid th.aggr').length < 3) {
                            // For now, only show graph tab if grouped by at most 2 columns.
                            graphTab.removeAttribute('hidden');
                        }
                    }
                }
            });
        } else if ($('li#map-tab').hasClass('active')) {
            map.addLayer(form_data);
        }
    });
    $("#csv-export").click(function (ev) {
        ev.preventDefault();
        form_data = $("#dataform").serialize();
        window.location.href = $("#dataform").attr('action') + '?format=csv&' + form_data;
    });
    document.querySelectorAll('li.tab').forEach(li => {
        li.addEventListener('click', (ev) => activateTab(ev.target));
    });
    $('ul#cols').on('click', 'input', function (ev) {
        if (map.vector_layer.styled_with == this.value) {
            map.vector_layer.setStyle();
            map.vector_layer.styled_with = 'default';
            $(this).prop('checked', false);
        } else map.styleMap(this.value);
    });
    $('img.clear_choices').click(function(ev) {
        ev.stopPropagation();
        $(this).parent().next().find('input:checked').prop('checked', false);
        highlight_perimeter_choices($(this).closest('div.perim_choice'));
    });

    $('input[name=bio]').click(function () {
        $('div.view-details').hide();
        $('#' + $(this).data('viewhelp')).show();
        document.getElementById('datamain').innerHTML = '';
        activateTab(document.querySelector('#data-tab'));
        populate_grouping();
    });
    populate_grouping();
    // Allow deselecting 'perimeter' radio buttons
    $('div.deselectable input[type=radio]').click(function() {
        if($(this).data('previousValue') == true){
            $(this).prop('checked', false);
        } else {
            $('input[name=paggr]').data('previousValue', false);
        }
        $(this).data('previousValue', $(this).prop('checked'));
    });

    $('input[name=aggr]').click(function () {
        var details = $('div#details-' + $(this).attr('id'));
        if (!details.length) return;
        details.toggle($(this).prop('checked'));
    });
    $('#cb_no_aggr').change(function () {
        if (this.checked == true) {
            // Uncheck and deactivate all
            $('input[name=aggr]').prop('checked', false).prop('disabled', true);
            $('input[name=paggr]').prop('checked', false).prop('disabled', true);
            $('input[name=yaggr]').prop('checked', false).prop('disabled', true);
            $('#cb_stddev').prop('checked', false).prop('disabled', true);
        } else {
            $('input[name=aggr]').prop('disabled', false);
            $('input[name=paggr]').prop('disabled', false);
            $('input[name=yaggr]').prop('disabled', false);
            // Check year by default
            $('#cb_year').prop('checked', true);
            $('#cb_stddev').prop('disabled', false).prop('checked', true);
        }
    });

    $('div.perim_choice input').change(function() {
        highlight_perimeter_choices($(this).closest('div.perim_choice'));
    });
    highlight_perimeter_choices($('div.perim_choice'));
});
