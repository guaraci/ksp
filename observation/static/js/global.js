// Missing IE indexOf for arrays
if (!Array.prototype.indexOf) {
   Array.prototype.indexOf = function(item) {
      var i = this.length;
      while (i--) {
         if (this[i] === item) return i;
      }
      return -1;
   }
}

function color_standardfehler(elements) {
    // Set cell color when standard error greater than some threshold
    var greater_than_10 = elements.filter(function() {
        return parseInt($(this).text()) > 10;
    }).css("background-color", "#fabf8f");
    var greater_than_25 = greater_than_10.filter(function() {
        return parseInt($(this).text()) > 25;
    }).css("background-color", "#ff6161");
    var empty = elements.filter(function() {
        return ($(this).text() == '' && !isNaN(parseInt($(this).prev().text())));
    }).css("background-color", "#ff6161");
    greater_than_10.prev().css("background-color", "#fabf8f");
    greater_than_25.prev().css("background-color", "#ff6161");
    empty.prev().css("background-color", "#ff6161");
}

function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

function attachHandler(selector, event, func) {
    document.querySelectorAll(selector).forEach(item => {
        item.addEventListener(event, func);
    });
}

function filter_choices(container, value) {
    // With a value coming from a text input, this allows filtering lengthy
    // checkboxes lists.
    var search_regex = new RegExp("(" + value + ")", "gi");
    if (value) {
        container.querySelectorAll('input[type=checkbox]').forEach(cb => {
            var cont_div = cb.parentNode;
            if (cont_div.textContent.match(search_regex) || cb.checked) {
                cont_div.removeAttribute('hidden');
            } else cont_div.setAttribute('hidden', true);
        });
    }
    else {
        $(container).find(':hidden').show();
    }
    return false;
}

$(document).ready(function() {
    $('div.togglable').prepend('<img class="tri" src="' + static_url + 'img/tri-closed-white.png' + '">');
    $('div.togglable').click(function (ev) {
        var target = $(this).next();
        if (target.is(':visible')) {
            $('img.tri', this).attr('src', static_url + 'img/tri-closed-white.png');
        } else {
            $('img.tri', this).attr('src', static_url + 'img/tri-open-white.png');
        }
        target.toggle();
    });
    $('select[name="inventory_filter"]').change(function(ev) {
        window.location.href = '?aufn=' + $(this).val();
    });
    $(document).tooltip();
    $('img.info, a.info').click(function(ev) {
        ev.preventDefault();
        var url = $(this).data('url') || $(this).attr('href');
        if (!url) return;
        $('div#dialog-div').load(url, function() {
            $(document).tooltip('destroy');
            $('div#dialog-div').dialog({
                modal: true,
                width: '60%',
                close: function() {$(document).tooltip()}
            });
        });
    });
    $('button.confirm').click((ev) => {
        if (!confirm(ev.target.dataset.confirm)) {
            ev.preventDefault();
            return false;
        }
    });
    // Delay when user type in choice filter input
    var timer;
    attachHandler("input.choice_filter", 'keyup', ev => {
        clearTimeout(timer);
        var ms = 1000;
        var val = ev.target.value;
        var parent = ev.target.parentNode;
        timer = setTimeout(function() {
            filter_choices(parent, val);
        }, ms);
    });
});

