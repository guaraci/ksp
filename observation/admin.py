import re

from django import forms
from django.contrib import admin, messages
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.forms import OSMWidget, PointField
from django.contrib.gis.gdal import CoordTransform, SpatialReference
from django.core import serializers
from django.http import HttpResponse, JsonResponse


class MyOSMWidget(OSMWidget):
    """ Overriden OSMWidget to set local JS files (collected with django-remote-finder). """
    default_lon = 7.65
    default_lat = 47.47

    class Media:
        extend = False
        css = {
            'all': ('css/ol.css', 'gis/css/ol3.css')
        }
        js = (
            'js/ol3.js',
            'gis/js/OLMapWidget.js',
        )


class CoordField(PointField):
    widget = forms.TextInput

    def clean(self, value):
        # Accept decimal coords notation (47 35'38,1367 N 7 40'49,9824 E)
        m = re.match(r'(\d+)[°\s]+(\d+)[\'′\s]+([\d,\.]+)\s?[NS]' +
                     r'\s?(\d+)[°\s]+(\d+)[\'′\s]+([\d,\.]+)\s?[EW]',
                     value)
        if m:
            deg_lat, min_lat, sec_lat, deg_lon, min_lon, sec_lon = m.groups()
            lat = int(deg_lat) + int(min_lat)/60 + float(sec_lat.replace(',', '.'))/3600
            lon = int(deg_lon) + int(min_lon)/60 + float(sec_lon.replace(',', '.'))/3600
            return "SRID=4326;POINT(%(lon)s %(lat)s)" % {'lon': lon, 'lat': lat}
        return super().clean(value)


def export_to_json(modeladmin, request, queryset):
    def get_objects():
        for plotobs in queryset:
            yield plotobs.plot
            yield plotobs
            for treeobs in plotobs.treeobs_set.all():
                yield treeobs.tree
                yield treeobs
            for regenobs in plotobs.regenobs_set.all():
                yield regenobs

    response = HttpResponse(content_type="application/json")
    serializers.serialize("json", get_objects(), indent=2, use_natural_foreign_keys=True, stream=response)
    return response


def export_to_geojson(modeladmin, request, queryset):
    struct = {'type': "FeatureCollection", "features": []}
    trans_to_wgs84 = CoordTransform(SpatialReference(2056), SpatialReference(4326))
    plot_fields = ('nr', 'phytosoc', 'slope', 'exposition', 'sealevel')
    plotobs_fields = queryset[0].visible_fields(exclude_empty=False)
    regen_fields = ('perc', 'perc_an', 'perc_auf', 'verbiss', 'fegen')
    tree_fields = ('spec', 'nr', 'azimuth', 'distance')
    treeobs_fields = (
        'survey_type', 'layer', 'dbh', 'rank', 'vita', 'damage', 'damage_cause',
        'crown_length', 'crown_form', 'stem', 'stem_forked', 'stem_height', 'woodpecker',
        'ash_dieback', 'remarks',
    )

    def str_or_none(val):
        return val if val is None or val is True or val is False else str(val)

    def serialize(obj, fields):
        return {fname: str_or_none(getattr(obj, fname)) for fname in fields}

    for plotobs in queryset:
        pt = plotobs.plot.the_geom  # plotobs.plot.point_exact or plotobs.plot.the_geom
        pt.transform(trans_to_wgs84)
        props = serialize(plotobs.plot, plot_fields)
        props.update(serialize(plotobs, plotobs_fields))
        props['regenobs'] = {str(ro.spec): serialize(ro, regen_fields) for ro in plotobs.regenobs_set.all()}

        struct["features"].append({
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": pt.coords,
            },
            "properties": props,
        })
        for treeobs in plotobs.treeobs_set.all():
            pt = treeobs.tree.point
            pt.transform(trans_to_wgs84)
            props = serialize(treeobs.tree, tree_fields)
            props.update(serialize(treeobs, treeobs_fields))
            struct["features"].append({
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": pt.coords,
                },
                "properties": props,
            })
    return JsonResponse(struct)
