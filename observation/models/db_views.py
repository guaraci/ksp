"""
**************
Database Views
**************
"""
import re

from django.conf import settings
from django.db import connections, models
from django.db.models.sql import Query
from django.db.models.sql.compiler import SQLCompiler

from ..common_models import (
    AdminRegion, Gemeinde, Plot, PlotObs, TreeGroup, TreeSpecies, Vita,
)
from ..model_utils import DBView

vol_rel_title = "Volume (m3/ha) [ET]:\n Einheitstarif BL/BS" if settings.MAIN_APP=='ksp_bl' else "Volume [m3/ha]"

DEFAULT = object()


"""
This Adaptable* class series is a hack to allow for modifying the SQL of a QuerySet
before it is submitted to the database.
"""
class AdaptableCompiler(SQLCompiler):
    def as_sql(self, **kwargs):
        sql, params = super().as_sql(**kwargs)
        if self.query.dbh is not DEFAULT:
            match = re.search('FROM "(bv_grundf_vol_yx_pro_plotobs(?:_und_)(?:allspec|specgroup)?)"', sql)
            if match:
                # Extract view definition, replace the dbh condition in the view def,
                # and substitute the view name by the view definition as a subquery
                view_name = match.group(1)
                view = DBView(name=view_name)
                new_def = view.definition.replace("dbh > 11", f"dbh > {self.query.dbh}")
                sql = sql.replace(
                    f'FROM "{view_name}"',
                    f'FROM ({new_def.rstrip(";")}) "{view_name}"'
                )
        return sql, params


class AdaptableQuery(Query):
    dbh = DEFAULT

    def get_compiler(self, using=None, connection=None, elide_empty=True):
        if using is None and connection is None:
            raise ValueError("Need either using or connection")
        if using:
            connection = connections[using]
        return AdaptableCompiler(
            self, connection, using, elide_empty
        )


class AdaptableQuerySet(models.QuerySet):
    def __init__(self, model=None, query=None, **kwargs):
        if query is None:
            query = AdaptableQuery(model)
        super().__init__(model=model, query=query)

    def as_dbh(self, value):
        clone = self._chain()
        clone.query.dbh = value
        return clone


class LatestInventories(models.Model):
    municipality = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', on_delete=models.PROTECT)
    inv_from = models.DateField("Von")
    inv_to = models.DateField("Bis")

    class Meta:
        db_table = 'bv_latest_inventory'
        managed = False


if issubclass(AdminRegion, models.Model):
    class PlotInRegionView(models.Model):
        """A materialized view linking plots to region polygons."""
        plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
        adminregion = models.ForeignKey(AdminRegion, on_delete=models.DO_NOTHING)
        region_name = models.CharField(max_length=100)
        region_type = models.CharField(max_length=20)

        class Meta:
            db_table = 'plot_in_region'
            managed = False


class GemeindenWithFlaecheView(models.Model):
    """A materialized view storing the forest surface calculated from waldbestandkarte."""
    gid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, unique=True, db_column='gemeindename')
    # + all other gemeindegrenzen_bsbl fields
    waldflaeche = models.IntegerField()

    class Meta:
        db_table = 'gemeinden_with_flaeche'
        managed = False


class BaseHolzProduktion(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    plotobs = models.ForeignKey(PlotObs, db_column='id', primary_key=True, on_delete=models.DO_NOTHING, related_name='+')
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING, related_name='+')
    year = models.SmallIntegerField(verbose_name="Jahr")

    gemeinde = models.ForeignKey(
        Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING, related_name='+'
    )
    anzahl_baume_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    anzahl_baume_rel = models.FloatField("Stammzahl/ha", db_column="Stammzahl pro ha")
    volumen_abs = models.DecimalField("Volumen [m3]", max_digits=5, decimal_places=1, db_column="Volumen m3")
    volumen_rel = models.FloatField(verbose_name=vol_rel_title, db_column="Volumen pro ha")
    grundflaeche_abs = models.FloatField("Grundfläche [m2]", db_column="Grundflaeche m2")
    grundflaeche_rel = models.FloatField("Grundfläche [m2/ha]", db_column="Grundflaeche pro ha")

    objects = AdaptableQuerySet.as_manager()

    class Meta:
        abstract = True


class HolzProduktion(BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs"
        managed = False


class ModelProSpec(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    id = models.CharField(max_length=20, primary_key=True)
    plotobs = models.ForeignKey(PlotObs, db_column='plot_obs_id', on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING)
    spec = models.ForeignKey(TreeSpecies, verbose_name="Baumart", db_column='tree_spec_id', on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")

    gemeinde = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING)
    anzahl_baume_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    anzahl_baume_rel = models.FloatField("Stammzahl/ha", db_column="Stammzahl pro ha")
    volumen_abs = models.DecimalField("Volumen [m3]", max_digits=5, decimal_places=1, db_column="Volumen m3")
    volumen_rel = models.FloatField(verbose_name=vol_rel_title, db_column="Volumen pro ha")
    grundflaeche_abs = models.FloatField("Grundfläche [m2]", db_column="Grundflaeche m2")
    grundflaeche_rel = models.FloatField("Grundfläche [m2/ha]", db_column="Grundflaeche pro ha")

    class Meta:
        abstract = True


if issubclass(TreeGroup, models.Model):
    class ModelProSpecGroup(models.Model):
        # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
        id = models.CharField(max_length=20, primary_key=True)
        plotobs = models.ForeignKey(PlotObs, db_column='plot_obs_id', on_delete=models.DO_NOTHING)
        plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING)
        group = models.ForeignKey(TreeGroup, verbose_name="Baumartgruppe", db_column='tree_group_id', on_delete=models.DO_NOTHING)
        year = models.SmallIntegerField(verbose_name="Jahr")

        gemeinde = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING)
        anzahl_baume_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
        anzahl_baume_rel = models.FloatField("Stammzahl/ha", db_column="Stammzahl pro ha")
        volumen_abs = models.DecimalField("Volumen [m3]", max_digits=5, decimal_places=1, db_column="Volumen m3")
        volumen_rel = models.FloatField(verbose_name=vol_rel_title, db_column="Volumen pro ha")
        grundflaeche_abs = models.FloatField("Grundfläche [m2]", db_column="Grundflaeche m2")
        grundflaeche_rel = models.FloatField("Grundfläche [m2/ha]", db_column="Grundflaeche pro ha")

        class Meta:
            abstract = True


class HolzProduktionProSpec(ModelProSpec):
    objects = AdaptableQuerySet.as_manager()

    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec"
        managed = False


if issubclass(TreeGroup, models.Model):
    class HolzProduktionProSpecGroup(ModelProSpecGroup):
        objects = AdaptableQuerySet.as_manager()

        class Meta:
            db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup"
            managed = False


class Totholz(BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_totholz"
        managed = False


class TotholzProSpec(ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec_totholz"
        managed = False


if issubclass(TreeGroup, models.Model):
    class TotholzProSpecGroup(ModelProSpecGroup):
        class Meta:
            db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup_totholz"
            managed = False


class Einwuchs(BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_einwuchs"
        managed = False


class EinwuchsProSpec(ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs"
        managed = False


if issubclass(TreeGroup, models.Model):
    class EinwuchsProSpecGroup(ModelProSpecGroup):
        class Meta:
            db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup_einwuchs"
            managed = False


class Nutzung(BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_nutzung"
        managed = False


class NutzungProSpec(ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec_nutzung"
        managed = False


if issubclass(TreeGroup, models.Model):
    class NutzungProSpecGroup(ModelProSpecGroup):
        class Meta:
            db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup_nutzung"
            managed = False


class Zuwachs(models.Model):
    id = models.ForeignKey(PlotObs, db_column='id', primary_key=True, on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    leb_stem = models.IntegerField("Lebende Stammzahl")
    #leb_perc = models.FloatField()  # Unused in interface
    leb_growth_ha = models.FloatField("Lebender Zuwachs/ha/Jahr")
    ein_stem = models.IntegerField("Einwuchs Stammzahl")
    ein_growth_ha = models.FloatField("Einwuchs Zuwachs/ha/Jahr")
    mor_stem = models.IntegerField("Tote Stammzahl")
    mor_growth_ha = models.FloatField("Tote/genutzte Bäume Zuwachs/ha/Jahr")
    growth_ha_total = models.FloatField("Totaler Zuwachs/ha/Jahr")

    class Meta:
        db_table = "bv_zuwachs_total"
        managed = False


class BodenSchaden(models.Model):
    id = models.ForeignKey(PlotObs, db_column='id', primary_key=True, on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    municipality = models.ForeignKey(Gemeinde, verbose_name="Gemeinde", on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField("Jahr")
    #soil_compaction =
    code0 = models.SmallIntegerField("nicht, wenig verdichtet (0-10%)")
    code1 = models.SmallIntegerField("teilweise verdichtet (10-30%)")
    code2 = models.SmallIntegerField("flächig verdichtet >30%")

    class Meta:
        db_table = "bv_bodenschaden"
        managed = False


class Giganten(models.Model):
    #id = TreeObs.id
    species = models.CharField(max_length=100)
    dbh = models.SmallIntegerField("BHD")
    plot_obs = models.ForeignKey(PlotObs, on_delete=models.DO_NOTHING, db_column='plot_obs_id')
    year = models.SmallIntegerField("Jahr")
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING, verbose_name="Aufnahmepunkt")
    vita = models.ForeignKey(Vita, on_delete=models.DO_NOTHING, verbose_name="Lebenslauf")
    st_x = models.IntegerField("X Koord.")
    st_y = models.IntegerField("Y Koord.")
    sealevel= models.SmallIntegerField("Höhe ü. Meer", null=True)
    municipality = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = "bv_giganten"
        managed = False
