import json
import math
import re
import subprocess
from datetime import date

from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.serializers.json import DjangoJSONEncoder
from django.db import connection, models
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.functional import cached_property, classproperty
from django.utils.translation import gettext_lazy as _

from observation.swisstopo import query_elevation

# This table maps the slope to the radius of the plot.
SLOPE_MAPPING = {
    0: 9.77,
    20: 9.77,
    25: 9.92,
    30: 9.98,
    35: 10.06,
    40: 10.14,
    45: 10.23,
    50: 10.33,
    55: 10.44,
    60: 10.55,
    65: 10.67,
    70: 10.80,
    75: 10.93,
    80: 11.06,
    85: 11.2,
    90: 11.3,
    95: 11.48,
    100: 11.62,
    105: 11.77,
    110: 11.91,
    115: 12.06,
    120: 12.21,
    125: 12.36,
    130: 12.51,
    135: 12.67,
    140: 12.82,
    145: 12.97,
    150: 13.12,
    160: 13.43,
}


class CodeDescriptionManager(models.Manager):
    def get_by_natural_key(self, code, description):
        return self.get(code=code)


class CodeDescriptionBase(models.Model):
    """Common methods for models with code and description (most lookup tables)"""
    objects = CodeDescriptionManager()

    class Meta:
        abstract = True

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)

    def natural_key(self):
        return (self.code, self.description)


class PlotBase(models.Model):
    class Meta:
        abstract = True
        db_table = 'plot'

    default_radius = 9.77

    @classproperty
    def slope_mapping(cls):
        return SLOPE_MAPPING

    @property
    def radius(self):
        if not self.slope:
            return self.default_radius
        if self.slope in self.slope_mapping:
            return self.slope_mapping[self.slope]
        if self.slope <= 20:
            return self.default_radius
        rounded = int(round(self.slope/5.0)*5)
        return self.slope_mapping[rounded]

    def set_elevation_from_swisstopo(self, save=True):
        """Get elevation from SwissTOPO Height service and return True if elevation changed."""
        elevation = query_elevation(self.point_exact or self.the_geom)
        if elevation is not None and self.sealevel != int(elevation):
            self.sealevel = int(elevation)
            if save:
                self.save()
            return True
        return False

    def get_observations(self):
        return self.plotobs_set.prefetch_related('treeobs_set').order_by('year')

    def get_absolute_url(self):
        return reverse('plot_detail', args=[self.pk])


class PlotObsBase(models.Model):
    operator = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_("Operator"), on_delete=models.SET_NULL, null=True
    )
    created = models.DateTimeField(_("Erstellt am"), auto_now_add=True, null=True)

    class Meta:
        abstract = True

    @classmethod
    def get_from_mobile_data(cls, data, plot):
        """Return PlotObs instance from data received from mobile forms."""
        from .common_models import Plot, PlotObs, Inventory, Gemeinde

        municipality_id = data['municipality'][0] if isinstance(data['municipality'], list) else data['municipality']
        if municipality_id == '':
            municipality = get_object_or_404(Gemeinde, the_geom__contains=getattr(plot, Plot.geom_field))
        else:
            municipality = get_object_or_404(Gemeinde, pk=municipality_id)
        if data['inv_team']:
            inventory = get_object_or_404(Inventory, pk=int(data['inv_team']))
        else:
            inventory = municipality.inventory_set.get(inv_to__gte=date.today())
            data['inv_team'] = inventory.pk

        try:
            plotobs = PlotObs.objects.get(plot=plot, year=data['year'])
            # It's a new sync with existing data, overwrite old data with the new one
            plotobs.delete()
        except PlotObs.DoesNotExist:
            pass
        return PlotObs(
            plot=plot, year=data['year'], municipality=municipality,
            inv_team=inventory, density=inventory.default_density, gwl=0
        )

    def previous_obs(self):
        return self.plot.plotobs_set.order_by('year').last()

    def not_cut_trees(self):
        return self.treeobs_set.filter(vita__code__in=['e', 'l', 'm'])

    def as_geojson(self, dumped=True, srid=None, verbose=False):
        """
        Return a FeatureCollection GeoJSON with the plot as first feature and
        the PlotObs trees as following features.
        """
        def serialize(value):
            if isinstance(value, models.Model):
                return [value.pk, str(value)]
            elif isinstance(value, bool):
                return value
            return str(value)

        geojson = {"type": "FeatureCollection", "features": [], "plot": self.plot.pk}
        geom = self.plot.point_exact if self.plot.point_exact else self.plot.the_geom
        if (srid is not None and srid != geom.srid):
            geom.transform(srid)
        # Add plot (center), radius, trees
        geojson['features'].append({
            "type": "Feature",
            "id": self.pk,
            "geometry": json.loads(geom.json),
            "properties": {"type": "center"},
        })
        geojson['features'][-1]["properties"]["radius"] = self.plot.radius
        if self.plot.point_exact and getattr(self.plot, 'gps_precision', None):
             geojson['features'][-1]["properties"]["gps_precision"] = self.plot.gps_precision
        for field_name in self.visible_fields():
            key = str(self._meta.get_field(field_name).verbose_name) if verbose else field_name
            geojson['features'][-1]["properties"][key] = serialize(getattr(self, field_name))

        # Add trees with position
        for treeobs in self.treeobs_set.select_related('tree', 'tree__spec', 'vita').all(
                ).order_by('tree__nr'):
            tree = treeobs.tree
            pt = tree.point
            if (srid is not None and srid != pt.srid):
                pt.transform(srid)
            geojson['features'].append({
                "type": "Feature",
                "id": treeobs.pk,
                "geometry": json.loads(pt.json),
                "properties": {
                    "type": "tree",
                    "nr": tree.nr,
                    "azimuth": tree.azimuth,
                    "distance": str(tree.distance),  # Decimal is not JSON-serializable
                    "species": serialize(tree.spec),
                    "color": tree.spec.color if tree.spec else '',
                },
            })
            geojson['features'][-1]["properties"].update(dict(
                (str(field.name), serialize(getattr(treeobs, field.name)))
                 for field in treeobs._meta.fields
                 if not field.name.startswith('i') and getattr(treeobs, field.name) not in (None, '')
            ))
            geojson['features'][-1]["properties"]["tree"] = treeobs.tree_id
        if dumped:
            return json.dumps(geojson, cls=DjangoJSONEncoder)
        return geojson


class TreeMixin:
    @property
    def point(self):
        """
        Calculate and return point from plot origin and relative distance/azimuth.
        """
        grad_to_rad = lambda val: val*math.pi/200
        plot_center = self.plot.point_exact or self.plot.the_geom
        return Point(
            plot_center.x + float(self.distance/10) * math.sin(grad_to_rad(self.azimuth)),
            plot_center.y + float(self.distance/10) * math.cos(grad_to_rad(self.azimuth)),
            srid=plot_center.srid
        )


class DBView:
    """Introspection class for a database view."""
    def __init__(self, name=None, oid=None):
        if not name and not oid:
            raise ValueError("You must provide either name or oid")
        self._name = name
        self._oid = oid
        self._relkind = None

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __lt__(self, other):
        return self.name.lower() < other.name.lower()

    @property
    def oid(self):
        if self._oid:
            return int(self._oid)
        with connection.cursor() as cursor:
            cursor.execute("SELECT oid, relkind FROM pg_class WHERE relname=%s", [self._name])
            try:
                self._oid, self._relkind = cursor.fetchone()
            except TypeError:
                pass
        return self._oid

    @property
    def name(self):
        if self._name:
            return self._name
        with connection.cursor() as cursor:
            cursor.execute("SELECT relname, relkind FROM pg_class WHERE oid=%s", [self._oid])
            try:
                self._name, self._relkind = cursor.fetchone()
            except TypeError:
                pass
        return self._name

    @property
    def is_view(self):
        if self._relkind is None:
            self.name, self.oid  # Force db query
        return self._relkind in ('v', 'm')

    @cached_property
    def definition(self):
        with connection.cursor() as cursor:
            if self.is_view:
                cursor.execute("SELECT pg_get_viewdef(%s, true)", [self.oid])
                try:
                    definition = cursor.fetchone()[0]
                except TypeError:
                    return None
            else:
                db_default = settings.DATABASES['default']
                try:
                    dump = subprocess.check_output(
                        "pg_dump -U %s -t 'public.%s' --schema-only --no-acl %s" % (
                            db_default['USER'], self.name, db_default['NAME']
                        ),
                        env={'PGPASSWORD': db_default.get('PASSWORD', '')},
                        shell=True
                    )
                except subprocess.CalledProcessError as err:
                    return "Error retrieving table structure (%s)." % err
                # Grab the CREATE TABLE part
                capture = False
                definition = ''
                comments = self.field_comments()
                for line in dump.decode('utf-8').splitlines():
                    if 'CREATE TABLE' in line:
                        capture = True
                    if capture:
                        first_word = line.strip().split()[0]
                        comment = ''
                        if first_word in comments and comments[first_word] is not None:
                            comment = '    -- %s\n' % comments[first_word]
                        definition += comment + line + "\n"
                        if line == ");":
                            break
        return definition

    @cached_property
    def comment(self):
        with connection.cursor() as cursor:
            cursor.execute("SELECT obj_description(%s, 'pg_class')", [self.oid])
            return cursor.fetchone()[0]

    def field_comments(self):
        query = """SELECT
            cols.column_name,
            (
                SELECT
                    pg_catalog.col_description(c.oid, cols.ordinal_position::int)
                FROM
                    pg_catalog.pg_class c
                WHERE
                    c.oid = (SELECT ('"' || cols.table_name || '"')::regclass::oid)
                    AND c.relname = cols.table_name
            ) AS column_comment
        FROM
            information_schema.columns cols
        WHERE
            cols.table_catalog = %s AND cols.table_name = %s AND cols.table_schema = 'public';
        """
        with connection.cursor() as cursor:
            cursor.execute(query, (settings.DATABASES['default']['NAME'], self.name))
            return {line[0]: line[1] for line in cursor.fetchall()}

    def sub_views(self, recurse=False, only_views=False):
        """Return a list of dependent views or tables."""
        if not self.is_view:
            return []
        matches = re.findall(r'(?:FROM|JOIN)\s(\w+)', self.definition)
        sub_views = [DBView(name=view_name) for view_name in set(matches)]
        if recurse:
            [sub_views.extend(dbv.sub_views(recurse=True)) for dbv in sub_views]
        if only_views:
            return [sb for sb in sub_views if sb.is_view]
        return sub_views
